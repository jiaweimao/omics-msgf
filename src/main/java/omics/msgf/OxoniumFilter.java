/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf;

import omics.util.ms.peaklist.PeakAnnotation;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.AbstractPeakProcessor;

import java.util.Arrays;
import java.util.List;

/**
 * A mz filter which remove all OxoniumMarker from the spectrum.
 *
 * @author JiaweiMao
 * @version 1.2.0
 * @since 22 May 2018, 9:53 AM
 */
public class OxoniumFilter extends AbstractPeakProcessor<PeakAnnotation, PeakAnnotation>
{
    private final Tolerance tolerance;
    private double[] oxoniumMzs;
    private List<OxoniumMarker> markerList;

    public OxoniumFilter(List<OxoniumMarker> markerList, Tolerance tolerance)
    {
        this.markerList = markerList;
        this.tolerance = tolerance;
        oxoniumMzs = new double[markerList.size()];
        for (int i = 0; i < markerList.size(); i++) {
            oxoniumMzs[i] = markerList.get(i).getMz();
        }
        Arrays.sort(oxoniumMzs);
    }

    public List<OxoniumMarker> getMarkerList()
    {
        return markerList;
    }

    @Override
    public void processPeak(double mz, double intensity, List<PeakAnnotation> annotations)
    {
        // retain peaks too small or too large
        if (mz < tolerance.getMin(oxoniumMzs[0]) || mz > tolerance.getMax(oxoniumMzs[oxoniumMzs.length - 1])) {
            sink.processPeak(mz, intensity, annotations);
            return;
        }

        int index = Arrays.binarySearch(oxoniumMzs, mz);
        if (index < 0)
            index = -index - 1;

        // check for right value
        if ((index < oxoniumMzs.length) && tolerance.withinTolerance(oxoniumMzs[index], mz))
            return;

        // check for left value
        if (index > 0 && tolerance.withinTolerance(oxoniumMzs[index - 1], mz))
            return;

        // no match
        sink.processPeak(mz, intensity, annotations);
    }
}
