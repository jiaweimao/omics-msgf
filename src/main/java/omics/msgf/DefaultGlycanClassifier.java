/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf;

import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.spectra.MsnSpectrum;

/**
 * Use 138 and 204 to detect if a Spectrum is Glycan peptide spectrum.
 * <p>
 * It is better to use spectrum that has been remove noise.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jun 2018, 11:43 PM
 */
public class DefaultGlycanClassifier implements SpectrumClassifier
{
    private static final OxoniumMarker M138 = OxoniumDB.getMarkerOfName("HexNAc-2H2O-CH2O");
    private static final OxoniumMarker M204 = OxoniumDB.getMarkerOfName("HexNAc");

    private Tolerance iTol;

    public DefaultGlycanClassifier(Tolerance tol)
    {
        this.iTol = tol;
    }

    @Override
    public RawSpectrumType classify(MsnSpectrum spectrum)
    {
        int id138 = spectrum.getMostIntenseIndex(iTol.getMin(M138.getMz()), iTol.getMax(M138.getMz()));
        int id204 = spectrum.getMostIntenseIndex(iTol.getMin(M204.getMz()), iTol.getMax(M204.getMz()));

        return (id138 >= 0 || id204 >= 0) ? RawSpectrumType.Glyco_PEPTIDE : RawSpectrumType.PEPTIDE;
    }
}
