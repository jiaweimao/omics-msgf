/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf.gui;

import edu.ucsd.msjava.msdbsearch.ReverseDB;
import edu.ucsd.msjava.msdbsearch.SearchParams;
import edu.ucsd.msjava.msutil.*;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.StringConverter;
import omics.msgf.DeltaEntry;
import omics.msgf.task.ExportResultTask;
import omics.msgf.task.SearchTask;
import omics.util.io.FilenameUtils;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;
import omics.util.ms.peaklist.impl.PpmTolerance;
import omics.util.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TreeSet;

/**
 * @author JiaweiMao
 * @version 1.2.0
 * @since 11 Oct 2018, 12:34 PM
 */
public class OSearchController extends SearchController
{
    private static final Logger logger = LoggerFactory.getLogger(OSearchController.class);

    public ListView<String> msFileListNode;
    public TextField fastaField;

    public ChoiceBox<Enzyme> enzymeNode;
    public ChoiceBox<Integer> missedCleavageNode;
    public ChoiceBox<String> cleavageModeNode;

    public Spinner<Integer> minLenNode;
    public Spinner<Integer> maxLenNode;
    public TextField modfileText;
    /**
     * Consider protein N-term Met cleavage
     */
    public CheckBox nMChoose;
    public Spinner<Integer> minIso;
    public Spinner<Integer> maxIso;

    public TextArea logNode;
    public Spinner<Integer> threadNode;
    public CheckBox searchDecoyCheck;
    public ComboBox<Integer> isoformNode;

    public ComboBox<Integer> topN;
    public ComboBox<Integer> minDenovoNode;
    public CheckBox edgeScoreNode;
    public CheckBox moreFeaturesNode;

    public Spinner<Integer> minChargeNode;
    public Spinner<Integer> maxChargeNode;
    public ChoiceBox<Double> carrerMassNode;

    public ChoiceBox<ActivationMethod> activationNode;
    public ChoiceBox<InstrumentType> instruNode;
    public ChoiceBox<Protocol> protocolNode;

    public ComboBox<Integer> minPeakNode;
    public CheckBox isSearchGlycosyl;

    public TextField runParameterNode;
    public TextField editParameterNode;

    public ComboBox<Double> leftTolNode;
    public ComboBox<Double> rightTolNode;
    public ChoiceBox<String> parentUnitBox;

    public ComboBox<Double> leftFragTolNode;
    public ComboBox<Double> rightFragNode;
    public ChoiceBox<String> fragUnitNode;
    public TitledPane deltaEntryPane;

    public ComboBox<Integer> minRawScoreNode;
    public ComboBox<Double> maxEValueNode;
    public ComboBox<Integer> exportTopNNode;
    public ComboBox<Double> fdrNode;
    public TextField targetFileNode;
    public ListView<String> resultFileListNode;
    public CheckBox keepNonGlycanNode;
    public TabPane mainPane;
    public ListView<DeltaEntry> deltaList4Search;
    public ChoiceBox fdrType_Btn;

    private SearchParams editorParam = new SearchParams();
    private SearchParams runParams = new SearchParams();
    private SearchTask oSearchTask;
    private Window mainWindow;

    public OSearchController() { }

    public void setMainWindow(Window mainWindow)
    {
        this.mainWindow = mainWindow;
    }

    /**
     * update the editorParam according to UI nodes.
     */
    private void readUIParam()
    {
        File fastaFile = new File(fastaField.getText());
        editorParam.setDatabaseFile(fastaFile);
        editorParam.setUseTDA(searchDecoyCheck.isSelected());
        editorParam.setModFile(new File(modfileText.getText()));

        editorParam.setEnzyme(enzymeNode.getSelectionModel().getSelectedItem());
        editorParam.setMaxMissedCleavages(missedCleavageNode.getSelectionModel().getSelectedItem());
        editorParam.setNumTolerableTermini(cleavageModeNode.getSelectionModel().getSelectedIndex());

        editorParam.setMinPeptideLength(minLenNode.getValue());
        editorParam.setMaxPeptideLength(maxLenNode.getValue());
        editorParam.setIgnoreMetCleavage(!nMChoose.isSelected());

        editorParam.setMaxNumVariatsPerPeptide(isoformNode.getValue());

        double leftTol = leftTolNode.getValue();
        double rightTol = rightTolNode.getValue();
        Tolerance parentTol;
        int parentUnit = parentUnitBox.getSelectionModel().getSelectedIndex();
        if (parentUnit == 0) {
            parentTol = new PpmTolerance(leftTol, rightTol);
        } else {
            parentTol = new AbsoluteTolerance(leftTol, rightTol);
        }
        editorParam.setParentMassTolerance(parentTol);

        double fragLeftTol = leftFragTolNode.getValue();
        double fragRightTol = rightFragNode.getValue();
        Tolerance fragTol;
        if (fragUnitNode.getSelectionModel().getSelectedIndex() == 0) {
            fragTol = new PpmTolerance(fragLeftTol, fragRightTol);
        } else {
            fragTol = new AbsoluteTolerance(fragLeftTol, fragRightTol);
        }
        editorParam.setFragmentTolerance(fragTol);

        editorParam.setMinIsotopeError(minIso.getValue());
        editorParam.setMaxIsotopeError(maxIso.getValue());

        editorParam.setMinCharge(minChargeNode.getValue());
        editorParam.setMaxCharge(maxChargeNode.getValue());

        editorParam.setNumMatchesPerSpec(topN.getSelectionModel().getSelectedItem());
        editorParam.setMinDeNovoScore(minDenovoNode.getValue());

        editorParam.setInstType(instruNode.getSelectionModel().getSelectedItem());
        editorParam.setActivationMethod(activationNode.getSelectionModel().getSelectedItem());
        editorParam.setProtocol(protocolNode.getSelectionModel().getSelectedItem());
        editorParam.setChargeCarrierMass(carrerMassNode.getSelectionModel().getSelectedItem());
        editorParam.setDoNotUseEdgeScore(!edgeScoreNode.isSelected());
        editorParam.setOutputAdditionalFeatures(moreFeaturesNode.isSelected());

        editorParam.setMinNumPeaksPerSpectrum(minPeakNode.getSelectionModel().getSelectedItem());
        editorParam.setSearchGlycosyl(isSearchGlycosyl.isSelected());

        editorParam.getDeltaEntries().clear();
        ObservableList<DeltaEntry> items = deltaList4Search.getItems();
        for (DeltaEntry deltaEntry : items) {
            editorParam.addDeltaEntry(deltaEntry);
        }
    }

    /**
     * update the Parameter editor page
     */
    private void updateUI()
    {
        File databaseFile = editorParam.getDatabaseFile();
        if (databaseFile != null && databaseFile.exists()) {
            fastaField.setText(databaseFile.getAbsolutePath());
        }
        searchDecoyCheck.setSelected(editorParam.useTDA());

        File modFile = editorParam.getModFile();
        if (modFile != null && modFile.exists())
            modfileText.setText(modFile.getAbsolutePath());

        enzymeNode.setValue(editorParam.getEnzyme());
        missedCleavageNode.setValue(editorParam.getMaxMissedCleavages());
        cleavageModeNode.getSelectionModel().select(editorParam.getNumTolerableTermini());

        minLenNode.getValueFactory().setValue(editorParam.getMinPeptideLength());
        maxLenNode.getValueFactory().setValue(editorParam.getMaxPeptideLength());
        nMChoose.setSelected(!editorParam.ignoreMetCleavage());

        isoformNode.setValue(editorParam.getMaxNumVariatsPerPeptide());

        Tolerance parentTolerance = editorParam.getParentMassTolerance();
        Tolerance fragTolerance = editorParam.getFragmentTolerance();
        if (parentTolerance instanceof PpmTolerance) {
            parentUnitBox.getSelectionModel().select(0);
        } else {
            parentUnitBox.getSelectionModel().select(1);
        }
        if (fragTolerance instanceof PpmTolerance) {
            fragUnitNode.getSelectionModel().select(0);
        } else {
            fragUnitNode.getSelectionModel().select(1);
        }

        leftTolNode.setValue(parentTolerance.getMinusError());
        rightTolNode.setValue(parentTolerance.getPlusError());
        leftFragTolNode.setValue(fragTolerance.getMinusError());
        rightFragNode.setValue(fragTolerance.getPlusError());

        minIso.getValueFactory().setValue(editorParam.getMinIsotopeError());
        maxIso.getValueFactory().setValue(editorParam.getMaxIsotopeError());

        minChargeNode.getValueFactory().setValue(editorParam.getMinCharge());
        maxChargeNode.getValueFactory().setValue(editorParam.getMaxCharge());

        topN.setValue(editorParam.getNumMatchesPerSpec());
        minDenovoNode.setValue(editorParam.getMinDeNovoScore());

        instruNode.setValue(editorParam.getInstType());
        activationNode.setValue(editorParam.getActivationMethod());
        protocolNode.setValue(editorParam.getProtocol());

        carrerMassNode.setValue(editorParam.getChargeCarrierMass());
        edgeScoreNode.setSelected(!editorParam.doNotUseEdgeScore());
        moreFeaturesNode.setSelected(editorParam.outputAdditionalFeatures());

        minPeakNode.setValue(editorParam.getMinNumPeaksPerSpectrum());
        isSearchGlycosyl.setSelected(editorParam.isSearchGlycosyl());

        TreeSet<DeltaEntry> deltaEntries = editorParam.getDeltaEntries();
        deltaList4Search.getItems().addAll(deltaEntries);
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle)
    {
        msFileListNode.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        resultFileListNode.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        TextAreaAppender.setDelegateOutputStream(new TextAreaAppender.TextAreaOutputStream(logNode));

        int processors = Runtime.getRuntime().availableProcessors();
        int currentProcessor = processors / 2;
        if (currentProcessor < 1)
            currentProcessor = 1;
        threadNode.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, processors, currentProcessor));

        enzymeNode.setConverter(new StringConverter<Enzyme>()
        {
            @Override public String toString(Enzyme object)
            {
                return object.getName();
            }

            @Override public Enzyme fromString(String string)
            {
                return Enzyme.getEnzymeByName(string);
            }
        });
        enzymeNode.getItems().addAll(Enzyme.getAllRegisteredEnzymes());

        missedCleavageNode.getItems().addAll(0, 1, 2, 3, 4, 5);
        cleavageModeNode.getItems().addAll("non-specific", "semi", "full-specific");

        minLenNode.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(5, 40, 7, 2));
        maxLenNode.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(5, 100, 40, 5));

        StringConverter<Integer> intConverter = new StringConverter<Integer>()
        {
            @Override public String toString(Integer object)
            {
                return object.toString();
            }

            @Override public Integer fromString(String string)
            {
                return Integer.parseInt(string);
            }
        };

        isoformNode.setConverter(intConverter);
        isoformNode.getItems().add(128);

        parentUnitBox.getItems().addAll("ppm", "Da");
        fragUnitNode.getItems().addAll("ppm", "Da");
        StringConverter<Double> doubleConverter = new StringConverter<Double>()
        {
            @Override public String toString(Double object)
            {
                return object.toString();
            }

            @Override public Double fromString(String string)
            {
                return Double.parseDouble(string);
            }
        };
        leftTolNode.setConverter(doubleConverter);
        rightTolNode.setConverter(doubleConverter);
        leftFragTolNode.setConverter(doubleConverter);
        rightFragNode.setConverter(doubleConverter);

        leftTolNode.getItems().addAll(5., 10., 15., 20.);
        rightTolNode.getItems().addAll(5., 10., 15., 20.);
        leftFragTolNode.getItems().addAll(0.01, 0.05, 0.1, 0.5);
        rightFragNode.getItems().addAll(0.01, 0.05, 0.1, 0.5);

        topN.setConverter(intConverter);
        topN.getItems().addAll(1, 5, 10);

        minDenovoNode.setConverter(intConverter);
        minDenovoNode.getItems().addAll(Integer.MIN_VALUE, 0, 5, 10, 15, 20);

        minIso.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 2, 0));
        maxIso.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(0, 4, 1));

        minChargeNode.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 4, 2));
        maxChargeNode.setValueFactory(new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 6, 4));

        carrerMassNode.setConverter(doubleConverter);
        carrerMassNode.getItems().addAll(1.00727649, 22.98922189, 38.96315989);

        instruNode.setConverter(new StringConverter<InstrumentType>()
        {
            @Override
            public String toString(InstrumentType object)
            {
                return object.getName();
            }

            @Override
            public InstrumentType fromString(String string)
            {
                return InstrumentType.get(string);
            }
        });
        instruNode.getItems().addAll(InstrumentType.getAllRegisteredInstrumentTypes());
        activationNode.setConverter(new StringConverter<ActivationMethod>()
        {
            @Override public String toString(ActivationMethod object)
            {
                return object.getName();
            }

            @Override public ActivationMethod fromString(String string)
            {
                return ActivationMethod.get(string);
            }
        });
        activationNode.getItems().addAll(ActivationMethod.getAllRegisteredActivationMethods());
        protocolNode.setConverter(new StringConverter<Protocol>()
        {
            @Override public String toString(Protocol object)
            {
                return object.getName();
            }

            @Override public Protocol fromString(String string)
            {
                return Protocol.get(string);
            }
        });
        protocolNode.getItems().addAll(Protocol.getAllRegisteredProtocols());

        minPeakNode.getItems().addAll(10, 15, 20);

        deltaEntryPane.expandedProperty().setValue(false);

        minRawScoreNode.setConverter(intConverter);
        minRawScoreNode.getItems().addAll(0, 5, 10, 20);
        minRawScoreNode.setValue(0);

        maxEValueNode.setConverter(doubleConverter);
        maxEValueNode.getItems().addAll(0.01, 0.05);
        maxEValueNode.getSelectionModel().select(0);

        exportTopNNode.setConverter(intConverter);
        exportTopNNode.getItems().addAll(1);
        exportTopNNode.setValue(1);

        fdrNode.setConverter(doubleConverter);
        fdrNode.getItems().addAll(0.01, 0.05);
        fdrNode.getSelectionModel().select(0);

        updateUI();
    }

    public void onChooseFasta(ActionEvent actionEvent)
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Choose Fasta File");

        File file = chooser.showOpenDialog(mainWindow);
        if (file != null) {
            this.fastaField.setText(file.getAbsolutePath());
        }
    }

    public void onChooseMod(ActionEvent actionEvent)
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Choose Modification File");

        File file = chooser.showOpenDialog(mainWindow);
        if (file != null) {
            modfileText.setText(file.getAbsolutePath());
        }
    }

    /**
     * do database search
     */
    public void onSearch(ActionEvent actionEvent)
    {
        logNode.clear();

        List<DBSearchIOFiles> dbSearchIOList = runParams.getDBSearchIOList();
        if (dbSearchIOList != null)
            dbSearchIOList.clear();

        for (String file : msFileListNode.getItems()) {
            runParams.addSearchIOFile(new File(file));
        }
        runParams.setNumThreads(threadNode.getValue());
        runParams.parse();
        oSearchTask = new SearchTask(runParams);

        try {
            Thread thread = new Thread(oSearchTask);
            thread.setDaemon(true);
            thread.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
        oSearchTask.setOnSucceeded(event -> System.gc());
    }

    /**
     * save parameters to a file
     */
    @FXML
    public void onSaveParams(ActionEvent actionEvent)
    {
        if (minLenNode.getValue() > maxLenNode.getValue()) {
            showAlert(Alert.AlertType.ERROR, "Max peptide length must be larger than min peptide length");
            return;
        }

        if (minChargeNode.getValue() > maxChargeNode.getValue()) {
            showAlert(Alert.AlertType.ERROR, "Min charge must not be larger than max charge");
            return;
        }

        File database = new File(fastaField.getText());
        if (!database.exists()) {
            showAlert(Alert.AlertType.ERROR, "The fasta file is not exist!");
            return;
        }

        File modFile = new File(modfileText.getText());
        if (!modFile.exists()) {
            showAlert(Alert.AlertType.ERROR, "The mod file is not exist!");
            return;
        }

        readUIParam();

        FileChooser chooser = new FileChooser();
        chooser.setInitialFileName("standard.opm");
        File file = chooser.showSaveDialog(mainWindow);
        if (file != null) {
            editorParam.serialize(file.getAbsolutePath());
            editParameterNode.setText(file.getAbsolutePath());
        }
    }

    /**
     * read parameters file show in the parameter editor
     */
    @FXML
    public void onLoadEditorParam(ActionEvent actionEvent)
    {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("O-Search Parameter File", "*.opm"),
                new FileChooser.ExtensionFilter("All", "*.*"));
        File file = chooser.showOpenDialog(mainWindow);
        if (file != null) {
            editorParam.deserialize(file.getAbsolutePath());
            editParameterNode.setText(file.getAbsolutePath());
            updateUI();
        }
    }

    public void onCancel(ActionEvent actionEvent)
    {
        if (oSearchTask != null && oSearchTask.isRunning()) {
            oSearchTask.cancel();
            logger.info("The task is cancelled !!!");
        }
        System.gc();
    }

    /**
     * select parameter file in the Task editor
     */
    public void onSelectRunParamFile(ActionEvent actionEvent)
    {
        FileChooser chooser = new FileChooser();
        if (StringUtils.isNotEmpty(editParameterNode.getText())) {
            chooser.setInitialDirectory(new File(FilenameUtils.getFullPath(editParameterNode.getText())));
        }
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("O-Search Parameter File", "*.opm"),
                new FileChooser.ExtensionFilter("All", "*.*"));

        File file = chooser.showOpenDialog(mainWindow);
        if (file != null) {
            runParameterNode.setText(file.getAbsolutePath());
            runParams.deserialize(file.getAbsolutePath());

            File databaseFile = runParams.getDatabaseFile();
            if (!databaseFile.exists()) {
                showAlert(Alert.AlertType.ERROR, "Database file: " + databaseFile.getAbsolutePath() + " not exist!");
            }
            File modFile = runParams.getModFile();
            if (!modFile.exists()) {
                showAlert(Alert.AlertType.WARNING, "Mod file: " + modFile.getAbsolutePath() + " not exist!");
            }
        }
    }

    /**
     * process and export search result.
     */
    @FXML
    public void onExportResult(ActionEvent actionEvent)
    {
        String targetFilePath = targetFileNode.getText();
        if (StringUtils.isEmpty(targetFilePath.trim())) {
            showAlert(Alert.AlertType.ERROR, "The target file path is not valid");
            return;
        }

        Double fdrValue = fdrNode.getValue();
        if (fdrValue == null || fdrValue > 1 || fdrValue < 0) {
            showAlert(Alert.AlertType.ERROR, "fdr should in range [0,1]");
            return;
        }

        ObservableList<String> fileList = resultFileListNode.getItems();
        if (fileList == null || fileList.isEmpty()) {
            showAlert(Alert.AlertType.ERROR, "The file list is empty");
            return;
        }

        Double maxEValue = maxEValueNode.getValue();
        if (maxEValue == null || maxEValue < 0) {
            showAlert(Alert.AlertType.ERROR, "E-Value is not valid.");
            return;
        }

        Integer topN = exportTopNNode.getValue();
        Integer minRaw = minRawScoreNode.getValue();

        mainPane.getSelectionModel().select(1);

        ExportResultTask task = new ExportResultTask(fdrValue, maxEValue, minRaw, topN,
                keepNonGlycanNode.isSelected(), fileList, targetFilePath, ReverseDB.DECOY_PROTEIN_PREFIX);
        Thread thread = new Thread(task);
        thread.setDaemon(true);
        thread.start();
    }

    private void showAlert(Alert.AlertType type, String msg)
    {
        Alert alert = new Alert(type, msg);
        alert.showAndWait();
    }

    /**
     * select the target file to store result.
     */
    @FXML
    public void onSelectTargetResultFile(ActionEvent actionEvent)
    {
        FileChooser chooser = new FileChooser();
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Excel", "*.xlsx"),
                new FileChooser.ExtensionFilter("All", "*.*"));
        File file = chooser.showSaveDialog(mainWindow);
        if (file != null) {
            targetFileNode.setText(file.getAbsolutePath());
        }
    }

    @FXML
    public void onClearMSFile(ActionEvent actionEvent)
    {
        msFileListNode.getItems().clear();
    }

    /**
     * remove selected MS file from list
     */
    @FXML
    public void onRemoveSelectedMSFile(ActionEvent actionEvent)
    {
        ObservableList<String> selectedItems = msFileListNode.getSelectionModel().getSelectedItems();
        msFileListNode.getItems().removeAll(selectedItems);
    }

    /**
     * select MS files to search
     */
    @FXML
    public void onSelectMSFiles(ActionEvent actionEvent)
    {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Choose MS Files");

        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All", "*.*"),
                new FileChooser.ExtensionFilter("MGF", "*.mgf"),
                new FileChooser.ExtensionFilter("mzML", "*.mzML"),
                new FileChooser.ExtensionFilter("mzXML", "*.mzXML"),
                new FileChooser.ExtensionFilter("PKL", "*.pkl"));

        List<File> files = fileChooser.showOpenMultipleDialog(mainWindow);
        if (files != null) {
            for (File file : files) {
                msFileListNode.getItems().add(file.getAbsolutePath());
            }
        }
    }

    /**
     * add result file
     */
    @FXML
    public void onSelectResultFiles(ActionEvent actionEvent)
    {
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Select identification result file");
        chooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("MzIdentML File", "*.mzid"),
                new FileChooser.ExtensionFilter("All", "*.*"));

        List<File> files = chooser.showOpenMultipleDialog(mainWindow);
        if (files != null) {
            for (File file : files) {
                resultFileListNode.getItems().add(file.getAbsolutePath());
            }
        }
    }

    /**
     * remove the selected result file from list.
     */
    @FXML
    public void onRemoveResultFile(ActionEvent actionEvent)
    {
        ObservableList<String> selectedItems = resultFileListNode.getSelectionModel().getSelectedItems();
        resultFileListNode.getItems().removeAll(selectedItems);
    }

    /**
     * clear all result files in the list
     */
    @FXML
    public void clearResultFile(ActionEvent actionEvent)
    {
        resultFileListNode.getItems().clear();
    }

    /**
     * open the delta entry editing pane
     */
    @FXML
    public void editDeltaEntryList(ActionEvent actionEvent) throws IOException
    {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(this.getClass().getClassLoader().getResource("delta_entry.fxml"));
        Parent root = loader.load();

        DeltaEntryController controller = loader.getController();

        Scene scene = new Scene(root);
        Stage stage = new Stage();
        stage.setTitle("Select Delta Entries For Searching");
        stage.setScene(scene);
        stage.show();

        stage.setOnCloseRequest(event -> {
            if (!controller.finalEntryList.getItems().isEmpty()) {
                deltaList4Search.getItems().clear();
                deltaList4Search.getItems().addAll(controller.finalEntryList.getItems());
            }
        });

        if (!deltaList4Search.getItems().isEmpty())
            controller.finalEntryList.getItems().addAll(deltaList4Search.getItems());
    }
}
