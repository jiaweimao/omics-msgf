/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf.gui;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.scene.control.*;
import javafx.util.StringConverter;
import javafx.util.converter.IntegerStringConverter;
import omics.msgf.DeltaEntry;
import omics.msgf.DeltaEntryDatabase;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jan 2019, 8:47 AM
 */
public class DeltaEntryController extends SearchController
{
    public ChoiceBox<DeltaEntryDatabase.DB_TYPE> databaseButton;
    public ListView<DeltaEntry> databaseItemList;
    public ListView<DeltaEntry> selectedUnitList;
    public ListView<DeltaEntry> buildList;
    public ChoiceBox<Integer> maxUnitCount;
    public ListView<DeltaEntry> finalEntryList;
    public TextField nameField;
    public TextField massField;

    private boolean changed = false;
    private DeltaEntryDatabase database;

    @Override public void initialize(URL location, ResourceBundle resources)
    {
        super.initialize(location, resources);
        databaseButton.setConverter(new StringConverter<DeltaEntryDatabase.DB_TYPE>()
        {
            @Override public String toString(DeltaEntryDatabase.DB_TYPE object)
            {
                return object.getName();
            }

            @Override public DeltaEntryDatabase.DB_TYPE fromString(String string)
            {
                return DeltaEntryDatabase.DB_TYPE.valueOfName(string);
            }
        });
        databaseButton.getItems().addAll(DeltaEntryDatabase.DB_TYPE.values());
        databaseButton.getSelectionModel().select(DeltaEntryDatabase.DB_TYPE.O_CUSTOM);
        database = DeltaEntryDatabase.getInstance(DeltaEntryDatabase.DB_TYPE.O_CUSTOM);

        databaseItemList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        selectedUnitList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        buildList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        finalEntryList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        databaseItemList.getItems().addAll(database.getEntries());

        databaseButton.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            database = DeltaEntryDatabase.getInstance(newValue);
            databaseItemList.getItems().clear();
            databaseItemList.getItems().addAll(database.getEntries());
        });

        maxUnitCount.setConverter(new IntegerStringConverter());
        maxUnitCount.getItems().addAll(1, 2, 3);
        maxUnitCount.getSelectionModel().select(2);
    }

    /**
     * calculate the combinations of delta units.
     */
    public void build(ActionEvent actionEvent)
    {
        if (selectedUnitList.getItems().isEmpty())
            return;

        Integer countValue = maxUnitCount.getValue();
        List<DeltaEntry> entryList = DeltaEntry.permutate(selectedUnitList.getItems(), countValue);
        buildList.getItems().clear();
        buildList.getItems().addAll(entryList);
    }

    /**
     * add a delta unit from database to selected units
     */
    public void addDeltaUnit(ActionEvent actionEvent)
    {
        ObservableList<DeltaEntry> selectedItems = databaseItemList.getSelectionModel().getSelectedItems();
        if (selectedItems.isEmpty())
            return;

        selectedUnitList.getItems().addAll(selectedItems);
    }

    /**
     * remove selected delta units
     */
    public void removeDeltaUnit(ActionEvent actionEvent)
    {
        ObservableList<DeltaEntry> selectedItems = selectedUnitList.getSelectionModel().getSelectedItems();
        selectedUnitList.getItems().removeAll(selectedItems);
    }

    /**
     * add combinated delta entries to delta entry list
     */
    public void addBuildEntry2FinalList(ActionEvent actionEvent)
    {
        ObservableList<DeltaEntry> selectedItems = buildList.getSelectionModel().getSelectedItems();
        if (selectedItems.isEmpty())
            return;

        finalEntryList.getItems().addAll(selectedItems);
    }

    /**
     * add a custom delta entry to the final list.
     */
    public void addDeltaEntry(ActionEvent actionEvent)
    {
        String name = nameField.getText().trim();
        if (name.isEmpty())
            return;
        String massTxt = massField.getText().trim();
        if (massTxt.isEmpty())
            return;

        double mass;
        try {
            mass = Double.parseDouble(massTxt);
        } catch (NumberFormatException e) {
            return;
        }

        DeltaEntry entry = new DeltaEntry(name, mass);
        finalEntryList.getItems().add(entry);
    }

    public void removeDeltaEntry(ActionEvent actionEvent)
    {
        ObservableList<DeltaEntry> selectedItems = finalEntryList.getSelectionModel().getSelectedItems();
        if (selectedItems.isEmpty())
            return;

        finalEntryList.getItems().removeAll(selectedItems);
    }

    public void addUnit2DeltaEntries(ActionEvent actionEvent)
    {
        ObservableList<DeltaEntry> selectedItems = databaseItemList.getSelectionModel().getSelectedItems();
        if (selectedItems.isEmpty())
            return;

        finalEntryList.getItems().addAll(selectedItems);
    }

    /**
     * add a delta entry in the list to database, which helps to modified database.
     */
    public void addDeltaEntry2Database(ActionEvent actionEvent)
    {
        ObservableList<DeltaEntry> selectedItems = finalEntryList.getSelectionModel().getSelectedItems();
        if (selectedItems.isEmpty())
            return;

        databaseItemList.getItems().addAll(selectedItems);
        changed = true;
    }

    public void saveDatabaseChange(ActionEvent actionEvent)
    {
        if (!changed)
            return;

        Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure to save changes to " + database.getType().getName());
        Optional<ButtonType> buttonType = alert.showAndWait();
        if (!buttonType.isPresent() || !buttonType.get().equals(ButtonType.OK))
            return;

        List<DeltaEntry> entries = database.getEntries();
        entries.clear();
        entries.addAll(databaseItemList.getItems());
        database.serialize();
    }
}
