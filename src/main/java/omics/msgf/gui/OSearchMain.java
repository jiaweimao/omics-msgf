/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf.gui;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

import java.net.URL;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 11 Oct 2018, 12:43 PM
 */
public class OSearchMain extends Application
{
    public static final String VERSION = "1.3.3";

    @Override
    public void start(Stage primaryStage) throws Exception
    {
        URL url = getClass().getClassLoader().getResource("osearch_main.fxml");
        FXMLLoader loader = new FXMLLoader(url);
        Parent root = loader.load();

        OSearchController controller = loader.getController();
        controller.setMainWindow(primaryStage);

        Scene scene = new Scene(root);
        primaryStage.setScene(scene);
        primaryStage.setTitle("OMICS SEMI-OPEN (" + VERSION + ")");
        primaryStage.getIcons().add(new Image(OSearchMain.class.getClassLoader().getResourceAsStream("main_icon.png")));
        primaryStage.show();
    }
}
