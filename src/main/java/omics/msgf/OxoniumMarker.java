package omics.msgf;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jan 2019, 8:22 PM
 */
public class OxoniumMarker implements Comparable<OxoniumMarker>
{
    private final int id;
    private final String name;
    private final double mz;

    public OxoniumMarker(int id, String name, double mz)
    {
        this.id = id;
        this.name = name;
        this.mz = mz;
    }

    public int getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public double getMz()
    {
        return mz;
    }

    @Override public int compareTo(OxoniumMarker o)
    {
        return Double.compare(mz, o.mz);
    }
}
