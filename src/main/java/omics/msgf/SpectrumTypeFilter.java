/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf;

import omics.util.ms.spectra.MsnSpectrum;
import omics.util.ms.spectra.SpectrumFilter;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Jan 2019, 12:51 PM
 */
public class SpectrumTypeFilter implements SpectrumFilter
{
    private SpectrumClassifier classifier;
    private RawSpectrumType spectrumType;

    /**
     * Create a {@link SpectrumTypeFilter}
     *
     * @param classifier   {@link SpectrumClassifier} used to predicate the type of the spectrum
     * @param spectrumType {@link RawSpectrumType} to retain
     */
    public SpectrumTypeFilter(SpectrumClassifier classifier, RawSpectrumType spectrumType)
    {
        this.classifier = classifier;
        this.spectrumType = spectrumType;
    }

    @Override public boolean test(MsnSpectrum filterable)
    {
        RawSpectrumType type = classifier.classify(filterable);
        return type == spectrumType;
    }
}
