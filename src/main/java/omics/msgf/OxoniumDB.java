/*
 * Copyright 2017 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf;

import omics.util.io.xml.XMLUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.*;


/**
 * Database of Oxonium ions.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 06 Apr 2017, 10:32 PM
 */
public class OxoniumDB
{
    private static final Logger logger = LoggerFactory.getLogger(OxoniumDB.class);

    private static OxoniumDB instance;
    private final Map<String, OxoniumMarker> nameMap;
    private List<OxoniumMarker> markers;

    private OxoniumDB()
    {
        nameMap = new HashMap<>();
        markers = new ArrayList<>();

        try {
            parseOxoniumXml(new FileInputStream(new File("conf" + File.separator + "oxonium.xml")));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static OxoniumDB getInstance()
    {
        if (instance == null)
            instance = new OxoniumDB();
        return instance;
    }

    /**
     * Return the OxoniumMarker of given name
     *
     * @param name marker name
     * @return {@link OxoniumMarker} of the name
     */
    public static OxoniumMarker getMarkerOfName(String name)
    {
        OxoniumDB db = OxoniumDB.getInstance();
        return db.getMarker(name);
    }

    private void parseOxoniumXml(InputStream stream)
    {
        try {
            XMLInputFactory factory = XMLInputFactory.newFactory();
            XMLStreamReader reader = factory.createXMLStreamReader(stream);

            while (reader.hasNext()) {
                reader.next();

                if (XMLUtils.isStartElement(reader, "oxonium")) {
                    int id = XMLUtils.intAttr(reader, "id");
                    String name = reader.getAttributeValue(null, "name");
                    double mz = XMLUtils.doubleAttr(reader, "mz");
                    OxoniumMarker marker = new OxoniumMarker(id, name, mz);
                    markers.add(marker);
                    nameMap.put(name, marker);
                }
            }

            reader.close();
        } catch (XMLStreamException e) {
            logger.error("Error while parsing the oxonium.xml file.");
            e.printStackTrace();
        }
    }


    /**
     * Get the OxoniumMarker with given id.
     *
     * @param name the unique name of {@link OxoniumMarker} object.
     * @return a {@link OxoniumMarker} object.
     */
    public OxoniumMarker getMarker(String name)
    {
        return nameMap.get(name);
    }

    /**
     * @return size of this Oxonium map size.
     */
    public int size()
    {
        return markers.size();
    }

    /**
     * Return an unmodifiable list of all Oxonium markers.
     *
     * @return oxonium marker list.
     */
    public List<OxoniumMarker> getMarkers()
    {
        return Collections.unmodifiableList(markers);
    }
}
