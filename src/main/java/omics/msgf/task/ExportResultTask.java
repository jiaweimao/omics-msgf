/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf.task;

import com.google.common.collect.ArrayListMultimap;
import javafx.concurrent.Task;
import omics.pdk.IdentResult;
import omics.pdk.ScoreType;
import omics.pdk.SpectrumIdentifier;
import omics.pdk.io.OmicsExcelWriter;
import omics.pdk.matches.PeptideSpectrumMatch;
import omics.pdk.parameters.IdentParameters;
import omics.pdk.parameters.PSMParameter;
import omics.pdk.pia.DoProteinInference;
import omics.pdk.pia.GroupType;
import omics.pdk.psm.consumer.AddLogEValue;
import omics.pdk.psm.filter.DecoyProteinFilter;
import omics.pdk.psm.filter.PSMScoreFilter;
import omics.pdk.psm.score.AddPSMQValue;
import omics.pdk.psm.score.AddPeptideQValue;
import omics.util.ms.spectra.SpectraData;
import omics.util.utils.DelegateFilter;
import omics.util.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Comparator;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 24 Oct 2018, 10:27 AM
 */
public class ExportResultTask extends Task<Void>
{
    private static final Logger logger = LoggerFactory.getLogger(ExportResultTask.class);

    private double fdr;
    private double maxEValue;
    private int minRawScore;
    private int topN;
    private boolean keepNonGlycan;
    private List<String> fileList;
    private String targetFile;
    private String decoyTag;

    public ExportResultTask(double fdr, double maxEValue, int minRawScore, int topN, boolean keepNonGlycan, List<String> fileList, String targetFile, String decoyTag)
    {
        this.fdr = fdr;
        this.maxEValue = maxEValue;
        this.minRawScore = minRawScore;
        this.topN = topN;
        this.keepNonGlycan = keepNonGlycan;
        this.fileList = fileList;
        this.targetFile = targetFile;
        this.decoyTag = decoyTag;
    }

    @Override
    protected Void call() throws Exception
    {
        logger.info("Configuration: ");
        logger.info("FDR={}", fdr);
        logger.info("Max E-value={}", maxEValue);
        logger.info("Min RawScore={}", minRawScore);
        logger.info("TopN={}", topN);
        logger.info("Keep Non-glycan PSM: {}", keepNonGlycan);
        logger.info("decoy tag={}", decoyTag);
        logger.info("File list:");
        for (String file : fileList) {
            logger.info("\t" + file);
        }

        DelegateFilter<PeptideSpectrumMatch> filter = new DelegateFilter<>();
        filter.addFilter(new PSMScoreFilter(minRawScore, ScoreType.MSGF_RAW_SCORE))
                .addFilter(new PSMScoreFilter(maxEValue, ScoreType.MSGF_EVALUE));
        if (!keepNonGlycan) {
            filter.addFilter(filterable -> {
                String delta_entry = filterable.getPMString("Delta Entry").trim();
                return StringUtils.isNotEmpty(delta_entry);
            });
        }

        ArrayListMultimap<SpectrumIdentifier, PeptideSpectrumMatch> psmMap = ArrayListMultimap.create();
        IdentParameters parameters = null;
        for (String file : fileList) {
            IdentResult result = IdentResult.read(new File(file), decoyTag);
            result.apply(filter);

            List<SpectraData> spectraDataList = result.getParameters().getSpectraDataList();
            for (SpectraData spectraData : spectraDataList) {
                spectraData.setId(spectraData.getFileName());
            }

            if (parameters == null)
                parameters = result.getParameters();
            else {
                parameters.addSpectraData(result.getParameters().getSpectraData());
            }

            for (PeptideSpectrumMatch psm : result) {
                psmMap.put(psm.getIdentifier(), psm);
            }
        }

        Comparator<PeptideSpectrumMatch> scoreComp = Comparator.comparingDouble(o -> o.getDoubleScore(ScoreType.MSGF_SPEC_EVALUE));

        IdentResult identResult = new IdentResult("O-Search Identification Result");
        identResult.setParameters(parameters);
        for (SpectrumIdentifier identifier : psmMap.keySet()) {
            List<PeptideSpectrumMatch> psmList = psmMap.get(identifier);
            if (psmList.size() > topN) {
                psmList.sort(scoreComp);
                int rank = 0;
                double preEValue = Double.NaN;
                for (PeptideSpectrumMatch psm : psmList) {
                    Double eValue = psm.getDoubleScore(ScoreType.MSGF_SPEC_EVALUE);
                    if (eValue != preEValue)
                        rank++;
                    psm.setRank(rank);
                    preEValue = eValue;
                    if (rank <= topN)
                        identResult.add(psm);
                }
            } else {
                identResult.addAll(psmList);
            }
        }

        PSMParameter psmParameter = parameters.getPSMParameter();
        psmParameter.addScore(ScoreType.MSGF_EVALUE, maxEValue);
        psmParameter.addScore(ScoreType.MSGF_RAW_SCORE, minRawScore);
        psmParameter.addScore(ScoreType.PSM_QVALUE, fdr);

        identResult.apply(new AddLogEValue(ScoreType.MSGF_SPEC_EVALUE));
        identResult.apply(new AddPSMQValue(ScoreType.LOG_EVALUE));
        identResult.apply(new AddPeptideQValue(ScoreType.LOG_EVALUE));
        identResult.apply(new PSMScoreFilter(fdr, ScoreType.PSM_QVALUE));
        identResult.apply(new DecoyProteinFilter());
        identResult.rebuildReference();

        if (identResult.size() > 0) {
            identResult.apply(new DoProteinInference(GroupType.OccamsRazor, ScoreType.LOG_EVALUE));
        }

        OmicsExcelWriter writer = new OmicsExcelWriter(identResult, targetFile);
        writer.messageProperty().addListener((observable, oldValue, newValue) -> logger.info(newValue));
        writer.start();
        writer.close();

        return null;
    }
}
