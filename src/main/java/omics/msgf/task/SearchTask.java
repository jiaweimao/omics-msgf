package omics.msgf.task;

import edu.ucsd.msjava.msdbsearch.SearchParams;
import edu.ucsd.msjava.ui.MSGFPlus;
import javafx.concurrent.Task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jan 2019, 8:38 PM
 */
public class SearchTask extends Task<Void>
{
    private static final Logger logger = LoggerFactory.getLogger(SearchTask.class);
    private SearchParams params;

    public SearchTask(SearchParams params)
    {
        this.params = params;
    }

    @Override protected Void call()
    {
        String s = MSGFPlus.runMSGFPlus(params);
        if (s != null)
            logger.info(s);
        return null;
    }
}
