/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package omics.msgf;

import omics.util.io.xml.IdentingXMLStreamWriter;
import omics.util.io.xml.XMLUtils;
import omics.util.utils.NumberFormatFactory;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 28 Jan 2019, 12:53 PM
 */
public class DeltaEntryDatabase
{
    public enum DB_TYPE
    {
        O_COMMON("Common O-Glycans", "delta_oglycans_common.xml"),
        O_CUSTOM("Custom O-Glycans", "delta_oglycans_custom.xml"),
        O_HUMAN("Human O-Glycans", "delta_oglycans_human.xml"),
        O_MAMMALIAN("Mammalian O-Glycans", "delta_oglycans_mammalian.xml");

        private String name;
        private String fileName;

        DB_TYPE(String name, String fileName)
        {
            this.name = name;
            this.fileName = fileName;
        }

        DB_TYPE(String name)
        {
            this.name = name;
        }

        public String getName()
        {
            return name;
        }

        public String getFileName()
        {
            return fileName;
        }

        /**
         * Return the {@link DB_TYPE} of given database name
         *
         * @param name database name
         */
        public static DB_TYPE valueOfName(String name)
        {
            for (DB_TYPE value : DB_TYPE.values()) {
                if (value.getName().equals(name))
                    return value;
            }
            return null;
        }
    }

    private List<DeltaEntry> entries;
    private DB_TYPE type;

    private static DeltaEntryDatabase database;

    public static DeltaEntryDatabase getInstance(DB_TYPE type)
    {
        if (database == null || database.getType() != type)
            database = new DeltaEntryDatabase(type);

        return database;
    }

    public DeltaEntryDatabase(DB_TYPE type)
    {
        this.type = type;
        try {
            this.entries = deserialize(new File("conf", type.getFileName()).getAbsolutePath());
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<DeltaEntry> getEntries()
    {
        return entries;
    }

    public DB_TYPE getType()
    {
        return type;
    }

    public void serialize(String path)
    {
        if (entries.isEmpty())
            return;

        try {
            serialize(entries, path);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    /**
     * serialize to its original path
     */
    public void serialize()
    {
        String path = new File("conf", type.getFileName()).getAbsolutePath();
        serialize(path);
    }


    private static void serialize(List<DeltaEntry> entries, String path) throws IOException, XMLStreamException
    {
        entries.sort(Comparator.naturalOrder());
        IdentingXMLStreamWriter writer = new IdentingXMLStreamWriter(new File(path));
        writer.writeStartDocument();
        writer.writeStartElement("delta_entry_list");

        for (DeltaEntry item : entries) {
            writer.writeEmptyElement("delta_entry");
            writer.writeAttribute("name", item.getName());
            writer.writeAttribute("mass", NumberFormatFactory.MASS_PRECISION.format(item.getMolecularMass()));
        }

        writer.writeEndElement();
        writer.writeEndDocument();
        writer.close();
    }

    private static ArrayList<DeltaEntry> deserialize(String path) throws XMLStreamException, FileNotFoundException
    {
        ArrayList<DeltaEntry> entries = new ArrayList<>();

        XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        XMLStreamReader xmlReader = xmlInputFactory.createXMLStreamReader(new FileReader(path));
        while (xmlReader.hasNext()) {
            int event = xmlReader.next();
            if (event == XMLStreamConstants.START_ELEMENT) {
                String localName = xmlReader.getLocalName();
                if (localName.equals("delta_entry")) {
                    String name = xmlReader.getAttributeValue(null, "name");
                    double mass = XMLUtils.doubleAttr(xmlReader, "mass");
                    DeltaEntry entry = new DeltaEntry(name, mass);

                    entries.add(entry);
                }
            }
        }

        entries.sort(Comparator.naturalOrder());
        return entries;
    }
}
