package omics.msgf;

import it.unimi.dsi.fastutil.ints.IntArrayList;
import it.unimi.dsi.fastutil.ints.IntList;
import omics.util.chem.Weighable;
import omics.util.math.MathUtils;
import omics.util.utils.NumberFormatFactory;
import omics.util.utils.Pair;
import omics.util.utils.StringUtils;

import java.util.*;

import static omics.util.utils.ObjectUtils.checkArgument;
import static omics.util.utils.ObjectUtils.checkNotNull;

/**
 * A delta entry value.
 *
 * @author JiaweiMao
 * @version 1.1.0
 * @since 21 Jan 2019, 5:53 PM
 */
public class DeltaEntry extends Pair<String, Double> implements Comparable<DeltaEntry>, Weighable
{
    public static double DELTA_MASS = 0.01;
    public static final DeltaEntry EMPTY = new DeltaEntry("", 0);

    /**
     * Generate combinations of {@link DeltaEntry}s in the <code>deltaEntries</code>.
     * DeltaEntrys in different combinations are separated with ';'
     *
     * @param deltaEntries {@link DeltaEntry} to permutate
     * @param maxCount     max number of {@link DeltaEntry} allowed to joint together
     */
    public static List<DeltaEntry> permutate(List<DeltaEntry> deltaEntries, int maxCount)
    {
        checkNotNull(deltaEntries);
        checkArgument(deltaEntries.size() > 0);
        checkArgument(maxCount > 0);

        IntList ids = new IntArrayList();
        // same type combination
        for (int i = 0; i < deltaEntries.size(); i++) {
            for (int count = 0; count < maxCount; count++)
                ids.add(i);
        }

        List<int[]> combinations = new ArrayList<>();
        int[] indexes = ids.toIntArray();
        for (int count = 1; count <= maxCount; count++) {
            List<int[]> ints = MathUtils.permutationDup(indexes, count);
            combinations.addAll(ints);
        }

        Map<Integer, DeltaEntry> nominalMassMap = new HashMap<>();
        for (int[] group : combinations) {
            double mass = 0;
            Map<Integer, Integer> countMap = new HashMap<>();
            for (int id : group) {
                countMap.put(id, countMap.getOrDefault(id, 0) + 1);
                mass += deltaEntries.get(id).getMolecularMass();
            }

            StringBuilder builder = new StringBuilder();
            List<Integer> keys = new ArrayList<>(countMap.keySet());
            keys.sort(Comparator.naturalOrder());
            for (Integer key : keys) {
                Integer count = countMap.get(key);
                builder.append(deltaEntries.get(key).getName()).append('{').append(count).append('}');
            }
            String name = builder.toString();

            int key = (int) (mass / DELTA_MASS);

            DeltaEntry item = nominalMassMap.get(key);
            if (item != null)
                name = item.getName() + StringUtils.SEP1 + name;

            DeltaEntry newIt = new DeltaEntry(name, mass);
            nominalMassMap.put(key, newIt);
        }
        List<DeltaEntry> glycanItemList = new ArrayList<>(nominalMassMap.values());
        glycanItemList.sort(Comparator.comparingDouble(DeltaEntry::getMolecularMass));

        return glycanItemList;
    }


    /**
     * Create a deltaEntry
     *
     * @param name  entry name
     * @param entry delta value
     */
    public DeltaEntry(String name, double entry)
    {
        super(name, entry);
    }

    /**
     * @return name of this delta entry
     */
    public String getName()
    {
        return getFirst();
    }

    @Override
    public int compareTo(DeltaEntry o)
    {
        return Double.compare(this.getSecond(), o.getSecond());
    }

    @Override
    public double getMolecularMass()
    {
        return getSecond();
    }

    @Override
    public String toString()
    {
        return getFirst() + " (" + NumberFormatFactory.MASS_PRECISION.format(getSecond()) + ")";
    }
}
