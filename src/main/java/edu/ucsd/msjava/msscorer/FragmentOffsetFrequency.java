package edu.ucsd.msjava.msscorer;

import edu.ucsd.msjava.msutil.IonType;

public class FragmentOffsetFrequency implements Comparable<FragmentOffsetFrequency>
{
    private IonType ionType;
    private float frequency;

    /**
     * Create a fragment offset frequency
     *
     * @param ionType   {@link IonType}
     * @param frequency frequency
     */
    public FragmentOffsetFrequency(IonType ionType, float frequency)
    {
        this.ionType = ionType;
        this.frequency = frequency;
    }

    /**
     * @return the {@link IonType} of this fragment offset frequency
     */
    public IonType getIonType()
    {
        return ionType;
    }

    /**
     * @return frequency of this fragment, which is a ratio in range [0, 1], the larger, the
     */
    public float getFrequency()
    {
        return frequency;
    }

    @Override public String toString()
    {
        return "Fragment OFF{" +
                "ionType=" + ionType +
                ", frequency=" + frequency +
                '}';
    }

    public int compareTo(FragmentOffsetFrequency o)
    {
        return Float.compare(this.frequency, o.frequency);
    }
}
