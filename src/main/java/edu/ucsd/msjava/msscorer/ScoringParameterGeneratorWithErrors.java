package edu.ucsd.msjava.msscorer;

import edu.ucsd.msjava.msgf.Histogram;
import edu.ucsd.msjava.msgf.IntHistogram;
import edu.ucsd.msjava.msgf.NominalMass;
import edu.ucsd.msjava.msscorer.NewScorerFactory.SpecDataType;
import edu.ucsd.msjava.msutil.*;
import edu.ucsd.msjava.msutil.IonType.PrefixIon;
import edu.ucsd.msjava.parser.MgfSpectrumParser;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;
import omics.util.utils.Pair;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * @author sangtaekim
 */
public class ScoringParameterGeneratorWithErrors extends NewRankScorer
{
    private static final float MIN_PRECURSOR_OFFSET = -300;    // for precursors
    private static final float MAX_PRECURSOR_OFFSET = 30;
    /**
     * minimum number of spectra required for a partition
     */
    private static final int MIN_NUM_SPECTRA_PER_PARTITION = 400;
    private static final int MIN_NUM_SPECTRA_FOR_PRECURSOR_OFF = 150;
    private static final int MAX_NUM_PARTITIONS_PER_CHARGE = 30;    // 30

    private static final float MIN_PRECURSOR_OFFSET_PROBABILITY = 0.15f;    // 0.15
    private static final float MIN_ION_OFFSET_PROBABILITY = 0.15f;    // 0.15, for ion types
    private static final float MIN_MAIN_ION_OFFSET_PROBABILITY = 0.01f;    // ions with probabilities below this number will be ignored

    private static final int MAX_RANK = 150;
    private static final int NUM_SEGMENTS_PER_SPECTRUM = 2;

    private static final int[] smoothingRanks = {3, 5, 10, 20, 50, Integer.MAX_VALUE}; //Ranks around which smoothing occurs
    private static final int[] smoothingWindowSize = {0, 1, 2, 3, 4, 5}; //Smoothing windows for each smoothing rank

    private static final float DECONVOLUTION_MASS_TOLERANCE = 0.02f;

    public static void generateParameters(
            File specFile,
            SpecDataType dataType,
            AminoAcidSet aaSet,
            File outputDir,
            boolean singlePartition
    )
    {
        SpectraContainer container = new SpectraContainer(specFile.getPath(), new MgfSpectrumParser().aaSet(aaSet));
        generateParameters(container, dataType, aaSet, outputDir, singlePartition);
    }

    public static void generateParameters(
            SpectraContainer container,
            SpecDataType dataType,
            AminoAcidSet aaSet,
            File outputDir)
    {
        generateParameters(container, dataType, aaSet, outputDir, false);
    }

    /**
     * Generate scoring parameters of given information
     *
     * @param container       a spectra container contains all annotated spectra
     * @param dataType        the {@link SpecDataType}
     * @param aaSet
     * @param outputDir       the directory to writer the parameter file
     * @param singlePartition true if create only one partition for each charge
     */
    public static void generateParameters(
            SpectraContainer container,
            SpecDataType dataType,
            AminoAcidSet aaSet,
            File outputDir,
            boolean singlePartition)
    {
        logger.info("Number of annotated PSMs: {}", container.size());

        String paramFileName = dataType.toString() + ".param";

        File outputFile;
        if (outputDir != null)
            outputFile = new File(outputDir, paramFileName);
        else
            outputFile = new File(paramFileName);
        logger.info("Output file name: ", outputFile.getAbsolutePath());

        int errorScalingFactor = 0;
        boolean applyDeconvolution = false;
        if (dataType.getInstrumentType() == InstrumentType.HIGH_RESOLUTION_LTQ
                || dataType.getInstrumentType() == InstrumentType.TOF
                || dataType.getInstrumentType().isHighResolution()) {
            errorScalingFactor = 100;
            applyDeconvolution = true;
            logger.info("High-precision MS/MS data: errorScalingFactor({}) chargeDeconvolution({})", errorScalingFactor, applyDeconvolution);
        }

        boolean considerPhosLoss = false;
        if (dataType.getProtocol().getName().equals("Phosphorylation")) {
            considerPhosLoss = true;
            logger.info("Consider H3PO4 loss.");
        }

        boolean consideriTRAQLoss = false;
        if (dataType.getProtocol().getName().equals("iTRAQ")) {
            consideriTRAQLoss = true;
            logger.info("Consider iTRAQ loss.");
        }

        boolean considerTMTLoss = false;
        if (dataType.getProtocol().getName().equals("TMT")) {
            considerTMTLoss = true;
            logger.info("Consider TMT loss.");
        }

        if (dataType.getProtocol().getName().equals("iTRAQPhospho")) {
            considerPhosLoss = true;
            consideriTRAQLoss = true;
            logger.info("Consider iTRAQ and H3PO4 loss.");
        }

        HashSet<String> pepSet = new HashSet<>(); // all peptides
        for (Spectrum spec : container)
            pepSet.add(spec.getAnnotationStr());
        logger.info("Number of unique peptides: {}", pepSet.size());

        int numSpecsPerPeptide; // 每个肽段每个价态采用的谱图数目
        if (pepSet.size() < 2000) {
            numSpecsPerPeptide = 3;
        } else {
            numSpecsPerPeptide = 1;
        }
        logger.info("Consider {} per spectrum.", numSpecsPerPeptide);

        // multiple spectra with the same peptide -> one spec per peptide
        HashMap<String, ArrayList<Spectrum>> pepSpecMap = new HashMap<>(); // peptide+charge -> spectrum
        for (Spectrum spec : container) {
            if (spec.getAnnotationStr() == null)
                continue;
            String pep = spec.getAnnotationStr() + ":" + spec.getCharge();
            ArrayList<Spectrum> specList = pepSpecMap.computeIfAbsent(pep, k -> new ArrayList<>());
            if (specList.size() < numSpecsPerPeptide)
                specList.add(spec);
        }

        SpectraContainer specContOnePerPep = new SpectraContainer();
        for (ArrayList<Spectrum> specList : pepSpecMap.values()) {
            specContOnePerPep.addAll(specList);
        }

        ScoringParameterGeneratorWithErrors gen = new ScoringParameterGeneratorWithErrors(
                specContOnePerPep,
                dataType,
                considerPhosLoss,
                consideriTRAQLoss,
                considerTMTLoss,
                applyDeconvolution);

        // set up the tolerance
        gen.tolerance(new AbsoluteTolerance(0.5f));

        // Step 1: partition spectra
        if (singlePartition)
            gen.partition(NUM_SEGMENTS_PER_SPECTRUM, true);
        else
            gen.partition(NUM_SEGMENTS_PER_SPECTRUM, false);
        logger.info("Partition: {}", gen.partitionSet.size());

        // Step 2: compute offset frequency functions of precursor peaks and their neutral losses
        gen.precursorOFF(MIN_PRECURSOR_OFFSET_PROBABILITY);
        logger.info("PrecursorOFF Done.");

        // Step 3: filter out "significant" precursor offsets
        gen.filterPrecursorPeaks();
        logger.info("Filtering Done.");

        if (applyDeconvolution) {
            gen.deconvoluteSpectra();
            logger.info("Deconvolution Done.");
        }

        // Step 4: compute offset frequency function of fragment peaks and determine ion types to be considered for scoring
        gen.selectIonTypes();
        logger.info("Ion types selected.");

        // Step 5: compute rank distributions
        gen.generateRankDist(MAX_RANK);
        logger.info("Rank distribution computed.");

        // Step 6 (optional): generate error distribution
        gen.generateErrorDist(errorScalingFactor);
        logger.info("Error distribution computed");

        // Step 7: smoothing parameters
        gen.smoothing();
        logger.info("Smoothing complete.");

        // output
        try {
            gen.writeParameters(outputFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        gen.writeParametersPlainText(new File(outputFile.getPath() + ".txt"));

        logger.info("Writing Done.");
    }

    // Required
    private SpectraContainer specContainer;
    private final boolean considerPhosLoss;
    private final boolean consideriTRAQLoss;
    private final boolean considerTMTLoss;

    /**
     * Create a {@link ScoringParameterGeneratorWithErrors}
     *
     * @param specContainer      annotated spectra used
     * @param dataType           {@link SpecDataType} to use
     * @param considerPhosLoss   true if consider phospho neutral loss
     * @param consideriTRAQLoss  true if consider iTRAQ loss
     * @param considerTMTLoss    true if consider TMT loss
     * @param applyDeconvolution true if apply deconvolution
     */
    public ScoringParameterGeneratorWithErrors(SpectraContainer specContainer, SpecDataType dataType, boolean considerPhosLoss,
                                               boolean consideriTRAQLoss, boolean considerTMTLoss, boolean applyDeconvolution)
    {
        this.specContainer = specContainer;
        this.considerPhosLoss = considerPhosLoss;
        this.consideriTRAQLoss = consideriTRAQLoss;
        this.considerTMTLoss = considerTMTLoss;
        super.dataType = dataType;
        super.applyDeconvolution = applyDeconvolution;
        super.deconvolutionErrorTolerance = DECONVOLUTION_MASS_TOLERANCE;
    }

    /**
     * Generate partitions
     *
     * @param numSegments     number of partitions to use
     * @param singlePartition true if create only one partition for each charge
     */
    private void partition(int numSegments, boolean singlePartition)
    {
        super.numSegments = numSegments;
        chargeHist = new Histogram<>();
        partitionSet = new TreeSet<>();

        Hashtable<Integer, ArrayList<Float>> parentMassMap = new Hashtable<>(); // charge -> parent mass list
        for (Spectrum spec : specContainer) {
            int charge = spec.getCharge();
            if (charge <= 0) // ignore negative charges
                continue;
            chargeHist.add(charge);
            if (spec.getAnnotation() != null) {
                ArrayList<Float> precursorList = parentMassMap.computeIfAbsent(charge, k -> new ArrayList<>());
                precursorList.add(spec.getParentMass());
            }
        }

        for (int c = chargeHist.minKey(); c <= chargeHist.maxKey(); c++) {

            ArrayList<Float> parentMassList = parentMassMap.get(c);
            if (parentMassList == null)
                continue;

            int numSpec = parentMassList.size();
            if (numSpec < Math.round(MIN_NUM_SPECTRA_PER_PARTITION * 0.9f))    // to few spectra
                continue;

            int partitionSize = Math.max(numSpec / MAX_NUM_PARTITIONS_PER_CHARGE, MIN_NUM_SPECTRA_PER_PARTITION);

            Collections.sort(parentMassList);
            int bestSetSize = 0; // best set size for a partition

            if (singlePartition)
                bestSetSize = numSpec;
            else {
                int smallestRemainder = partitionSize;
                for (int i = Math.round(partitionSize * 0.9f); i <= Math.round(partitionSize * 1.1f); i++) {
                    int remainder = numSpec % i;
                    if (i - remainder < remainder)
                        remainder = i - remainder;
                    if (remainder < smallestRemainder || (remainder == smallestRemainder && Math.abs(partitionSize - i) < Math.abs(partitionSize - bestSetSize))) {
                        bestSetSize = i;
                        smallestRemainder = remainder;
                    }
                }
            }
            int num = 0;
            for (int i = 0; i == 0 || i < Math.round(numSpec / (float) bestSetSize); i++) {
                if (num != 0) {
                    for (int seg = 0; seg < numSegments; seg++)
                        partitionSet.add(new Partition(c, parentMassList.get(num), seg));
                } else {
                    for (int seg = 0; seg < numSegments; seg++)
                        partitionSet.add(new Partition(c, 0f, seg));
                }
                num += bestSetSize;
            }
        }
    }

    public static void main(String[] args)
    {
        for (int i = 10; i >= 2; i--) {
            System.out.println(i);
        }
    }

    /**
     * 2. compute offset frequency functions of precursor peaks and their neutral losses
     *
     * @param minProbThreshold minimum of the probability
     */
    private void precursorOFF(float minProbThreshold)
    {
        if (chargeHist == null) {
            assert (false) : "partition() must have been called before";
            return;
        }
        precursorOFFMap = new TreeMap<>();
        numPrecurOFF = 0;

        for (int charge = chargeHist.minKey(); charge <= chargeHist.maxKey(); charge++) {
            if (chargeHist.get(charge) < MIN_NUM_SPECTRA_FOR_PRECURSOR_OFF)
                continue;
            ArrayList<PrecursorOffsetFrequency> precursorOffsetList = new ArrayList<>();
            int numSpecs = 0;
            Hashtable<Integer, Histogram<Integer>> histList = new Hashtable<>();
            for (int c = charge; c >= 2; c--) // get value in range [2, charge]
                histList.put(c, new Histogram<>());

            for (Spectrum spec : specContainer) {
                if (spec.getAnnotation() == null)
                    continue;
                if (spec.getCharge() != charge)
                    continue;

                numSpecs++;
                spec = filter.apply(spec);
                float precursorNeutralMass = spec.getParentMass();
                for (int c = charge; c >= 2; c--) {
                    float precursorMz = (precursorNeutralMass + c * (float) Composition.ChargeCarrierMass()) / c;
                    float min = precursorMz + MIN_PRECURSOR_OFFSET / (float) c;
                    float max = precursorMz + MAX_PRECURSOR_OFFSET / (float) c;
                    float minDelta = (float) (min - mme.getMin(min));
                    float maxDelta = (float) (mme.getMax(max) - max);
                    ArrayList<Peak> peakList = spec.getPeakListByMassRange(
                            precursorMz + MIN_PRECURSOR_OFFSET / (float) c - minDelta / 2,
                            precursorMz + MAX_PRECURSOR_OFFSET / (float) c + maxDelta / 2);

                    int prevMassIndexDiff = Integer.MIN_VALUE;
                    for (Peak p : peakList) {
                        float peakMass = p.getMz();
                        int massIndexDiff = NominalMass.toNominalMass(peakMass - precursorMz);
                        if (massIndexDiff > prevMassIndexDiff) {
                            histList.get(c).add(massIndexDiff);
                            prevMassIndexDiff = massIndexDiff;
                        }
                    }
                }
            }

            for (int c = charge; c >= 2; c--) {
                ArrayList<Integer> keyList = new ArrayList<>(histList.get(c).keySet());
                Collections.sort(keyList);
                for (Integer key : keyList) {
                    float prob = (histList.get(c).get(key)) / (float) numSpecs;
                    if (prob > minProbThreshold) {
                        precursorOffsetList.add(new PrecursorOffsetFrequency((charge - c), NominalMass.getMassFromNominalMass(key), prob));
                    }
                }
            }
            precursorOFFMap.put(charge, precursorOffsetList);
            numPrecurOFF += precursorOffsetList.size();
        }
    }

    private void filterPrecursorPeaks()
    {
        if (this.precursorOFFMap == null)
            return;
        for (Spectrum spec : specContainer) {
            for (PrecursorOffsetFrequency off : this.getPrecursorOFF(spec.getCharge()))
                spec.filterPrecursorPeaks(mme, off.getReducedCharge(), off.getOffset());
        }
    }

    private void deconvoluteSpectra()
    {
        SpectraContainer newSpecContainer = new SpectraContainer();
        for (Spectrum spec : specContainer) {
            newSpecContainer.add(spec.getDeconvolutedSpectrum(DECONVOLUTION_MASS_TOLERANCE));
        }
        specContainer = newSpecContainer;
    }

    private Pair<Float, Float> getParentMassRange(Partition partition)
    {
        float minParentMass = partition.getParentMass();
        float maxParentMass = Float.MAX_VALUE;
        Partition higherPartition = partitionSet.higher(partition);
        if (higherPartition != null) {
            if (higherPartition.getCharge() == partition.getCharge() && higherPartition.getSegNum() == partition.getSegNum()) {
                maxParentMass = higherPartition.getParentMass();
            }
        }
        return new Pair<>(minParentMass, maxParentMass);
    }

    private void selectIonTypes()
    {
        if (partitionSet == null) {
            assert (false) : "partition() must have been called before!";
            return;
        }

        fragOFFTable = new Hashtable<>();

        for (Partition partition : partitionSet) {
            int charge = partition.getCharge();
            // parent mass range check
            Pair<Float, Float> parentMassRange = getParentMassRange(partition);

            SpectraContainer curPartContainer = new SpectraContainer();
            for (Spectrum spec : specContainer) {
                if (spec.getAnnotation() == null)
                    continue;
                if (spec.getCharge() != charge)
                    continue;

                float curParentMass = spec.getParentMass();
                if (curParentMass < parentMassRange.getFirst() || curParentMass >= parentMassRange.getSecond())
                    continue;

                curPartContainer.add(spec);
            }

            ArrayList<FragmentOffsetFrequency> signalFragmentOffsetFrequencyList = new ArrayList<FragmentOffsetFrequency>();

            int seg = partition.getSegNum();
            IonType[] allIonTypes = IonType.getAllKnownIonTypes(Math.min(charge, 3), true, considerPhosLoss, consideriTRAQLoss, considerTMTLoss).toArray(new IonType[0]);

            IonProbability probGen = new IonProbability(
                    curPartContainer.iterator(),
                    allIonTypes,
                    mme)
                    .filter(filter)
                    .segment(seg, numSegments);

            float[] ionProb = probGen.getIonProb();

            for (int i = 0; i < allIonTypes.length; i++) {
                if (ionProb[i] >= MIN_ION_OFFSET_PROBABILITY)
                    signalFragmentOffsetFrequencyList.add(new FragmentOffsetFrequency(allIonTypes[i], ionProb[i]));
            }

            if (signalFragmentOffsetFrequencyList.size() == 0) {
                int maxIndex = -1;
                float maxIonProb = Float.MIN_VALUE;
                for (int i = 0; i < allIonTypes.length; i++) {
                    if (ionProb[i] > MIN_MAIN_ION_OFFSET_PROBABILITY && ionProb[i] > maxIonProb) {
                        maxIndex = i;
                        maxIonProb = ionProb[i];
                    }
                }
                if (maxIndex >= 0)
                    signalFragmentOffsetFrequencyList.add(new FragmentOffsetFrequency(allIonTypes[maxIndex], maxIonProb));
            }

            signalFragmentOffsetFrequencyList.sort(Collections.reverseOrder());
            fragOFFTable.put(partition, signalFragmentOffsetFrequencyList);
        }
        super.determineIonTypes();
    }

    private void generateRankDist(int maxRank)
    {
        if (partitionSet == null) {
            assert (false) : "partition() must have been called!";
            return;
        }

        rankDistTable = new Hashtable<>();
        this.maxRank = maxRank;

        for (Partition partition : partitionSet) {
            int charge = partition.getCharge();
            IonType[] ionTypes = getIonTypes(partition);
            if (ionTypes == null || ionTypes.length == 0)
                continue;

            Pair<Float, Float> parentMassRange = getParentMassRange(partition);
            int seg = partition.getSegNum();

            int numSpec = 0;
            Hashtable<IonType, Histogram<Integer>> rankDist = new Hashtable<>();
            Hashtable<IonType, Float> rankDistMaxRank = new Hashtable<>();
            Hashtable<IonType, Float> rankDistUnexplained = new Hashtable<>();

            for (IonType ion : ionTypes) {
                rankDist.put(ion, new Histogram<>());
                rankDistMaxRank.put(ion, 0f);
                rankDistUnexplained.put(ion, 0f);
            }
            rankDist.put(IonType.NOISE, new Histogram<Integer>());

            float[] noiseDist = new float[maxRank + 2];
            int numMaxRankPeaks = 0;
            int totalCleavageSites = 0;

            for (Spectrum spec : specContainer) {
                int numExplainedPeaks = 0;
                if (spec.getAnnotation() == null)
                    continue;
                if (spec.getCharge() != charge)
                    continue;
                float curParentMass = spec.getParentMass();
                if (curParentMass < parentMassRange.getFirst() || curParentMass >= parentMassRange.getSecond())
                    continue;

                Peptide annotation = spec.getAnnotation();
                spec.setRanksOfPeaks();
                numSpec++;
                numMaxRankPeaks += spec.size() - maxRank + 1;
                totalCleavageSites += annotation.size() - 1;
                int prmMassIndex = 0;
                int srmMassIndex = 0;

                HashSet<Peak> explainedPeakSet = new HashSet<Peak>();
                Hashtable<IonType, Integer> numExplainedMaxRankPeaks = new Hashtable<IonType, Integer>();
                for (IonType ion : ionTypes) {
                    numExplainedMaxRankPeaks.put(ion, 0);
                }

                int numSignalBinsAtThisSegment = 0;
                for (int i = 0; i < annotation.size() - 1; i++) {
                    prmMassIndex += NominalMass.toNominalMass(annotation.get(i).getMass());
                    srmMassIndex += NominalMass.toNominalMass(annotation.get(annotation.size() - 1 - i).getMass());

                    float prm = NominalMass.getMassFromNominalMass(prmMassIndex);
                    float srm = NominalMass.getMassFromNominalMass(srmMassIndex);
                    for (IonType ion : ionTypes) {
                        float theoMass;
                        if (ion instanceof IonType.PrefixIon)
                            theoMass = ion.getMz(prm);
                        else
                            theoMass = ion.getMz(srm);

                        int segNum = super.getSegmentNum(theoMass, curParentMass);
                        if (segNum == seg) {
                            numSignalBinsAtThisSegment++;
                            Peak p = spec.getPeakByMass(theoMass, mme);
                            if (p != null) {
                                numExplainedPeaks++;
                                int rank = p.getRank();
                                if (rank >= maxRank) {
                                    rank = maxRank;
                                    numExplainedMaxRankPeaks.put(ion, numExplainedMaxRankPeaks.get(ion) + 1);
                                }
                                explainedPeakSet.add(p);
                                rankDist.get(ion).add(rank);
                            } else {
                                rankDist.get(ion).add(maxRank + 1);    // maxRank+1: missing ion
                            }
                        }
                    }
                }

                ArrayList<Peak> unexplainedPeaksAtThisSegment = new ArrayList<Peak>();
                int numPeaksAtThisSegment = 0;
                int numMaxRankPeaksAtThisSegment = 0;
                for (Peak p : spec) {
                    if (super.getSegmentNum(p.getMz(), curParentMass) == seg) {
                        numPeaksAtThisSegment++;
                        if (p.getRank() >= maxRank)
                            numMaxRankPeaksAtThisSegment++;
                        if (!explainedPeakSet.contains(p))
                            unexplainedPeaksAtThisSegment.add(p);
                    }
                }

                float midMassThisSegment = (1f / numSegments * seg + 1f / numSegments / 2) * annotation.getParentMass();
                float unit = (float) (mme.getMax(midMassThisSegment) - midMassThisSegment);
                float numBinsAtThisSegment = annotation.getParentMass() / numSegments / unit / 2;

                for (Peak p : unexplainedPeaksAtThisSegment) {
                    int rank = p.getRank();
                    float noiseFreq = (annotation.size() - 1) / numSegments / numBinsAtThisSegment;
                    if (rank >= maxRank)
                        noiseDist[maxRank] += noiseFreq / numMaxRankPeaksAtThisSegment;
                    else
                        noiseDist[rank] += noiseFreq;
                }

                for (IonType ion : ionTypes) {
                    if (numMaxRankPeaksAtThisSegment > 0) {
                        Float prevSumFreq = rankDistMaxRank.get(ion);
                        float curFreq = numExplainedMaxRankPeaks.get(ion) / (float) numMaxRankPeaksAtThisSegment;
                        rankDistMaxRank.put(ion, prevSumFreq + curFreq);
                    }
                }

                noiseDist[maxRank + 1] += (numBinsAtThisSegment - numPeaksAtThisSegment) * (annotation.size() - 1) / numSegments / numBinsAtThisSegment;
            }

            Hashtable<IonType, Float[]> freqDist = new Hashtable<>();
            for (IonType ion : ionTypes) {
                Float[] dist = new Float[maxRank + 1];
                Histogram<Integer> hist = rankDist.get(ion);
                for (int i = 1; i <= maxRank - 1; i++) {
                    Integer num = hist.get(i);
                    dist[i - 1] = (num / (float) numSpec);
                }
                dist[maxRank - 1] = rankDistMaxRank.get(ion) / numSpec;
                dist[maxRank] = hist.get(maxRank + 1) / (float) numSpec;
                freqDist.put(ion, dist);
            }

            // noise
            Float[] dist = new Float[maxRank + 1];
            for (int i = 1; i <= maxRank + 1; i++)
                dist[i - 1] = noiseDist[i] / numSpec;
            freqDist.put(IonType.NOISE, dist);

            rankDistTable.put(partition, freqDist);
        }
    }

    private void generateErrorDist(int errorScalingFactor)
    {
        this.errorScalingFactor = errorScalingFactor;
        if (errorScalingFactor > 0) {
            generateIonErrorDist();
            generateNoiseErrorDist();
        }
    }

    private void generateIonErrorDist()
    {
        ionErrDistTable = new Hashtable<>();
        ionExistenceTable = new Hashtable<>();
        for (Partition partition : partitionSet) {
            int charge = partition.getCharge();
            Pair<Float, Float> parentMassRange = getParentMassRange(partition);
            int seg = partition.getSegNum();
            if (seg != super.getNumSegments() - 1)
                continue;
            IonType mainIon = this.getMainIonType(partition);
            IntHistogram errHist = new IntHistogram();
            int[] edgeCount = new int[4];
            int numSpecs = 0;
            for (Spectrum spec : specContainer) {
                if (spec.getAnnotation() == null)
                    continue;
                if (spec.getCharge() != charge)
                    continue;

                float curParentMass = spec.getParentMass();
                if (curParentMass < parentMassRange.getFirst() || curParentMass >= parentMassRange.getSecond())
                    continue;

                numSpecs++;
                Peptide peptide;

                peptide = spec.getAnnotation();

                int intResidueMass = 0;
                float[] obsMass = new float[peptide.size() + 1];

                obsMass[0] = mainIon.getOffset();
                for (int i = 0; i < peptide.size() - 1; i++) {
                    if (mainIon instanceof PrefixIon)
                        intResidueMass += peptide.get(i).getNominalMass();
                    else
                        intResidueMass += peptide.get(peptide.size() - 1 - i).getNominalMass();

                    float theoMass = mainIon.getMz(NominalMass.getMassFromNominalMass(intResidueMass));
                    Peak p = spec.getPeakByMass(theoMass, mme);
                    if (p != null)
                        obsMass[i + 1] = p.getMz();
                    else
                        obsMass[i + 1] = -1;
                }

                obsMass[peptide.size()] = mainIon.getMz(peptide.getMass());
                for (int i = 1; i <= peptide.size(); i++) {
                    if (obsMass[i] >= 0) {
                        if (obsMass[i - 1] >= 0)        // yy
                        {
                            AminoAcid aa;
                            if (mainIon instanceof PrefixIon)
                                aa = peptide.get(i - 1);
                            else
                                aa = peptide.get(peptide.size() - i);

                            float expMass = obsMass[i] - obsMass[i - 1];
                            float theoMass = aa.getMass() / mainIon.getCharge();
                            float diff = expMass - theoMass;
                            int diffIndex = Math.round(diff * errorScalingFactor);
                            if (diffIndex > errorScalingFactor)
                                diffIndex = errorScalingFactor;
                            else if (diffIndex < -errorScalingFactor)
                                diffIndex = -errorScalingFactor;
                            errHist.add(diffIndex);
                            edgeCount[3]++;
                        } else    // ny
                            edgeCount[1]++;
                    } else {
                        if (obsMass[i - 1] >= 0)        // yn
                            edgeCount[2]++;
                        else                        // nn
                            edgeCount[0]++;
                    }
                }
            }

            Float[] ionErrHist = new Float[2 * errorScalingFactor + 1];
            // smoothing
            float[] smoothedHist = errHist.getSmoothedHist(errorScalingFactor);
            for (int i = -errorScalingFactor; i <= errorScalingFactor; i++)
                ionErrHist[i + errorScalingFactor] = smoothedHist[i + errorScalingFactor] / (float) errHist.totalCount();

            Float[] ionExistence = new Float[edgeCount.length];
            int sumEdgeCount = 0;
            for (int i = 0; i < edgeCount.length; i++)
                sumEdgeCount += edgeCount[i];
            for (int i = 0; i < edgeCount.length; i++)
                ionExistence[i] = edgeCount[i] / (float) sumEdgeCount;

            for (int i = 0; i < this.numSegments; i++) {
                Partition part = new Partition(partition.getCharge(), partition.getParentMass(), i);
                if (partitionSet.contains(part)) {
                    ionErrDistTable.put(part, ionErrHist);
                    ionExistenceTable.put(part, ionExistence);
                }
            }
        }
    }

    private void generateNoiseErrorDist()
    {
        this.noiseErrDistTable = new Hashtable<>();
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSetWithFixedCarbamidomethylatedCys();
        AminoAcid aaK = aaSet.getAminoAcid('K');
        AminoAcid aaQ = aaSet.getAminoAcid('Q');
        int heaviestAANominalMass = aaSet.getMaxNominalMass();
        float[] nominalMass = new float[heaviestAANominalMass + 1];
        for (AminoAcid aa : aaSet)
            nominalMass[aa.getNominalMass()] = aa.getMass();

        for (Partition partition : partitionSet) {
            int charge = partition.getCharge();
            Pair<Float, Float> parentMassRange = getParentMassRange(partition);
            int seg = partition.getSegNum();
            if (seg != super.getNumSegments() - 1)
                continue;

            IntHistogram errHist = new IntHistogram();
            int numSpecs = 0;
            for (Spectrum spec : specContainer) {
                if (spec.getAnnotation() == null)
                    continue;
                if (spec.getCharge() != charge)
                    continue;

                float curParentMass = spec.getParentMass();
                if (curParentMass < parentMassRange.getFirst() || curParentMass >= parentMassRange.getSecond())
                    continue;

                Spectrum noiseSpec = (Spectrum) spec.clone();

                numSpecs++;

                for (int i = 0; i < noiseSpec.size() - 1; i++) {
                    Peak p1 = noiseSpec.get(i);
                    float p1Mass = p1.getMz();
                    int nominalP1 = NominalMass.toNominalMass(p1.getMz());
                    for (int j = i + 1; j < noiseSpec.size(); j++) {
                        Peak p2 = noiseSpec.get(j);
                        float p2Mass = p2.getMz();
                        int nominalP2 = NominalMass.toNominalMass(p2.getMz());
                        int nominalDiff = nominalP2 - nominalP1;
                        if (nominalDiff > heaviestAANominalMass)
                            break;
                        if (nominalMass[nominalDiff] == 0)
                            continue;

                        float diff = p2Mass - p1Mass;
                        float aaMass = nominalMass[nominalDiff];
                        if (nominalDiff == 128)    // K or Q
                        {
                            if (Math.abs(diff - aaQ.getMass()) > Math.abs(diff - aaK.getMass()))
                                aaMass = aaK.getMass();
                            else
                                aaMass = aaQ.getMass();
                        }
                        float err = diff - aaMass;
                        errHist.add(Math.round(err * errorScalingFactor));
                    }
                }
            }
            Float[] noiseErrHist = new Float[2 * errorScalingFactor + 1];
            // smoothing
            float[] smoothedHist = errHist.getSmoothedHist(errorScalingFactor);
            for (int i = -errorScalingFactor; i <= errorScalingFactor; i++)
                noiseErrHist[i + errorScalingFactor] = smoothedHist[i + errorScalingFactor] / (float) errHist.totalCount();

            for (int i = 0; i < this.numSegments; i++) {
                Partition part = new Partition(partition.getCharge(), partition.getParentMass(), i);
                if (partitionSet.contains(part)) {
                    noiseErrDistTable.put(part, noiseErrHist);
                }
            }
        }
    }

    protected void smoothing()
    {
        smoothingRankDistTable();
    }

    protected void smoothingRankDistTable()
    {
        if (rankDistTable == null)
            return;
        assert (smoothingRanks.length == smoothingWindowSize.length);
        for (Partition partition : rankDistTable.keySet()) {
            Hashtable<IonType, Float[]> table = this.rankDistTable.get(partition);
            for (IonType ion : table.keySet()) {
                Float[] freq = table.get(ion);
                Float[] smoothedFreq = new Float[freq.length];
                int smoothingIndex = 0;
                for (int i = 0; i < freq.length - 2; i++)    // last 2 columns: maxRank, unexplained
                {
                    if (smoothingIndex < smoothingRanks.length - 1 &&
                            i == smoothingRanks[smoothingIndex])
                        smoothingIndex++;
                    int windowSize = smoothingWindowSize[smoothingIndex];
                    float sumFrequencies = 0;
                    int numIndicesSummed = 0;
                    for (int d = -windowSize; d <= windowSize; d++) {
                        int index = i + d;
                        if (index < 0 || index > freq.length - 3)
                            continue;
                        sumFrequencies += freq[index];
                        numIndicesSummed++;
                    }
                    while (sumFrequencies == 0 && windowSize < freq.length - 4) {
                        windowSize++;
                        int index = i - windowSize;
                        if (index >= 0) {
                            sumFrequencies += freq[index];
                            numIndicesSummed++;
                        }
                        index = i + windowSize;
                        if (index <= freq.length - 3) {
                            sumFrequencies += freq[index];
                            numIndicesSummed++;
                        }
                    }
                    if (sumFrequencies != 0)
                        smoothedFreq[i] = sumFrequencies / numIndicesSummed;
                    else
                        assert (false);
                }
                for (int i = 0; i < freq.length - 2; i++)
                    freq[i] = smoothedFreq[i];
                if (freq[freq.length - 1] == 0)
                    freq[freq.length - 1] = Float.MIN_VALUE;
                if (freq[freq.length - 2] == 0)
                    freq[freq.length - 2] = freq[freq.length - 3];
            }
        }
    }
}

