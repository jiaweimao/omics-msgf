package edu.ucsd.msjava.msgf;

import edu.ucsd.msjava.msutil.AminoAcidSet;
import edu.ucsd.msjava.msutil.Annotation;
import edu.ucsd.msjava.msutil.Matter;
import edu.ucsd.msjava.msutil.Peptide;

import java.util.ArrayList;

/**
 * It is the abstract class for deNovo
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Feb 2019, 2:11 PM
 */
public abstract class DeNovoGraph<T extends Matter>
{
    protected T source;
    protected T pmNode;
    protected ArrayList<T> sinkNodes;
    protected ArrayList<T> intermediateNodes;

    /**
     * @return the source node
     */
    public T getSource()
    {
        return source;
    }

    public T getPMNode()
    {
        return pmNode;
    }

    public ArrayList<T> getSinkList()
    {
        return sinkNodes;
    }

    public ArrayList<T> getIntermediateNodeList()
    {
        return intermediateNodes;
    }

    public abstract boolean isReverse();

    /**
     * Return the de novo score of given {@link Peptide}
     *
     * @param pep a {@link Peptide}
     * @return de novo score
     */
    public abstract int getScore(Peptide pep);

    public abstract int getScore(Annotation annotation);

    /**
     * @param node deNovo graph node
     * @return score for this node
     */
    public abstract int getNodeScore(T node);

    public abstract ArrayList<Edge<T>> getEdges(T curNode);

    public abstract T getComplementNode(T node);

    public abstract AminoAcidSet getAASet();

    /**
     * This class define the edge of the deNovo Graph.
     */
    public static class Edge<T extends Matter>
    {
        private T prevNode;
        private float probability;
        private int index;
        private float mass;

        // scores
        private int cleavageScore;
        private int errorScore;

        /**
         * Create an {@link Edge}
         *
         * @param prevNode    previous node of the Edge
         * @param probability probability
         * @param index
         * @param mass        delta mass of this edge
         */
        public Edge(T prevNode, float probability, int index, float mass)
        {
            this.prevNode = prevNode;
            this.probability = probability;
            this.index = index;
            this.mass = mass;
        }

        /**
         * @return the previous Node of the Edge
         */
        public T getPrevNode()
        {
            return prevNode;
        }

        public void setCleavageScore(int cleavageScore)
        {
            this.cleavageScore = cleavageScore;
        }

        public void setErrorScore(int errorScore)
        {
            this.errorScore = errorScore;
        }

        public int getEdgeScore()
        {
            return cleavageScore + errorScore;
        }

        public int getErrorScore()
        {
            return errorScore;
        }

        public float getEdgeProbability()
        {
            return probability;
        }

        public int getEdgeIndex()
        {
            return index;
        }

        /**
         * @return mass of the edge
         */
        public float getEdgeMass()
        {
            return mass;
        }
    }
}
