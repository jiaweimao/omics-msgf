package edu.ucsd.msjava.msgf;

import edu.ucsd.msjava.msutil.Constants;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;

public class AminoAcidGraph extends GenericDeNovoGraph<NominalMass>
{
    public AminoAcidGraph(NominalMassFactory factory, float parentMass, ScoredSpectrum<NominalMass> scoredSpec)
    {
        super(factory, parentMass, new AbsoluteTolerance(0), factory.getEnzyme(), scoredSpec);
    }

    public AminoAcidGraph(NominalMassFactory factory, int nominalPeptideMass, ScoredSpectrum<NominalMass> scoredSpec)
    {
        this(factory, (nominalPeptideMass + 18) / Constants.INTEGER_MASS_SCALER, scoredSpec);
    }
}
