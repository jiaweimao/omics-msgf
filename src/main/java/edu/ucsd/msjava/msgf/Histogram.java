package edu.ucsd.msjava.msgf;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

/**
 * Histogram
 *
 * @version 1.0.0
 * @since 20 Jan 2019, 1:31 PM
 */
public class Histogram<T extends Comparable<T>> extends Hashtable<T, Integer>
{
    private static final long serialVersionUID = 1L;

    private T minKey = null;
    private T maxKey = null;
    private int size;

    /**
     * add a new item to this histogram
     *
     * @param t new item
     */
    public void add(T t)
    {
        this.merge(t, 1, (a, b) -> a + b);
        if (minKey == null || minKey.compareTo(t) > 0)
            minKey = t;
        if (maxKey == null || maxKey.compareTo(t) < 0)
            maxKey = t;
        size++;
    }

    public void setMinKey(T minKey)
    {
        this.minKey = minKey;
    }

    public void setMaxKey(T maxKey)
    {
        this.maxKey = maxKey;
    }

    public T minKey()
    {
        return minKey;
    }

    public T maxKey()
    {
        return maxKey;
    }

    public int totalCount()
    {
        return size;
    }

    @Override
    public Integer get(Object key)
    {
        Integer num = super.get(key);
        if (num == null)
            return 0;
        else
            return num;
    }

    public void printSorted()
    {
        ArrayList<T> keyList = new ArrayList<T>(this.keySet());
        Collections.sort(keyList);
        for (T key : keyList)
            System.out.println(key + "\t" + this.get(key));
    }

    public void printSortedRatio()
    {
        int totalCount = totalCount();
        ArrayList<T> keyList = new ArrayList<T>(this.keySet());
        Collections.sort(keyList);
        for (T key : keyList) {
            System.out.println(key + "\t" + this.get(key) + "\t" + this.get(key) / (float) totalCount);
        }
    }
}
