package edu.ucsd.msjava.msgf;

import edu.ucsd.msjava.msutil.Annotation;
import edu.ucsd.msjava.msutil.Matter;

import java.util.HashMap;
import java.util.Map;

/**
 * this class store a group of {@link GeneratingFunction}
 *
 * @version 1.0.0
 * @since 29 Jan 2019, 1:38 PM
 */
public class GeneratingFunctionGroup<T extends Matter> implements GF
{
    private static final long serialVersionUID = 1L;

    private static ScoreDistFactory factory = new ScoreDistFactory(false, true);

    private HashMap<T, GeneratingFunction<T>> gfMap = new HashMap<>();
    private ScoreDist mergedScoreDist = null;

    /**
     * add a new {@link GeneratingFunction} to this map
     *
     * @param sink object with mass {@link Matter}
     * @param gf   a {@link GeneratingFunction}
     */
    public void registerGF(T sink, GeneratingFunction<T> gf)
    {
        this.gfMap.put(sink, gf);
    }

    public boolean computeGeneratingFunction()
    {
        int minScore = Integer.MAX_VALUE;
        int maxScore = Integer.MIN_VALUE;
        for (Map.Entry<T, GeneratingFunction<T>> entry : gfMap.entrySet()) {
            GeneratingFunction<T> gf = entry.getValue();
            if (!gf.isGFComputed()) {
                if (gf.computeGeneratingFunction()) {
                    int curMinScore = gf.getMinScore();
                    if (minScore > curMinScore)
                        minScore = curMinScore;
                    int curMaxScore = gf.getMaxScore();
                    if (maxScore < curMaxScore)
                        maxScore = curMaxScore;
                }
            }
        }
        if (minScore >= maxScore)
            return false;
        mergedScoreDist = factory.getInstance(minScore, maxScore);
        for (Map.Entry<T, GeneratingFunction<T>> entry : gfMap.entrySet()) {
            GeneratingFunction<T> gf = entry.getValue();
            mergedScoreDist.addProbDist(gf.getScoreDist(), 0, 1f);
        }
        return true;
    }

    public int getScore(Annotation annotation)
    {
        int score = Integer.MIN_VALUE;
        for (Map.Entry<T, GeneratingFunction<T>> entry : gfMap.entrySet()) {
            GeneratingFunction<T> gf = entry.getValue();
            int curScore = gf.getScore(annotation);
            if (curScore > score)
                score = curScore;
        }

        return score;
    }

    public double getSpectralProbability(int score)
    {
        return mergedScoreDist.getSpectralProbability(score);
    }

    public int getMaxScore()
    {
        if (mergedScoreDist == null)
            System.out.println("Debug in getMaxScore: getMaxScore is null");
        return mergedScoreDist.getMaxScore();
    }

    public ScoreDist getScoreDist()
    {
        return mergedScoreDist;
    }
}
