package edu.ucsd.msjava.msgf;

/**
 * class to hold score distribution.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Feb 2019, 1:47 PM
 */
public class ScoreDist extends ScoreBound
{
    private double[] numDistribution;
    private double[] probDistribution;

    /**
     * Create a {@link ScoreDist}
     *
     * @param minScore   the minimum score
     * @param maxScore   the maximum score
     * @param calcNumber true if calculate number distribution
     * @param calcProb   true if calculate probability distribution
     */
    ScoreDist(int minScore, int maxScore, boolean calcNumber, boolean calcProb)
    {
        super(minScore, maxScore);
        if (calcNumber)
            numDistribution = new double[maxScore - minScore];
        if (calcProb)
            probDistribution = new double[maxScore - minScore];
    }

    /**
     * @return true if probability distribution is set.
     */
    public boolean isProbSet()
    {
        return probDistribution != null;
    }

    /**
     * @return true if number distribution is set.
     */
    public boolean isNumSet()
    {
        return numDistribution != null;
    }

    /**
     * set of number of given score
     *
     * @param score  score value
     * @param number number of the score
     */
    public void setNumber(int score, double number)
    {
        numDistribution[score - minScore] = number;
    }

    /**
     * set the probability of given score
     *
     * @param score score value
     * @param prob  probability
     */
    public void setProb(int score, double prob)
    {
        probDistribution[score - minScore] = prob;
    }

    /**
     * add count to given score value
     *
     * @param score  score value
     * @param number number to add
     */
    public void addNumber(int score, double number)
    {
        numDistribution[score - minScore] += number;
    }

    /**
     * add probability to given score value
     *
     * @param score score value
     * @param prob  probability
     */
    public void addProb(int score, double prob)
    {
        probDistribution[score - minScore] += prob;
    }

    /**
     * return the probability of given score
     *
     * @param score score value
     * @return probability of the score
     */
    public double getProbability(int score)
    {
        int index = (score >= minScore) ? score - minScore : 0;
        return probDistribution[index];
    }

    /**
     * return the number of given score
     *
     * @param score score value
     * @return number of the score
     */
    public double getNumberRecs(int score)
    {
        int index = (score >= minScore) ? score - minScore : 0;
        return numDistribution[index];
    }

    /**
     * Return the probability of the match score >= <code>score</code>.
     *
     * @param score score to test
     * @return probability
     */
    public double getSpectralProbability(int score)
    {
        double specProb = 0;
        int minIndex = (score >= minScore) ? score - minScore : 0;
        for (int t = minIndex; t < probDistribution.length; t++) {
            specProb += probDistribution[t];
        }
        if (specProb > 1.)
            specProb = 1.;
        return specProb;
    }

    /**
     * Return the number of peptides with score >= <code>score</code>
     *
     * @param score score threshold
     * @return number of peptides with score >= <code>score</code>
     */
    public double getNumEqualOrBetterPeptides(int score)
    {
        double numBetterPeptides = 0;
        int minIndex = (score >= minScore) ? score - minScore : 0;
        for (int t = minIndex; t < numDistribution.length; t++)
            numBetterPeptides += numDistribution[t];
        return numBetterPeptides;
    }

    public void addNumDist(ScoreDist otherDist, int scoreDiff)
    {
        addNumDist(otherDist, scoreDiff, 1);
    }

    public void addNumDist(ScoreDist otherDist, int scoreDiff, int coeff)
    {
        if (otherDist == null)
            return;

        for (int t = Math.max(otherDist.minScore, minScore - scoreDiff); t < otherDist.maxScore; t++)
            numDistribution[t + scoreDiff - minScore] += coeff * otherDist.numDistribution[t - otherDist.minScore];
    }

    public void addProbDist(ScoreDist otherDist, int scoreDiff, float aaProb)
    {
        if (otherDist == null)
            return;
        for (int t = Math.max(otherDist.minScore, minScore - scoreDiff); t < otherDist.maxScore; t++) {
            double prob = otherDist.probDistribution[t - otherDist.minScore] * aaProb;
            probDistribution[t + scoreDiff - minScore] += prob;    // TODO: underflow
        }
    }
}
