package edu.ucsd.msjava.msgf;

/**
 * class to hold information of score value range
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Feb 2019, 1:48 PM
 */
public class ScoreBound
{
    protected int minScore;    // inclusive
    protected int maxScore;    // exclusive

    /**
     * Create a {@link ScoreBound}
     *
     * @param minScore min score value
     * @param maxScore max score value
     */
    public ScoreBound(int minScore, int maxScore)
    {
        this.minScore = minScore;
        this.maxScore = maxScore;
    }

    /**
     * @return the minimum score value
     */
    public int getMinScore()
    {
        return minScore;
    }

    public void setMinScore(int minScore)
    {
        this.minScore = minScore;
    }

    /**
     * @return maximum score value
     */
    public int getMaxScore()
    {
        return maxScore;
    }

    /**
     * @return score value range, which the the difference between maximum score and minimum score.
     */
    public int getRange()
    {
        return maxScore - minScore;
    }

    public void setMaxScore(int maxScore)
    {
        this.maxScore = maxScore;
    }
}
