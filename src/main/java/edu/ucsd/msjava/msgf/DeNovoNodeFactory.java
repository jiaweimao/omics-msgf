package edu.ucsd.msjava.msgf;

import edu.ucsd.msjava.msutil.*;
import omics.util.ms.peaklist.Tolerance;

import java.util.ArrayList;
import java.util.Collection;

/**
 * This interface define methods to create DeNovo graph nodes.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Feb 2019, 2:17 PM
 */
public interface DeNovoNodeFactory<T extends Matter>
{
    /**
     * @return the {@link AminoAcidSet} used for mass calculation
     */
    AminoAcidSet getAASet();

    /**
     * @return the zero node
     */
    T getZero();

    /**
     * return all nodes with masses within to
     *
     * @param mass
     * @param tolerance
     * @return
     */
    ArrayList<T> getNodes(float mass, Tolerance tolerance);

    /**
     * @param mass mass
     * @return the closed node of given mass
     */
    T getNode(float mass);

    /**
     * return the complementary node of givem node
     *
     * @param srm    source node to test
     * @param pmNode precursor mass node
     * @return complementary node to the <code>srm</code> node
     */
    T getComplementNode(T srm, T pmNode);

    ArrayList<T> getLinkedNodeList(Collection<T> destNodes);

    /**
     * return all {@link edu.ucsd.msjava.msgf.DeNovoGraph.Edge} linked to given nodes
     *
     * @param curNode graph node
     * @return {@link edu.ucsd.msjava.msgf.DeNovoGraph.Edge}s linked to the node
     */
    ArrayList<DeNovoGraph.Edge<T>> getEdges(T curNode);

    /**
     * convert the denovo graph to sequence
     *
     * @param isPrefix true if the peptide is the prefix
     * @param pep
     * @return
     */
    Sequence<T> toCumulativeSequence(boolean isPrefix, Peptide pep);

    /**
     * Return the previous node linked to <code>curNode</code> with mass equal to the {@link AminoAcid}
     *
     * @param curNode current graph node
     * @param aa      an {@link AminoAcid}
     * @return the previous node linked to <code>curNode</code> with the {@link AminoAcid} edge
     */
    T getPreviousNode(T curNode, AminoAcid aa);

    /**
     * return the next node linked to <code>curNode</code> with mass equal to the {@link AminoAcid}
     *
     * @param curNode current graph node
     * @param aa      {@link AminoAcid} as graph edge
     * @return next node linked to current node with the {@link AminoAcid} edge
     */
    T getNextNode(T curNode, AminoAcid aa);

    /**
     * @return number of nodes in the graph.
     */
    int size();

    /**
     * Return true if the graph contains given Node
     *
     * @param node node to test
     * @return true if the graph contains the node
     */
    boolean contains(T node);

    /**
     * @return true if this graph represents reverse peptides.
     */
    boolean isReverse();

    /**
     * @return {@link Enzyme}
     */
    Enzyme getEnzyme();
}
