package edu.ucsd.msjava.msgf;

import edu.ucsd.msjava.msutil.Annotation;

/**
 * generate function interface to calculate match score.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 29 Jan 2019, 1:59 PM
 */
public interface GF
{
    /**
     * compute the generating function score, return true if compute successfully
     *
     * @return true if compute generating function score successfully
     */
    boolean computeGeneratingFunction();

    /**
     * return the GF score of given {@link Annotation}
     *
     * @param annotation an {@link Annotation}
     * @return GF score
     */
    int getScore(Annotation annotation);

    /**
     * Return the E-value of given match score
     *
     * @param score match score
     * @return spectrum E-value
     */
    double getSpectralProbability(int score);

    int getMaxScore();

    ScoreDist getScoreDist();
}
