package edu.ucsd.msjava.msgf;

import edu.ucsd.msjava.msutil.ActivationMethod;
import edu.ucsd.msjava.msutil.Matter;
import edu.ucsd.msjava.msutil.Peak;

public interface ScoredSpectrum<T extends Matter>
{
    /**
     * Return the score of given node and its complementary node
     *
     * @param prm complementary node
     * @param srm source node
     * @return node score
     */
    int getNodeScore(T prm, T srm);

    float getNodeScore(T node, boolean isPrefix);

    int getEdgeScore(T curNode, T prevNode, float edgeMass);

    /**
     * @return true: prefix, false: suffix
     */
    boolean getMainIonDirection();

    /**
     * @return precursor peak
     */
    Peak getPrecursorPeak();

    ActivationMethod[] getActivationMethodArr();

    int[] getScanNumArr();
}
