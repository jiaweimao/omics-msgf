package edu.ucsd.msjava.suffixarray;

import edu.ucsd.msjava.sequences.Sequence;


/**
 * SuffixFactory and Suffix classes. This class will allow the creation of
 * light weight suffix objects given a long sequence in the form of an Adapter
 * object.
 *
 * @author jung
 */
public class SuffixFactory
{
    /**
     * Class that represents a Suffix object.
     *
     * @author jung
     */
    public class Suffix extends ByteSequence
    {
        // the index of this suffix
        private int index;

        /**
         * Constructor.
         *
         * @param index the starting index of the suffix.
         */
        public Suffix(int index)
        {
            this.index = index;
        }

        /**
         * @return length of the suffix
         */
        public int getSize()
        {
            return (int) sequence.getSize() - index;
        }

        /**
         * return the byte at given index of the this suffix
         *
         * @param index the index to retrieve the value from.
         */
        public byte getByteAt(int index)
        {
            return sequence.getByteAt(this.index + index);
        }


        /**
         * Getter method.
         *
         * @return the index of this suffix.
         */
        public int getIndex()
        {
            return this.index;
        }
    }

    /**
     * The sequence used to create suffixes.
     */
    private Sequence sequence;

    /**
     * Constructor.
     *
     * @param sequence the sequence object to create the suffixes from.
     */
    public SuffixFactory(Sequence sequence)
    {
        this.sequence = sequence;
    }


    /**
     * Factory method that creates a new suffix object from a sequence.
     *
     * @param index the starting index of the suffix.
     * @return the suffix object
     */
    public Suffix makeSuffix(int index)
    {
        return new Suffix(index);
    }
}
