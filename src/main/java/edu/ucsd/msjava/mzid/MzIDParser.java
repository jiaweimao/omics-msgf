package edu.ucsd.msjava.mzid;

import edu.ucsd.msjava.msutil.Composition;
import omics.pdk.HitType;
import omics.pdk.IdentResult;
import omics.pdk.ScoreType;
import omics.pdk.SpectrumIdentifier;
import omics.pdk.matches.PeptideMatch;
import omics.pdk.matches.PeptideProteinMatch;
import omics.pdk.matches.PeptideSpectrumMatch;
import omics.util.io.csv.CsvWriter;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.PpmTolerance;
import omics.util.ms.spectra.ScanNumberList;
import omics.util.ms.spectra.SpectraData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;

public class MzIDParser
{
    private final boolean showDecoy;
    private boolean doNotShowQValue;
    private final boolean unrollResults;
    private IdentResult identResult;

    public MzIDParser(IdentResult identResult)
    {
        this(identResult, false, false);
    }

    /**
     * Create a MzIDParser
     */
    public MzIDParser(IdentResult identResult, boolean showDecoy, boolean unrollResults)
    {
        this.identResult = identResult;
        this.unrollResults = unrollResults;
        this.showDecoy = showDecoy;
    }


    public void writeToTSVFile(File outputFile)
    {
        PrintStream out = null;
        if (outputFile != null)
            try {
                out = new PrintStream(new BufferedOutputStream(new FileOutputStream(outputFile)));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        else
            out = System.out;

        try {
            writeToTSVFile(out);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (out != System.out)
            out.close();
    }

    public void writeToTSVFile(PrintStream out) throws IOException
    {
        boolean isPrecursorTolerancePPM = false;
        Tolerance parentTol = identResult.getParameters().getPSMParameter().getParentTol();
        if (parentTol instanceof PpmTolerance)
            isPrecursorTolerancePPM = true;

        if (!doNotShowQValue) {
            boolean searchDecoy = identResult.getParameters().getPSMParameter().isSearchDecoy();
            if (!searchDecoy) {
                doNotShowQValue = true;
            }
        }

        CsvWriter writer = new CsvWriter(out, CsvWriter.Letters.TAB, StandardCharsets.UTF_8);
        writer.write("#SpecFile");
        writer.write("SpecID");
        writer.write("ScanNum");
        writer.write("Title");
        writer.write("FragMethod");
        writer.write("Precursor");
        writer.write("IsotopeError");

        String unit = isPrecursorTolerancePPM ? "ppm" : "Da";
        writer.write("PrecursorError(" + unit + ")");
        writer.write("Charge");
        writer.write("Peptide");
        writer.write("Protein");
        writer.write("DeNovoScore");
        writer.write("MSGFScore");
        writer.write("SpecEValue");
        writer.write("EValue");

        if (!doNotShowQValue) {
            writer.write("QValue");
            writer.write("PepQValue");
        }
        writer.endRecord();

        for (PeptideSpectrumMatch psm : identResult) {
            SpectrumIdentifier identifier = psm.getIdentifier();
            SpectraData spectraData = identifier.getSpectraData();
            String specFileName = spectraData.getFileName();
            String specID = psm.getIdentifier().getSpectrumTitle();

            String scanNum = "-1";
            ScanNumberList scanNumberList = psm.getScanNumberList();
            if (!scanNumberList.isEmpty()) {
                scanNum = scanNumberList.getFirst().toString();
            }

            String title = identifier.getSpectrumTitle();

            Double calculatedMassToCharge = psm.getCalculatedMz();
            Double experimentalMassToCharge = psm.getPrecursorMz();
            int charge = psm.getCharge();

            String deNovoScore = psm.hasScore(ScoreType.MSGF_DENOVO_SCORE) ? ScoreType.MSGF_DENOVO_SCORE.format(psm.getIntScore(ScoreType.MSGF_DENOVO_SCORE)) : "";
            String rawScore = psm.hasScore(ScoreType.MSGF_RAW_SCORE) ? ScoreType.MSGF_RAW_SCORE.format(psm.getIntScore(ScoreType.MSGF_RAW_SCORE)) : "";
            String specEValue = psm.hasScore(ScoreType.MSGF_SPEC_EVALUE) ? ScoreType.MSGF_SPEC_EVALUE.format(psm.getNumberScore(ScoreType.MSGF_SPEC_EVALUE)) : "";
            String eValue = psm.hasScore(ScoreType.MSGF_EVALUE) ? ScoreType.MSGF_EVALUE.format(psm.getNumberScore(ScoreType.MSGF_EVALUE)) : "";
            String psmQValue = psm.hasScore(ScoreType.MSGF_QVALUE) ? ScoreType.MSGF_QVALUE.format(psm.getNumberScore(ScoreType.MSGF_QVALUE)) : "";
            String pepQValue = psm.hasScore(ScoreType.MSGF_PEP_QVALUE) ? ScoreType.MSGF_PEP_QVALUE.format(psm.getNumberScore(ScoreType.MSGF_PEP_QVALUE)) : "";

            String fragMethod = psm.getPMString("AssumedDissociationMethod");
            Object error = psm.getPM("IsotopeError").orElse(null);
            Integer isotopeError = error == null ? 0 : Integer.parseInt(error.toString());

            double adjustedExpMz = experimentalMassToCharge - Composition.ISOTOPE * isotopeError / charge;
            double precursorError = adjustedExpMz - calculatedMassToCharge;
            if (isPrecursorTolerancePPM)
                precursorError = precursorError / calculatedMassToCharge * 1e6;

            PeptideMatch peptideMatch = psm.getPeptideMatch();
            String peptideSeq = peptideMatch.getPeptide().toString();

            HashSet<String> proteinSet = new HashSet<>();
            if (unrollResults) {
                for (PeptideProteinMatch peptideProteinMatch : peptideMatch.getPeptideProteinMatches()) {
                    boolean isDecoy = peptideProteinMatch.getHitType() == HitType.DECOY;
                    if (isDecoy && !this.showDecoy)
                        continue;

                    String pre = peptideProteinMatch.getPreviousAA();
                    String post = peptideProteinMatch.getNextAA();

                    String protein = peptideProteinMatch.getAccession();
                    if (proteinSet.add(pre + protein + post)) {
                        writer.write(specFileName);
                        writer.write(specID);
                        writer.write(scanNum);
                        writer.write(title);
                        writer.write(fragMethod);
                        writer.write(experimentalMassToCharge);
                        writer.write(isotopeError);
                        writer.write(precursorError);
                        writer.write(charge);
                        writer.write(pre + "." + peptideSeq + "." + post);
                        writer.write(protein);
                        writer.write(deNovoScore);
                        writer.write(rawScore);
                        writer.write(specEValue);
                        writer.write(eValue);
                        if (!doNotShowQValue) {
                            writer.write(psmQValue);
                            writer.write(pepQValue);
                        }

                        writer.endRecord();
                    }
                }
            } else {
                StringBuffer proteinBuf = new StringBuffer();
                boolean isAllDecoy = true;
                for (PeptideProteinMatch peptideProteinMatch : peptideMatch.getPeptideProteinMatches()) {
                    boolean isDecoy = peptideProteinMatch.getHitType() == HitType.TARGET;
                    if (isDecoy && !this.showDecoy) {
                        continue;
                    }

                    isAllDecoy = false;
                    String pre = peptideProteinMatch.getPreviousAA();
                    String post = peptideProteinMatch.getNextAA();

                    String protein = peptideProteinMatch.getAccession();

                    if (proteinSet.add(pre + protein + post)) {
                        if (proteinBuf.length() != 0)
                            proteinBuf.append(";");
                        proteinBuf.append(protein + "(pre=" + pre + ",post=" + post + ")");
                    }
                }

                if (!isAllDecoy) {
                    writer.write(specFileName);
                    writer.write(specID);
                    writer.write(scanNum);
                    writer.write(title);
                    writer.write(fragMethod);
                    writer.write(experimentalMassToCharge);
                    writer.write(isotopeError);
                    writer.write(precursorError);
                    writer.write(charge);
                    writer.write(peptideSeq);
                    writer.write(proteinBuf.toString());
                    writer.write(deNovoScore);
                    writer.write(rawScore);
                    writer.write(specEValue);
                    writer.write(eValue);

                    if (!doNotShowQValue) {
                        writer.write(psmQValue);
                        writer.write(pepQValue);
                    }
                    writer.endRecord();
                }

            } // end spectrum identification item
        }
        writer.close();
    }
}
