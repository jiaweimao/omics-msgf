/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucsd.msjava.mzid;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import edu.ucsd.msjava.msdbsearch.*;
import edu.ucsd.msjava.msutil.*;
import edu.ucsd.msjava.msutil.Modification.Location;
import edu.ucsd.msjava.ui.MSGFPlus;
import omics.msgf.DeltaEntry;
import omics.pdk.*;
import omics.pdk.matches.PeptideMatch;
import omics.pdk.matches.PeptideProteinMatch;
import omics.pdk.matches.PeptideSpectrumMatch;
import omics.pdk.matches.ProteinMatch;
import omics.pdk.parameters.IdentParameters;
import omics.pdk.parameters.PSMParameter;
import omics.util.OmicsRuntimeException;
import omics.util.OmicsTask;
import omics.util.chem.MassType;
import omics.util.chem.PeriodicTable;
import omics.util.io.FilenameUtils;
import omics.util.math.MathUtils;
import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.spectra.TimeUnit;
import omics.util.protein.database.DecoyType;
import omics.util.protein.digest.EnzymeFactory;
import omics.util.protein.digest.ProteinDigester;
import omics.util.protein.mod.*;
import omics.util.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * Generator of mzidentML file
 *
 * @author JiaweiMao
 * @version 1.1.2
 * @since 20 Dec 2018, 4:29 PM
 */
public class Convert2IdentResult extends OmicsTask<IdentResult>
{
    private static final Logger logger = LoggerFactory.getLogger(Convert2IdentResult.class);

    private static final String siProtocolID = "SearchProtocol_1";
    private static final String analysisSoftID = "ID_software";
    private static final String pepIDPrefix = "Pep_";
    private static final String siiID = "SII_";
    private static final String searchDBID = "SearchDB_1";
    private static final String spectraDataID = "SID_1";
    private static final String sirID = "index=";
    private static final String pepEvIDPrefix = "PepEv_";

    private final SearchParams params;
    private AminoAcidSet aaSet;
    private CompactSuffixArray sa;
    private SpectraAccessor specAcc;
    private final int ioIndex;

    private float eValueThreshold = Float.MAX_VALUE;

    private Map<Integer, ProteinMatch> proteinMatchMap = new HashMap<>();
    private Map<Integer, Boolean> isDecoyMap = new HashMap<>();
    private Map<String, PeptideMatch> peptideMatchMap = new HashMap<>();
    private Map<String, PeptideProteinMatch> pepEvMap = new HashMap<>();
    private Map<String, List<PeptideProteinMatch>> evRefListMap = new HashMap<>();

    private List<MSGFPlusMatch> resultList;

    private IdentResult identResult;
    private PSMParameter psmParameter;
    private IdentParameters parameters;

    public Convert2IdentResult(SearchParams params, AminoAcidSet aaSet, CompactSuffixArray sa, SpectraAccessor specAcc, int ioIndex, List<MSGFPlusMatch> resultList)
    {
        this.params = params;
        this.aaSet = aaSet;
        this.sa = sa;
        this.specAcc = specAcc;
        this.ioIndex = ioIndex;
        this.resultList = resultList;
    }


    @Override
    public void start()
    {
        identResult = new IdentResult(FilenameUtils.getBaseName(specAcc.getSpecFile().getName()));
        parameters = identResult.getParameters();
        psmParameter = parameters.getPSMParameter();

        addParameter();
        addPSMList();

        updateValue(identResult);
    }

    private void addParameter()
    {
        AnalysisSoftware msgfPlus = new AnalysisSoftware(ScoreInfo.software4Id("5"));
        msgfPlus.setInnerId(analysisSoftID);
        msgfPlus.setVersion(MSGFPlus.VERSION);
        parameters.addSoftware(msgfPlus);

        // search database
        File databaseFile = params.getDatabaseFile();
        DatabaseInfo databaseInfo = new DatabaseInfo();
        databaseInfo.setId(searchDBID);
        databaseInfo.setNumSequence((long) sa.getSequence().getNumProteins());
        databaseInfo.setLocation(databaseFile.getAbsolutePath());
        databaseInfo.setName(databaseFile.getName());
        databaseInfo.setType(DatabaseInfo.DatabaseType.AA);

        // for decoy
        if (params.useTDA()) {
            databaseInfo.setDecoyTag(ReverseDB.DECOY_PROTEIN_PREFIX);
            databaseInfo.setDecoyType(DecoyType.REVERSE);
        }
        parameters.setDatabaseInfo(databaseInfo);

        // spectra data
        omics.util.ms.spectra.SpectraData spectraData = new omics.util.ms.spectra.SpectraData(spectraDataID);
        File specFile = params.getDBSearchIOList().get(ioIndex).getSpecFile();
        spectraData.setLocation(specFile.getAbsolutePath());
        spectraData.setFileName(specFile.getName());
        SpecFileFormat specFileFormat = params.getDBSearchIOList().get(ioIndex).getSpecFileFormat();
        spectraData.setMSFileType(specFileFormat.getMSFileType());
        parameters.addSpectraData(spectraData);

        psmParameter.setId(siProtocolID);
        psmParameter.setAnalysisSoftware(msgfPlus);
        psmParameter.setSearchType(SearchType.MS2);

        psmParameter.addPM("TargetDecoyApproach", String.valueOf(params.useTDA()));
        psmParameter.addPM("MinIsotopeError", String.valueOf(params.getMinIsotopeError()));
        psmParameter.addPM("MaxIsotopeError", String.valueOf(params.getMaxIsotopeError()));
        psmParameter.addPM("FragmentMethod", params.getActivationMethod().getName());

        // Enzymes
        edu.ucsd.msjava.msutil.Enzyme enzyme = params.getEnzyme();

        // AdditionalSearchParams
        psmParameter.setParentMassType(MassType.MONOISOTOPIC);
        psmParameter.setFragmentMassType(MassType.MONOISOTOPIC);
        psmParameter.addPM("Instrument", params.getInstType().getName());
        psmParameter.addPM("Protocol", params.getProtocol().getName());
        int ntt = params.getNumTolerableTermini();
        if (enzyme == edu.ucsd.msjava.msutil.Enzyme.NoCleavage || enzyme == edu.ucsd.msjava.msutil.Enzyme.UnspecificCleavage)
            ntt = 0;

        psmParameter.addPM("NumTolerableTermini", String.valueOf(ntt));
        psmParameter.addPM("NumMatchesPerSpec", String.valueOf(params.getNumMatchesPerSpec()));
        // ModificationFile
        psmParameter.addPM("MaxNumModifications", String.valueOf(aaSet.getMaxNumberOfVariableModificationsPerPeptide()));
        psmParameter.addPM("MinPepLength", String.valueOf(params.getMinPeptideLength()));
        psmParameter.addPM("MaxPepLength", String.valueOf(params.getMaxPeptideLength()));
        psmParameter.addPM("MinCharge", String.valueOf(params.getMinCharge()));
        psmParameter.addPM("MaxCharge", String.valueOf(params.getMaxCharge()));
        psmParameter.addPM("ChargeCarrierMass", String.valueOf(params.getChargeCarrierMass()));

        // add fixed mods
        if (!aaSet.getModifications().isEmpty())
            addModificationParam();

        Tolerance parentTol = params.getParentMassTolerance();
        psmParameter.setParentTol(parentTol);

        if (enzyme != null) {
            String psiCvAccession = enzyme.getPSICvAccession();

            omics.util.protein.digest.Enzyme myEnzyme;
            if (psiCvAccession != null) {
                myEnzyme = EnzymeFactory.getEnzyme(psiCvAccession);
            } else {
                if (enzyme == Enzyme.LysN) {
                    myEnzyme = EnzymeFactory.getEnzyme("Lys-N");
                } else if (enzyme == Enzyme.ALP) {
                    myEnzyme = new omics.util.protein.digest.Enzyme(null, "alphaLP", null, "", null, null);
                } else {
                    return;
                }
            }

            ProteinDigester.Builder builder = new ProteinDigester.Builder(myEnzyme);
            if (ntt != 2)
                builder.semi();
            int maxMissedCleavages = params.getMaxMissedCleavages();
            if (maxMissedCleavages < 0)
                maxMissedCleavages = 0;
            builder.missedCleavageMax(maxMissedCleavages);
            psmParameter.addEnzyme(builder.build());
        }
    }


    private void addModificationParam()
    {
        PTMFactory factory = PTMFactory.getInstance();
        for (edu.ucsd.msjava.msutil.Modification.Instance mod : aaSet.getModifications()) {
            String modName = mod.getModification().getName();
            char residue = mod.getResidue();
            omics.util.protein.AminoAcid aa = null;
            Pos pos = null;
            switch (mod.getLocation()) {
                case C_Term: {
                    if (residue == '*')
                        aa = omics.util.protein.AminoAcid.C_TERM;
                    pos = Pos.ANY_C_TERM;
                    break;
                }
                case N_Term: {
                    if (residue == '*')
                        aa = omics.util.protein.AminoAcid.N_TERM;
                    pos = Pos.ANY_N_TERM;
                    break;
                }
                case Protein_C_Term: {
                    if (residue == '*')
                        aa = omics.util.protein.AminoAcid.C_TERM;
                    pos = Pos.PROTEIN_C_TERM;
                    break;
                }
                case Protein_N_Term: {
                    if (residue == '*')
                        aa = omics.util.protein.AminoAcid.N_TERM;
                    pos = Pos.PROTEIN_N_TERM;
                    break;
                }
                case Anywhere: {
                    aa = omics.util.protein.AminoAcid.valueOf(residue);
                    pos = Pos.ANY_WHERE;
                    break;
                }
            }
            Specificity specificity = new Specificity(aa, pos);

            PTM newPTM;
            PTM ptm = factory.get(modName);
            if (ptm == null) {
                Composition composition = mod.getModification().getComposition();
                omics.util.chem.Composition.Builder builder = new omics.util.chem.Composition.Builder();
                builder.add(PeriodicTable.C, composition.getC());
                builder.add(PeriodicTable.H, composition.getH());
                builder.add(PeriodicTable.O, composition.getO());
                builder.add(PeriodicTable.N, composition.getN());
                builder.add(PeriodicTable.S, composition.getS());
                omics.util.chem.Composition comp = builder.build();

                if (!MathUtils.equal(comp.getMolecularMass(), composition.getAccurateMass(), 0.01)) {
                    throw new OmicsRuntimeException("PTM mass is not right: " + modName);
                }

                newPTM = new PTM(modName, comp);
            } else {
                newPTM = new PTM(ptm);
            }
            newPTM.addSpecificity(specificity);

            if (mod.isFixedModification()) {
                psmParameter.addFixedMod(newPTM);
            } else {
                psmParameter.addVariableMod(newPTM);
            }
        }
    }

    public void setEValueThreshold(float eValueThreshold)
    {
        this.eValueThreshold = eValueThreshold;
    }

    private void addPSMList()
    {
//        System.out.println("To Add: " + resultList.size());
        for (MSGFPlusMatch mpMatch : resultList) {
            List<DatabaseMatch> matchList = mpMatch.getMatchList();
            if (matchList == null || matchList.isEmpty())
                continue;

            int specIndex = mpMatch.getSpecIndex();
            SpectrumIdentifier identifier = new SpectrumIdentifier();
            identifier.setIndex(specIndex);
            identifier.setId(sirID + specIndex);

//            System.out.println("Index " + specIndex);
            Spectrum spec = specAcc.getSpecMap().getSpectrumBySpecIndex(specIndex);
            float precursorMz = spec.getPrecursorPeak().getMz();
            identifier.setPrecursorMz(precursorMz);
            identifier.setSpectraData(parameters.getSpectraData());

            // add title
            String title = spec.getTitle();
            if (title != null)
                identifier.setSpectrumTitle(title);

            // add scan number
            int scanNum = spec.getScanNum();
            if (scanNum >= 0)
                identifier.addScanNumber(scanNum);

            // add retention time
            float scanStartTime = spec.getRt();
            if (scanStartTime >= 0) {
                if (spec.getRtIsSeconds())
                    identifier.addRetentionTime(scanStartTime, TimeUnit.SECOND);
                else
                    identifier.addRetentionTime(scanStartTime, TimeUnit.MINUTE);
            }

//            System.out.println("Match size: " + matchList.size());
            int rank = 0;
            int resultCount = 0;
            double prevSpecEValue = Double.NaN;
            for (int i = matchList.size() - 1; i >= 0; --i) {
                ++resultCount;
                DatabaseMatch match = matchList.get(i);
//                System.out.println("denovo=" + match.getDeNovoScore() + ";" + match.getPepSeq());
                if (match.getDeNovoScore() < params.getMinDeNovoScore())
                    break;

                int length = match.getLength();
                int charge = match.getCharge();

                float peptideMass = match.getPeptideMass();
                float theoMz = (peptideMass + (float) Composition.H2O) / charge + (float) Composition.ChargeCarrierMass();

                int score = match.getScore();
                double specEValue = match.getSpecEValue();
                int numPeptides = sa.getNumDistinctPeptides(params.getEnzyme() == null ? length - 2 : length - 1);
                double eValue = specEValue * numPeptides;

                // Specification: rank does not increment for equally-scored results
                if (prevSpecEValue != specEValue)
                    ++rank;

                prevSpecEValue = specEValue;

                PeptideMatch peptideMatch = getPeptideMatch(match);
                PeptideSpectrumMatch psm = new PeptideSpectrumMatch(peptideMatch, identifier);
                psm.setRank(rank);
                psm.setCharge(charge);
                psm.setPassThreshold(eValue <= eValueThreshold);
                psm.setId(siiID + specIndex + "_" + resultCount);
                psm.setCalculatedMz((double) theoMz);

                psm.addScore(ScoreType.MSGF_RAW_SCORE, score);
                psm.addScore(ScoreType.MSGF_DENOVO_SCORE, match.getDeNovoScore());
                psm.addScore(ScoreType.MSGF_SPEC_EVALUE, specEValue);
                psm.addScore(ScoreType.MSGF_EVALUE, eValue);

                float expMass = precursorMz * charge;
                float theoMass = theoMz * charge;

                float deltaMass = expMass - theoMass;
                IsoDeltaEntry isoDeltaEntry = params.getIsoDeltaEntry(deltaMass);
                psm.addPM("IsotopeError", isoDeltaEntry.getIsotopeNum());

                DeltaEntry deltaEntry = isoDeltaEntry.getDeltaEntry();
                if (!params.getDeltaEntries().isEmpty()) {
                    psm.addPM("Delta Entry", deltaEntry.getFirst());
                    psm.addPM("Delta Mass", deltaEntry.getSecond());
                }

                double totalTheoMass = theoMass + isoDeltaEntry.getMolecularMass();
                double deltaDa = expMass - totalTheoMass;
                psm.setMassDiffDa(deltaDa);
                psm.setMassDiffPpm(deltaDa * 1E6 / totalTheoMass);

                ActivationMethod[] activationMethodArr = match.getActivationMethodArr();
                if (activationMethodArr != null) {
                    StringBuffer actMethodStrBuf = new StringBuffer();
                    actMethodStrBuf.append(activationMethodArr[0]);
                    for (int j = 1; j < activationMethodArr.length; j++)
                        actMethodStrBuf.append("/" + activationMethodArr[j]);
                    psm.addPM("AssumedDissociationMethod", actMethodStrBuf.toString());
                }

                if (match.getAdditionalFeatureList() != null) {
                    for (Pair<String, String> feature : match.getAdditionalFeatureList()) {
                        String name = feature.getFirst();
                        String value = feature.getSecond();
                        psm.addPM(name, value);
                    }
                }

                identResult.add(psm);
            }
        }
    }

    private PeptideMatch getPeptideMatch(DatabaseMatch match)
    {
        String pepStr = match.getPepSeq();
        PeptideMatch peptideMatch = peptideMatchMap.get(pepStr);

        if (peptideMatch == null) {
            // new peptide variant
            ListMultimap<Integer, PTM> sideChainMatchMap = ArrayListMultimap.create();
            ListMultimap<ModAttachment, PTM> termMatchMap = ArrayListMultimap.create();

            StringBuffer unmodPepStr = new StringBuffer();
            StringBuffer modPepStr = new StringBuffer();

            for (int i = 0; i < pepStr.length(); i++) {
                AminoAcid aa = aaSet.getAminoAcid(pepStr.charAt(i));
                char residue = aa.getUnmodResidue();
                unmodPepStr.append(residue);

                List<Double> modMasses = new ArrayList<>();
                boolean hasNTermMod = false;
                List<Double> nTermMasses = new ArrayList<>();
                boolean hasCTermMod = false;
                List<Double> cTermMasses = new ArrayList<>();
                boolean modified = false;

                omics.util.protein.AminoAcid aminoAcid = omics.util.protein.AminoAcid.valueOf(aa.getUnmodResidue());
                if (i == 0) {
                    List<PTM> fixedMods = getTerminalFixedModifications(aminoAcid, Location.N_Term);
                    if (!fixedMods.isEmpty())
                        termMatchMap.putAll(ModAttachment.N_TERM, fixedMods);
                } else if (i == pepStr.length() - 1) {
                    List<PTM> fixMods = getTerminalFixedModifications(aminoAcid, Location.C_Term);
                    if (!fixMods.isEmpty())
                        termMatchMap.putAll(ModAttachment.C_TERM, fixMods);
                }

                List<PTM> fixMods = getFixedModifications(aminoAcid);
                if (!fixMods.isEmpty())
                    sideChainMatchMap.putAll(i, fixMods);

                if (aa.isModified()) {
                    ModifiedAminoAcid modAA = (ModifiedAminoAcid) aa;
                    PTM variableMod = psmParameter.getVariableMod(modAA.getModification().getName());

                    if (i == 0 && modAA.isNTermVariableMod()) {
                        nTermMasses.add(modAA.getModification().getAccurateMass());
                        termMatchMap.put(ModAttachment.N_TERM, variableMod);
                        hasNTermMod = true;
                    } else if (i == pepStr.length() - 1 && modAA.isCTermVariableMod()) {
                        cTermMasses.add(modAA.getModification().getAccurateMass());
                        termMatchMap.put(ModAttachment.C_TERM, variableMod);
                        hasCTermMod = true;
                    } else {
                        modMasses.add(modAA.getModification().getAccurateMass());
                        sideChainMatchMap.put(i, variableMod);
                        modified = true;
                    }

                    while (modAA.getTargetAA().isModified())    // aa has two (or more) modifications
                    {
                        modAA = (ModifiedAminoAcid) modAA.getTargetAA();
                        PTM variableMod2 = psmParameter.getVariableMod(modAA.getModification().getName());
                        if (i == 0 && modAA.isNTermVariableMod()) {
                            nTermMasses.add(modAA.getModification().getAccurateMass());
                            termMatchMap.put(ModAttachment.N_TERM, variableMod2);
                            hasNTermMod = true;
                        } else if (i == pepStr.length() - 1 && modAA.isCTermVariableMod()) {
                            cTermMasses.add(modAA.getModification().getAccurateMass());
                            termMatchMap.put(ModAttachment.C_TERM, variableMod2);
                            hasCTermMod = true;
                        } else {
                            modMasses.add(modAA.getModification().getAccurateMass());
                            sideChainMatchMap.put(i, variableMod2);
                            modified = true;
                        }
                    }
                }

                if (hasNTermMod) {
                    modPepStr.append("[");
                    Collections.sort(nTermMasses);
                    for (Double nTMass : nTermMasses) {
                        if (nTMass >= 0)
                            modPepStr.append("+");
                        modPepStr.append(Math.round(nTMass));
                    }
                }

                modPepStr.append(residue);
                if (modified) {
                    Collections.sort(modMasses);
                    for (Double mMass : modMasses) {
                        if (mMass >= 0)
                            modPepStr.append("+");
                        modPepStr.append(Math.round(mMass));
                    }
                }

                if (hasCTermMod) {
                    modPepStr.append("}");
                    Collections.sort(cTermMasses);
                    for (Double cTMass : cTermMasses) {
                        if (cTMass >= 0)
                            modPepStr.append("+");
                        modPepStr.append(Math.round(cTMass));
                    }
                }
            }

            peptideMatch = new PeptideMatch(unmodPepStr.toString());
            for (int key : sideChainMatchMap.keySet()) {
                List<PTM> ptmList = sideChainMatchMap.get(key);
                for (PTM ptm : ptmList)
                    peptideMatch.addModificationMatch(key, ptm);
            }
            for (ModAttachment modAttachment : termMatchMap.keySet()) {
                List<PTM> ptmList = termMatchMap.get(modAttachment);
                for (PTM ptm : ptmList)
                    peptideMatch.addModificationMatch(modAttachment, ptm);
            }

            peptideMatch.setId(pepIDPrefix + modPepStr.toString());
            List<PeptideProteinMatch> peptideProteinMatches = getPeptideProteinMatches(match, peptideMatch);
            peptideMatch.addPeptideProteinMatches(peptideProteinMatches);

            peptideMatchMap.put(pepStr, peptideMatch);
        }

        return peptideMatch;
    }

    private List<PeptideProteinMatch> getPeptideProteinMatches(DatabaseMatch match, PeptideMatch peptide)
    {
        SortedSet<Integer> indices = match.getIndices();
        int length = match.getLength();

        int startKey = indices.first();

        String pepIDNum = peptide.getId().substring(pepIDPrefix.length());
        String annotationKey = (match.isNTermMetCleaved() ? "M" : "") + startKey + "_" + pepIDNum;
        List<PeptideProteinMatch> evRefList = evRefListMap.get(annotationKey);

        if (evRefList == null) {
            evRefList = new ArrayList<>();

            CompactFastaSequence seq = sa.getSequence();
            for (int index : indices) {
                boolean isNTermMetCleaved = match.isNTermMetCleaved() &&
                        sa.getSequence().getCharAt(index + 1) == 'M';

                char pre = sa.getSequence().getCharAt(index);
                if (pre == '_') {
                    if (isNTermMetCleaved)
                        pre = 'M';
                    else
                        pre = '-';
                }
                char post;
                if (isNTermMetCleaved)
                    post = sa.getSequence().getCharAt(index + length);
                else
                    post = sa.getSequence().getCharAt(index + length - 1);

                if (post == '_')
                    post = '-';

                int protStartIndex = (int) seq.getStartPosition(index);
                ProteinMatch proteinMatch = getProteinMatch(protStartIndex);

                PeptideProteinMatch ppm = new PeptideProteinMatch(proteinMatch.getAccession());
                ppm.setPreviousAA(String.valueOf(pre));
                ppm.setNextAA(String.valueOf(post));
                ppm.setProteinMatch(proteinMatch);
                ppm.setPeptideMatch(peptide);

                int start = index - protStartIndex + 1;
                if (isNTermMetCleaved)
                    ++start;

                int end = start + length - 2 - 1;
                ppm.setStart(start);
                ppm.setEnd(end);

                String pepEvKey = pepEvIDPrefix + (index + 1) + "_" + pepIDNum + "_" + start;
                ppm.setId(pepEvKey);
                ppm.setHitType(isDecoyMap.get(protStartIndex) ? HitType.DECOY : HitType.TARGET);

                if (pepEvMap.get(pepEvKey) != null) {
                    // Avoid duplicate peptide evidences
                    // currently only occurs when there are 2 results for a single peptide/mod/dbseq combo,
                    // one where match.isNTermMetCleaved() is true and sa.getSequence().getCharAt(index + 1) == 'M' is false,
                    // and another where match.isNTermMetCleaved() is false.
                    ppm = pepEvMap.get(pepEvKey);
                } else {
                    pepEvMap.put(pepEvKey, ppm);
                }

                evRefList.add(ppm);
            }
            evRefListMap.put(annotationKey, evRefList);
        }

        return evRefList;
    }

    /**
     * return the {@link ProteinMatch} of given index in the .seq file
     *
     * @param protStartIndex protein start index
     */
    private ProteinMatch getProteinMatch(int protStartIndex)
    {
        ProteinMatch proteinMatch = proteinMatchMap.get(protStartIndex);
        if (proteinMatch == null) {

            CompactFastaSequence seq = sa.getSequence();
            String annotation = seq.getAnnotation(protStartIndex);
            String proteinSeq = seq.getMatchingEntry(protStartIndex);
            String accession = annotation.split("\\s+")[0];

            proteinMatch = new ProteinMatch(accession);
            proteinMatch.setLength(proteinSeq.length());
            proteinMatch.setDatabase(parameters.getDatabaseInfo().getId());
            proteinMatch.setId("DBSeq" + (protStartIndex + 1));

            boolean isDecoy = accession.startsWith(ReverseDB.DECOY_PROTEIN_PREFIX);
            if (!isDecoy)
                proteinMatch.setDescription(annotation);

            proteinMatchMap.put(protStartIndex, proteinMatch);
            isDecoyMap.put(protStartIndex, isDecoy);
        }

        return proteinMatch;
    }

    private List<PTM> getFixedModifications(omics.util.protein.AminoAcid aminoAcid)
    {
        List<PTM> ptmList = new ArrayList<>();
        for (PTM ptm : psmParameter.getFixedMods()) {
            for (Specificity specificity : ptm.getSpecificityList()) {
                if (specificity.getPosition() == Pos.ANY_WHERE && specificity.getAminoAcid() == aminoAcid) {
                    ptmList.add(ptm);
                    break;
                }
            }
        }
        return ptmList;
    }

    private List<PTM> getTerminalFixedModifications(omics.util.protein.AminoAcid aminoAcid, Location location)
    {
        List<PTM> ptmList = new ArrayList<>();
        List<PTM> fixedMods = psmParameter.getFixedMods();
        if (location == Location.N_Term) {
            for (PTM ptm : fixedMods) {
                for (Specificity specificity : ptm.getSpecificityList()) {
                    if (specificity.getPosition() == Pos.ANY_N_TERM && (specificity.getAminoAcid() == aminoAcid
                            || specificity.getAminoAcid() == omics.util.protein.AminoAcid.N_TERM
                    )) {
                        ptmList.add(ptm);
                        break;
                    }
                }
            }
        } else if (location == Location.C_Term) {
            for (PTM ptm : fixedMods) {
                for (Specificity specificity : ptm.getSpecificityList()) {
                    if (specificity.getPosition() == Pos.ANY_C_TERM && (specificity.getAminoAcid() == aminoAcid
                            || specificity.getAminoAcid() == omics.util.protein.AminoAcid.C_TERM
                    )) {
                        ptmList.add(ptm);
                        break;
                    }
                }
            }
        }
        return ptmList;
    }

}
