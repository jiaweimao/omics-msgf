package edu.ucsd.msjava.params;

/**
 * abstract Parameter class.
 */
public abstract class Parameter
{
    private String key;
    private String name;
    private String description;
    private boolean isOptional = false;
    private boolean isValueAssigned = false;
    private String additionalDescription = null;

    private boolean hidden = false;

    /**
     * Create a parameter
     *
     * @param key         parameter key
     * @param name        parameter name
     * @param description description
     */
    protected Parameter(String key, String name, String description)
    {
        this.key = key;
        this.name = name;
        this.description = description;
    }

    /**
     * set this parameter as optional, it means that it has default value.
     */
    protected void setOptional()
    {
        this.isOptional = true;
    }

    public void setHidden()
    {
        this.hidden = true;
    }

    /**
     * @return parameter key
     */
    public String getKey()
    {
        return key;
    }

    /**
     * @return parameter name
     */
    public String getName()
    {
        return name;
    }

    public String getDescription()
    {
        return description;
    }

    public String getAdditionalDescription()
    {
        return additionalDescription;
    }

    public boolean isOptional()
    {
        return isOptional;
    }

    public boolean isHidden()
    {
        return hidden;
    }

    public void setAdditionalDescription(String additionalDescription)
    {
        this.additionalDescription = additionalDescription;
    }

    public String toString()
    {
        String usage = "-" + getKey() + " " + getName();
        if (isOptional())
            usage = "[" + usage + "]";
        usage = usage + " " + "(" + getDescription() + ")";
        return usage;
    }

    public void setValueAssigned()
    {
        this.isValueAssigned = true;
    }

    public boolean isValueAssigned()
    {
        return isValueAssigned;
    }

    public boolean isValid()
    {
        return !(!isOptional && !isValueAssigned());
    }

    /**
     * parse the parameter from the string.
     *
     * @param value string to parse
     * @return error message.
     */
    public abstract String parse(String value);

    public abstract String getValueAsString();
}
