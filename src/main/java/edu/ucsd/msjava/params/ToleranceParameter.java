package edu.ucsd.msjava.params;

import omics.util.ms.peaklist.Tolerance;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;
import omics.util.ms.peaklist.impl.PpmTolerance;
import omics.util.utils.Pair;

public class ToleranceParameter extends Parameter
{
    private Tolerance tolerance;
    private boolean allowAsymmetricValues = true;

    public ToleranceParameter(String key, String name, String description)
    {
        super(key, name, description);
    }

    public ToleranceParameter defaultValue(String value)
    {
        super.setOptional();
        String error = parse(value);
        if (error != null) {
            System.err.println("(ToleranceParameter) Error while setting default value: " + error);
            System.exit(-1);
        }
        return this;
    }

    public ToleranceParameter doNotAllowAsymmetricValues()
    {
        this.allowAsymmetricValues = false;
        return this;
    }

    public static Pair<Double, Boolean> parseStr(String tolStr)
    {
        double value;
        boolean isPPm;
        String tolStrcase = tolStr.toLowerCase();
        try {
            if (tolStrcase.endsWith("ppm")) {
                isPPm = true;
                value = Double.parseDouble(tolStrcase.substring(0, tolStrcase.length() - 3).trim());
            } else if (tolStrcase.endsWith("da")) {
                isPPm = false;
                value = Double.parseDouble(tolStrcase.substring(0, tolStrcase.length() - 2).trim());
            } else {
                value = Double.parseDouble(tolStrcase);
                isPPm = false;
            }
        } catch (NumberFormatException e) {
            return null;
        }
        return Pair.create(value, isPPm);
    }

    @Override
    public String parse(String value)
    {
        String[] token = value.split(",");
        if (token.length == 1) {
            String tolStr = token[0].toLowerCase();

            Pair<Double, Boolean> vals = parseStr(tolStr);
            if (vals == null)
                return "invalid tolerance value";

            if (vals.getSecond()) {
                tolerance = new PpmTolerance(vals.getFirst());
            } else {
                tolerance = new AbsoluteTolerance(vals.getFirst());
            }
        } else if (token.length == 2) {
            if (allowAsymmetricValues) {
                Pair<Double, Boolean> val1 = parseStr(token[0]);
                Pair<Double, Boolean> val2 = parseStr(token[1]);

                if (val1 == null || val2 == null)
                    return "invalid tolerance value";

                if (val1.getSecond()) {
                    tolerance = new PpmTolerance(val1.getFirst(), val2.getFirst());
                } else {
                    tolerance = new AbsoluteTolerance(val1.getFirst(), val2.getFirst());
                }
            } else
                return "asymmetric values are not allowed";
        }
        if (tolerance.getMinusError() < 0 || tolerance.getPlusError() < 0) {
            return "parent mass tolerance must not be negative";
        }
        return null;
    }

    @Override
    public String getValueAsString()
    {
        if (tolerance == null)
            return null;
        return tolerance.toString();
    }

    public Tolerance getTolerance()
    {
        return tolerance;
    }
}
