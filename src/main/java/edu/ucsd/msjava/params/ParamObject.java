package edu.ucsd.msjava.params;

import omics.util.cv.Cv;
import omics.util.cv.CvParam;

public interface ParamObject
{
    /**
     * Helper method to create and return a CvParam from accession, name and CV
     *
     * @return CvParam
     */
    static CvParam makeCvParam(String accession, String name)
    {
        return makeCvParam(accession, name, Cv.PSI_MS);
    }

    /**
     * Helper method to create and return a CvParam from accession, name and CV
     *
     * @return CvParam
     */
    static CvParam makeCvParam(String accession, String name, Cv cv)
    {
        return new CvParam(cv.getId(), accession, name, "");
    }


    String getParamDescription();
}
