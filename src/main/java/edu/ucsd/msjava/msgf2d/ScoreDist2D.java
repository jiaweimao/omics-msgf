package edu.ucsd.msjava.msgf2d;

public class ScoreDist2D extends ScoreBound2D
{
    private float[][] numDistribution;
    private float[][] probDistribution;

    public ScoreDist2D(int minScore1, int maxScore1, int minScore2, int maxScore2)
    {
        super(minScore1, maxScore1, minScore2, maxScore2);
        numDistribution = new float[scoreBound1.getRange()][scoreBound2.getRange()];
        probDistribution = new float[scoreBound1.getRange()][scoreBound2.getRange()];
    }

    public ScoreDist2D(ScoreBound2D scoreBound)
    {
        super(scoreBound.scoreBound1, scoreBound.scoreBound2);
        numDistribution = new float[scoreBound1.getRange()][scoreBound2.getRange()];
        probDistribution = new float[scoreBound1.getRange()][scoreBound2.getRange()];
    }

    public void setNumber(int score1, int score2, float number)
    {
        numDistribution[score1 - scoreBound1.getMinScore()][score2 - scoreBound2.getMinScore()] = number;
    }

    public void setProb(int score1, int score2, float prob)
    {
        probDistribution[score1 - scoreBound1.getMinScore()][score2 - scoreBound2.getMinScore()] = prob;
    }

    public void addNumber(int score1, int score2, float number)
    {
        numDistribution[score1 - scoreBound1.getMinScore()][score2 - scoreBound2.getMinScore()] += number;
    }

    public void addProb(int score1, int score2, float prob)
    {
        probDistribution[score1 - scoreBound1.getMinScore()][score2 - scoreBound2.getMinScore()] += prob;
    }

    public float getProbability(int score1, int score2)
    {
        int index1 = (score1 >= scoreBound1.getMinScore()) ? score1 - scoreBound1.getMinScore() : 0;
        int index2 = (score2 >= scoreBound2.getMinScore()) ? score2 - scoreBound2.getMinScore() : 0;
        return probDistribution[index1][index2];
    }

    public float getNumRecs(int score1, int score2)
    {
        int index1 = (score1 >= scoreBound1.getMinScore()) ? score1 - scoreBound1.getMinScore() : 0;
        int index2 = (score2 >= scoreBound2.getMinScore()) ? score2 - scoreBound2.getMinScore() : 0;
        return numDistribution[index1][index2];
    }


    public void addNumDist(ScoreDist2D otherDist, int scoreDiff1, int scoreDiff2, int coeff)
    {
        if (otherDist == null)
            return;
        int minScore1 = scoreBound1.getMinScore();
        int minScore2 = scoreBound2.getMinScore();

        for (int t1 = Math.max(otherDist.scoreBound1.getMinScore(), scoreBound1.getMinScore() - scoreDiff1); t1 < otherDist.scoreBound1.getMaxScore(); t1++) {
            for (int t2 = Math.max(otherDist.scoreBound2.getMinScore(), scoreBound2.getMinScore() - scoreDiff2); t2 < otherDist.scoreBound2.getMaxScore(); t2++) {
                numDistribution[t1 + scoreDiff1 - minScore1][t2 + scoreDiff2 - minScore2] += coeff * otherDist.numDistribution[t1 - otherDist.scoreBound1.getMinScore()][t2 - otherDist.scoreBound2.getMinScore()];
            }
        }
    }

    public void addProbDist(ScoreDist2D otherDist, int scoreDiff1, int scoreDiff2, float aaProb)
    {
        if (otherDist == null)
            return;
        int minScore1 = scoreBound1.getMinScore();
        int minScore2 = scoreBound2.getMinScore();

        for (int t1 = Math.max(otherDist.scoreBound1.getMinScore(), scoreBound1.getMinScore() - scoreDiff1); t1 < otherDist.scoreBound1.getMaxScore(); t1++) {
            for (int t2 = Math.max(otherDist.scoreBound2.getMinScore(), scoreBound2.getMinScore() - scoreDiff2); t2 < otherDist.scoreBound2.getMaxScore(); t2++) {
                probDistribution[t1 + scoreDiff1 - minScore1][t2 + scoreDiff2 - minScore2] += aaProb * otherDist.probDistribution[t1 - otherDist.scoreBound1.getMinScore()][t2 - otherDist.scoreBound2.getMinScore()];
            }
        }
    }

}
