package edu.ucsd.msjava.msutil;

import edu.ucsd.msjava.parser.SpectrumParser;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;

/**
 * a list store annotated spectrum
 *
 * @version 1.0.0
 * @since 26 Jan 2019, 1:15 PM
 */
public class SpectraContainer extends ArrayList<Spectrum>
{
    private static final long serialVersionUID = 1L;

    public SpectraContainer()
    {
    }

    public SpectraContainer(String fileName, SpectrumParser parser)
    {
        try {
            SpectraIterator iterator = new SpectraIterator(fileName, parser);
            while (iterator.hasNext())
                this.add(iterator.next());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void outputMgfFile(String fileName)
    {
        PrintStream out = null;
        try {
            out = new PrintStream(new BufferedOutputStream(new FileOutputStream(fileName)));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        assert out != null;

        for (Spectrum spec : this) {
            spec.outputMgf(out);
            out.println();
        }
        out.close();
    }
}
