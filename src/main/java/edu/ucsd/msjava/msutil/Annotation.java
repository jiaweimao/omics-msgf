package edu.ucsd.msjava.msutil;

/**
 * This class represents an identified peptide.
 *
 * @version 1.0.0
 * @since 29 Jan 2019, 2:06 PM
 */
public class Annotation
{
    private AminoAcid prevAA;
    private Peptide peptide;
    private AminoAcid nextAA;

    /**
     * Create an {@link Annotation}
     *
     * @param prevAA  previous {@link AminoAcid} in the protein
     * @param peptide {@link Peptide}
     * @param nextAA  next {@link AminoAcid} in the protein
     */
    public Annotation(AminoAcid prevAA, Peptide peptide, AminoAcid nextAA)
    {
        this.prevAA = prevAA;
        this.peptide = peptide;
        this.nextAA = nextAA;
    }

    /**
     * Create an {@link Annotation}
     *
     * @param annotationStr peptide string with format preAA.pepseq.nextAA
     * @param aaSet         {@link AminoAcidSet} to parse amino acid chars
     */
    public Annotation(String annotationStr, AminoAcidSet aaSet)
    {
        String pepStr = annotationStr.substring(annotationStr.indexOf('.') + 1, annotationStr.lastIndexOf('.'));
        char prevAAResidue = annotationStr.charAt(0);
        char nextAAResidue = annotationStr.charAt(annotationStr.length() - 1);

        prevAA = aaSet.getAminoAcid(prevAAResidue);
        peptide = aaSet.getPeptide(pepStr);
        nextAA = aaSet.getAminoAcid(nextAAResidue);
    }

    /**
     * @return true if the peptide begins with protein N-term
     */
    public boolean isProteinNTerm()
    {
        return prevAA == null;
    }

    /**
     * @return true if the peptide ends with protein C-term
     */
    public boolean isProteiNCTerm()
    {
        return nextAA == null;
    }

    /**
     * @return previous {@link AminoAcid}
     */
    public AminoAcid getPrevAA()
    {
        return prevAA;
    }

    /**
     * setter of previous {@link AminoAcid}
     *
     * @param prevAA {@link AminoAcid}
     */
    public void setPrevAA(AminoAcid prevAA)
    {
        this.prevAA = prevAA;
    }

    public Peptide getPeptide()
    {
        return peptide;
    }

    public void setPeptide(Peptide peptide)
    {
        this.peptide = peptide;
    }

    public AminoAcid getNextAA()
    {
        return nextAA;
    }

    public void setNextAA(AminoAcid nextAA)
    {
        this.nextAA = nextAA;
    }

    @Override
    public String toString()
    {
        if (peptide == null)
            return null;
        StringBuilder output = new StringBuilder();
        if (prevAA != null)
            output.append(prevAA.getResidueStr());
        output.append(".").append(peptide.toString()).append(".");
        if (nextAA != null)
            output.append(nextAA.getResidueStr());
        return output.toString();
    }
}
