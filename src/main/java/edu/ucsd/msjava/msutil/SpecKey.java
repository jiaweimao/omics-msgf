package edu.ucsd.msjava.msutil;

import omics.util.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

public class SpecKey extends Pair<Integer, Integer>
{
    private static final Logger logger = LoggerFactory.getLogger(SpecKey.class);

    private ArrayList<Integer> specIndexList;
    private float precursorMz;

    /**
     * Contructor of {@link SpecKey}
     *
     * @param specIndex spectrum index
     * @param charge    precursor charge
     */
    public SpecKey(int specIndex, int charge)
    {
        super(specIndex, charge);
    }

    public void setPrecursorMz(float precursorMz)
    {
        this.precursorMz = precursorMz;
    }

    /**
     * @return index of the spectrum
     */
    public int getSpecIndex()
    {
        return super.getFirst();
    }

    /**
     * @return precursor charge of the spectrum
     */
    public int getCharge()
    {
        return super.getSecond();
    }

    public float getPrecursorMz()
    {
        return precursorMz;
    }

    public String getSpecKeyString()
    {
        return getSpecIndex() + ":" + getCharge();
    }

    public void addSpecIndex(int scanNum)
    {
        if (specIndexList == null) {
            specIndexList = new ArrayList<>();
        }
        specIndexList.add(scanNum);
    }

    @Override
    public String toString()
    {
        return getSpecKeyString();
    }

    public ArrayList<Integer> getSpecIndexList()
    {
        return specIndexList;
    }

    /**
     * generate keys for given {@link Spectrum} iterator.
     *
     * @param itr                    {@link Iterator} of {@link Spectrum}
     * @param startSpecIndex         defined start index for filter
     * @param endSpecIndex           defined end index for filter
     * @param minCharge              minimum charge allowed
     * @param maxCharge              maximum charge allowed
     * @param activationMethod       {@link ActivationMethod} of spectrum
     * @param minNumPeaksPerSpectrum minimum number of peaks required.
     * @return {@link SpecKey} list
     */
    public static ArrayList<SpecKey> getSpecKeyList(Iterator<Spectrum> itr,
                                                    int startSpecIndex, int endSpecIndex,
                                                    int minCharge, int maxCharge,
                                                    ActivationMethod activationMethod,
                                                    int minNumPeaksPerSpectrum)
    {
        if (activationMethod == ActivationMethod.FUSION)
            return getFusedSpecKeyList(itr, startSpecIndex, endSpecIndex, minCharge, maxCharge);

        ArrayList<SpecKey> specKeyList = new ArrayList<>();

        int numProfileSpectra = 0;
        int numSpectraWithTooFewPeaks = 0; // spectrum count with peaks less than limit
        final int MAX_INFORMATIVE_MESSAGES = 2;
        int informativeMessageCount = 0;
        while (itr.hasNext()) {
            Spectrum spec = itr.next();
            int specIndex = spec.getSpecIndex();
            if (specIndex < startSpecIndex || specIndex >= endSpecIndex)
                continue;

            spec.setChargeIfSinglyCharged();
            int charge = spec.getCharge();
            ActivationMethod specActivationMethod = spec.getActivationMethod();

            if (activationMethod == ActivationMethod.ASWRITTEN) {

            } else if (specActivationMethod != null) {
                // If specActivationMethod is null, we use whatever was specified
                //   - some supported spectra input types do not allow/require activation method
                if (activationMethod == ActivationMethod.UVPD && specActivationMethod == ActivationMethod.HCD) {
                    if (informativeMessageCount < MAX_INFORMATIVE_MESSAGES) {
                        logger.info("Use spectrum {} since Thermo currently labels UVPD spectra as HCD", spec.getID());
                        informativeMessageCount++;
                    } else {
                        if (informativeMessageCount == MAX_INFORMATIVE_MESSAGES) {
                            logger.info("...");
                            informativeMessageCount++;
                        }
                    }
                } else {
                    if (specActivationMethod != activationMethod) {
                        if (informativeMessageCount < MAX_INFORMATIVE_MESSAGES) {
                            logger.info("Skip spectrum {} since activationMethod is {}, not {}", spec.getID(), specActivationMethod, activationMethod);
                            informativeMessageCount++;
                        } else {
                            if (informativeMessageCount == MAX_INFORMATIVE_MESSAGES) {
                                logger.info(" ...");
                                informativeMessageCount++;
                            }
                        }
                        continue;
                    }
                }
            } else {
                // specActivationMethod is null
                if (informativeMessageCount < MAX_INFORMATIVE_MESSAGES) {
                    logger.info("Spectrum {} activationMethod is unknown; Using {} as specified in parameters.", spec.getID(), activationMethod);
                    informativeMessageCount++;
                } else {
                    if (informativeMessageCount == MAX_INFORMATIVE_MESSAGES) {
                        logger.info(" ...");
                        informativeMessageCount++;
                    }
                }
            }

            if (!spec.isCentroided()) {
                if (informativeMessageCount < MAX_INFORMATIVE_MESSAGES) {
                    logger.info("Skip spectrum {} since it is not centroided", spec.getID());
                    informativeMessageCount++;
                } else {
                    if (informativeMessageCount == MAX_INFORMATIVE_MESSAGES) {
                        logger.info(" ...");
                        informativeMessageCount++;
                    }
                }
                numProfileSpectra++;
                continue;
            }

            if (spec.size() < minNumPeaksPerSpectrum) {
                numSpectraWithTooFewPeaks++;
                continue;
            }

            if (charge == 0) {
                for (int c = minCharge; c <= maxCharge; c++)
                    specKeyList.add(new SpecKey(specIndex, c));
            } else if (charge > 0) {
                specKeyList.add(new SpecKey(specIndex, charge));
            }
        }

        logger.info("Ignoring " + numProfileSpectra + " profile spectra.");
        logger.info("Ignoring " + numSpectraWithTooFewPeaks + " spectra having less than " + minNumPeaksPerSpectrum + " peaks.\n");
        return specKeyList;
    }

    /**
     * return {@link SpecKey} list for {@link ActivationMethod#FUSION}
     *
     * @param itr            {@link Spectrum} iterator.
     * @param startSpecIndex
     * @param endSpecIndex
     * @param minCharge
     * @param maxCharge
     * @return
     */
    public static ArrayList<SpecKey> getFusedSpecKeyList(Iterator<Spectrum> itr, int startSpecIndex, int endSpecIndex, int minCharge, int maxCharge)
    {
        HashMap<Peak, ArrayList<Integer>> precursorSpecIndexMap = new HashMap<>();

        while (itr.hasNext()) {
            Spectrum spec = itr.next();
            int specIndex = spec.getSpecIndex();
            if (specIndex < startSpecIndex || specIndex >= endSpecIndex)
                continue;
            Peak precursor = spec.getPrecursorPeak();
            if (spec.getActivationMethod() == null) {
                System.out.println("Error: activation method is not available: Scan=" + spec.getSpecIndex() + ", PrecursorMz=" + spec.getPrecursorPeak().getMz());
                System.exit(-1);
            }

            ArrayList<Integer> list = precursorSpecIndexMap.get(precursor);
            if (list == null) {
                list = new ArrayList<>();
                precursorSpecIndexMap.put(precursor, list);
            }
            list.add(specIndex);
        }

        Iterator<Entry<Peak, ArrayList<Integer>>> mapItr = precursorSpecIndexMap.entrySet().iterator();
        ArrayList<SpecKey> specKeyList = new ArrayList<SpecKey>();
        while (mapItr.hasNext()) {
            Entry<Peak, ArrayList<Integer>> entry = mapItr.next();
            Peak precursor = entry.getKey();
            ArrayList<Integer> list = entry.getValue();
            Collections.sort(list);

            int charge = precursor.getCharge();
            if (charge == 0) {
                for (int c = minCharge; c <= maxCharge; c++) {
                    SpecKey specKey = new SpecKey(list.get(0), c);
                    for (int specIndex : list)
                        specKey.addSpecIndex(specIndex);
                    specKeyList.add(specKey);
                }
            } else if (charge > 0) {
                SpecKey specKey = new SpecKey(list.get(0), charge);
                for (int specIndex : list)
                    specKey.addSpecIndex(specIndex);
                specKeyList.add(specKey);
            } else {
                System.out.println("Error: negative precursor charge: " + precursor);
                System.exit(-1);
            }
        }
        return specKeyList;
    }
}
