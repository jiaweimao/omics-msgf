package edu.ucsd.msjava.msutil;

import edu.ucsd.msjava.params.ParamObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Objects;


/**
 * This class represents an enzyme.
 *
 * @author sangtaekim
 */
public class Enzyme implements ParamObject
{
    private static final Logger logger = LoggerFactory.getLogger(Enzyme.class);

    private boolean isUnspecific;
    /**
     * Name of the enzyme.
     */
    private String name;
    /**
     * Description
     */
    private String description;
    /**
     * The amino acids before cleavage.
     */
    private HashSet<Character> aminoAcidBeforeSet = new HashSet<>();
    /**
     * The amino acids after cleavages.
     */
    private HashSet<Character> aminoAcidAfterSet = new HashSet<>();
    /**
     * The restriction amino acids before cleavage
     */
    private HashSet<Character> restrictionBeforeSet = new HashSet<>();
    /**
     * The restriction amino acids after cleavage
     */
    private HashSet<Character> restrictionAfterSet = new HashSet<>();

    private int residueCount = -1;
    /**
     * the probability that a peptide generated by this enzyme follows the cleavage rule
     * E.g. for trypsin, probability that a peptide ends with K or R
     */
    private float peptideCleavageEfficiency = 0;
    /**
     * the probability that a neighboring amino acid follows the enzyme rule
     * E.g. for trypsin, probability that the preceding amino acid is K or R
     */
    private float neighboringAACleavageEfficiency = 0;

    private String psiCvAccession;

    /**
     * Create a new enzyme
     *
     * @param name           enzyme name
     * @param isUnspecific   true if it is unspecific
     * @param description    description of the enzyme
     * @param psiCvAccession psi cv accession of the enzyme
     */
    public Enzyme(String name, boolean isUnspecific, String description, String psiCvAccession)
    {
        this.name = name;
        this.description = description;
        this.isUnspecific = isUnspecific;
        this.psiCvAccession = psiCvAccession;
    }

    /**
     * @return true if it is unspecific.
     */
    public boolean isUnspecific()
    {
        return isUnspecific;
    }

    /**
     * Adds an amino acid to the list of allowed amino acids after the cleavage
     * site.
     *
     * @param aminoAcid an amino acid represented by its single amino acid code.
     */
    public void addAminoAcidAfter(Character aminoAcid)
    {
        aminoAcidAfterSet.add(aminoAcid);
    }

    /**
     * Adds an amino acid to the list of allowed amino acids before the cleavage site.
     *
     * @param aminoAcid an amino acid represented by its single amino acid code.
     */
    public void addAminoAcidBefore(Character aminoAcid)
    {
        aminoAcidBeforeSet.add(aminoAcid);
    }

    /**
     * Adds an amino acid to the list of forbidden amino acids after the cleavage site.
     *
     * @param aminoAcid an amino acid represented by its single amino acid code.
     */
    public void addRestrictionAfter(Character aminoAcid)
    {
        restrictionAfterSet.add(aminoAcid);
    }

    /**
     * Adds an amino acid to the list of forbidden amino acids before the
     * cleavage site.
     *
     * @param aminoAcid an amino acid represented by its single amino acid code.
     */
    public void addRestrictionBefore(Character aminoAcid)
    {
        restrictionBeforeSet.add(aminoAcid);
    }

    /**
     * Sets the neighboring amino acid efficiency as the probability that a neighboring amino acid follows the enzyme rule
     *
     * @param neighboringAACleavageEfficiency neighboring amino acid effieicncy
     */
    private void setNeighboringAAEfficiency(float neighboringAACleavageEfficiency)
    {
        this.neighboringAACleavageEfficiency = neighboringAACleavageEfficiency;
    }

    /**
     * @return the number of residue allowed for cleavage.
     */
    public int getResidueCount()
    {
        return residueCount;
    }

    /**
     * @return neighboring amino acid efficiency
     */
    public float getNeighboringAACleavageEffiency()
    {
        return neighboringAACleavageEfficiency;
    }

    /**
     * Sets the peptide cleavage efficiency as the probability that a peptide generated by this enzyme follows the cleavage rule
     *
     * @param peptideCleavageEfficiency peptide cleavage efficiency
     */
    private void setPeptideCleavageEfficiency(float peptideCleavageEfficiency)
    {
        this.peptideCleavageEfficiency = peptideCleavageEfficiency;
    }

    /**
     * @return peptide cleavage efficiency of this enzyme
     */
    public float getPeptideCleavageEfficiency()
    {
        return peptideCleavageEfficiency;
    }

    /**
     * @return the name of the enzyme.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the description of the enzyme.
     */
    public String getDescription()
    {
        return description;
    }

    /**
     * @return the description of the enzyme when it is showed in the usage info.
     */
    public String getParamDescription()
    {
        return description;
    }

    /**
     * Checks if this enzyme cleaves N term.
     *
     * @return true, if it cleaves N term.
     */
    public boolean isNTerm()
    {
        return !aminoAcidAfterSet.isEmpty();
    }

    /**
     * Return true if the site before given amino acid is cleavable.
     *
     * @param aa test amino acid
     */
    public boolean isNTermCleavable(char aa)
    {
        if (isUnspecific)
            return true;
        return aminoAcidAfterSet.contains(aa);
    }

    /**
     * Checks if this enzyme cleaves C term.
     *
     * @return true, if it cleaves C term.
     */
    public boolean isCTerm()
    {
        return !aminoAcidBeforeSet.isEmpty();
    }

    /**
     * Return true if the site after given amino acid is cleavable.
     *
     * @param aa test amino acid
     */
    public boolean isCTermCleavable(char aa)
    {
        if (isUnspecific)
            return true;
        return aminoAcidBeforeSet.contains(aa);
    }

    /**
     * Checks if the amino acid is cleavable.
     *
     * @param aa the amino acid
     * @return true, if aa is cleavable
     */
    public boolean isCleavable(AminoAcid aa)
    {
        if (isUnspecific)
            return true;

        char unmodResidue = aa.getUnmodResidue();
        return aminoAcidBeforeSet.contains(unmodResidue) || aminoAcidAfterSet.contains(unmodResidue);
    }

    /**
     * Returns a boolean indicating whether the given amino acids represent a cleavage site.
     * Amino acid combinations are extended to find possible restrictions or cleavage sites.
     * Trypsin example: (D, E) returns false (R, D) returns true.
     *
     * @param aaBefore the amino acid before the cleavage site
     * @param aaAfter  the amino acid after the cleavage site
     * @return true if the amino acid combination can represent a cleavage site.
     */
    public boolean isCleavable(char aaBefore, char aaAfter)
    {
        if (isUnspecific)
            return true;

        return (aminoAcidBeforeSet.contains(aaBefore) && !restrictionAfterSet.contains(aaAfter)) ||
                (aminoAcidAfterSet.contains(aaAfter) && !restrictionBeforeSet.contains(aaBefore));
    }

    /**
     * Checks if the amino acid is cleavable.
     *
     * @param residue amino acid residue
     * @return true, if residue is cleavable
     */
    public boolean isCleavable(char residue)
    {
        if (isUnspecific)
            return true;
        return aminoAcidBeforeSet.contains(residue) || aminoAcidAfterSet.contains(residue);
    }

    /**
     * Checks if the peptide is cleaved by the enzyme.
     * Does not check for exception residues (meaning K.P or K.P is considered cleavable for trypsin)
     *
     * @param p peptide
     * @return true if p is cleaved, false otherwise.
     */
    public boolean isCleaved(Peptide p)
    {
        return isUnspecific || aminoAcidAfterSet.contains(p.get(0).getResidue())
                || aminoAcidBeforeSet.contains(p.get(p.size() - 1).getResidue());
    }

    /**
     * Returns PSI CV accession.
     *
     * @return HUPO PSI CV accession of this enzyme. null if unknown.
     */
    public String getPSICvAccession()
    {
        return this.psiCvAccession;
    }

    /**
     * Returns the number of cleavaged terminii
     *
     * @param annotation annotation (e.g. K.DLFGEK.I)
     * @return the number of cleavaged terminii
     */
    public int getNumCleavedTermini(String annotation, AminoAcidSet aaSet)
    {
        int nCT = 0;

        AminoAcid preAA = aaSet.getAminoAcid(annotation.charAt(0));
        AminoAcid startAA = aaSet.getAminoAcid(annotation.charAt(2));
        if (preAA == null || isCleavable(preAA.getUnmodResidue(), startAA.getUnmodResidue()))
            nCT++;

        AminoAcid nextAA = aaSet.getAminoAcid(annotation.charAt(annotation.length() - 1));
        AminoAcid endAA = aaSet.getAminoAcid(annotation.charAt(annotation.length() - 3));
        if (nextAA != null && isCleavable(endAA.getUnmodResidue(), nextAA.getUnmodResidue()))
            nCT++;

        return nCT;
    }

    @Override
    public int hashCode()
    {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Enzyme enzyme = (Enzyme) o;
        return Objects.equals(name, enzyme.name);
    }


    public HashSet<Character> getAminoAcidBeforeSet()
    {
        return aminoAcidBeforeSet;
    }

    public HashSet<Character> getAminoAcidAfterSet()
    {
        return aminoAcidAfterSet;
    }

    public HashSet<Character> getRestrictionBeforeSet()
    {
        return restrictionBeforeSet;
    }

    public HashSet<Character> getRestrictionAfterSet()
    {
        return restrictionAfterSet;
    }

    /**
     * Unspecific cleavage enzyme (can cleavage after any residue)
     */
    public static final Enzyme UnspecificCleavage;

    /**
     * TRYPSIN enzyme (cleave after K or R)
     */
    public static final Enzyme TRYPSIN;

    /**
     * CHYMOTRYPSIN enzyme (cleave after FYWL)
     */
    public static final Enzyme CHYMOTRYPSIN;

    /**
     * LysC enzyme (cleave after K)
     */
    public static final Enzyme LysC;

    /**
     * LysN enzyme (cleave before K)
     */
    public static final Enzyme LysN;

    /**
     * GluC enzyme (cleave after E)
     */
    public static final Enzyme GluC;

    /**
     * ArgC enzyme (cleave after R)
     */
    public static final Enzyme ArgC;

    /**
     * AspN enzyme (cleave before D)
     */
    public static final Enzyme AspN;

    /**
     * ALP enzyme，碱性磷酸酶，切除磷酸
     */
    public static final Enzyme ALP;

    /**
     * Endogenous peptides (do not cleave after any residue, i.e. no internal cleavage)
     */
    public static final Enzyme NoCleavage;

    /**
     * Get an Enzyme by enzyme name
     */
    public static Enzyme getEnzymeByName(String name)
    {
        return enzymeTable.get(name);
    }

    /**
     * Get all registered enzymes
     */
    public static Enzyme[] getAllRegisteredEnzymes()
    {
        return registeredEnzymeList.toArray(new Enzyme[0]);
    }

    private static HashMap<String, Enzyme> enzymeTable;
    private static ArrayList<Enzyme> registeredEnzymeList;

    private static void register(String name, Enzyme enzyme)
    {
        if (enzymeTable.put(name, enzyme) == null) {
            registeredEnzymeList.add(enzyme);
            enzyme.residueCount = enzyme.aminoAcidBeforeSet.size() + enzyme.aminoAcidAfterSet.size();
        }
    }

    static {
        UnspecificCleavage = new Enzyme("UnspecificCleavage", true, "unspecific cleavage", "MS:1001956");
        TRYPSIN = new Enzyme("Tryp", false, "Trypsin", "MS:1001251");
        TRYPSIN.addAminoAcidBefore('K');
        TRYPSIN.addAminoAcidBefore('R');
        TRYPSIN.setNeighboringAAEfficiency(0.99999f);  // Modified by Sangtae to boost the performance
        TRYPSIN.setPeptideCleavageEfficiency(0.99999f);

        CHYMOTRYPSIN = new Enzyme("Chymotrypsin", false, "Chymotrypsin", "MS:1001306");
        CHYMOTRYPSIN.addAminoAcidBefore('F');
        CHYMOTRYPSIN.addAminoAcidBefore('Y');
        CHYMOTRYPSIN.addAminoAcidBefore('W');
        CHYMOTRYPSIN.addAminoAcidBefore('L');

        LysC = new Enzyme("LysC", false, "Lys-C", "MS:1001309");
        LysC.addAminoAcidBefore('K');
        LysC.setNeighboringAAEfficiency(0.999f);
        LysC.setPeptideCleavageEfficiency(0.999f);

        LysN = new Enzyme("LysN", false, "Lys-N", null);
        LysN.addAminoAcidAfter('K');
        LysN.setNeighboringAAEfficiency(0.79f);
        LysN.setPeptideCleavageEfficiency(0.89f);

        GluC = new Enzyme("GluC", false, "glutamyl endopeptidase", "MS:1001917");
        GluC.addAminoAcidBefore('E');

        ArgC = new Enzyme("ArgC", false, "Arg-C", "MS:1001303");
        ArgC.addAminoAcidBefore('R');

        AspN = new Enzyme("AspN", false, "Asp-N", "MS:1001304");
        AspN.addAminoAcidAfter('D');

        ALP = new Enzyme("aLP", true, "alphaLP", null);

        // NoCleavage aka no internal cleavage
        // Do not allow cleavage after any residue
        NoCleavage = new Enzyme("NoCleavage", false, "no cleavage", "MS:1001955");

        enzymeTable = new HashMap<>();
        registeredEnzymeList = new ArrayList<>();

        register(UnspecificCleavage.name, UnspecificCleavage);
        register(TRYPSIN.name, TRYPSIN);              // 1
        register(CHYMOTRYPSIN.name, CHYMOTRYPSIN);    // 2
        register(LysC.name, LysC);                    // 3
        register(LysN.name, LysN);                    // 4
        register(GluC.name, GluC);                    // 5
        register(ArgC.name, ArgC);                    // 6
        register(AspN.name, AspN);                    // 7
        register(ALP.name, ALP);                      // 8
        register(NoCleavage.name, NoCleavage);        // 9

//        // Add user-defined enzymes
//        File enzymeFile = new File("params/enzymes.txt");
//        if (enzymeFile.exists()) {
//            ArrayList<String> paramStrs = UserParam.parseFromFile(enzymeFile.getPath(), 4);
//            for (String paramStr : paramStrs) {
//                String[] token = paramStr.split(",");
//                String shortName = token[0];
//                String cleaveAt = token[1];
//                if (cleaveAt.equalsIgnoreCase("null"))
//                    cleaveAt = null;
//                else {
//                    for (int i = 0; i < cleaveAt.length(); i++) {
//                        if (!AminoAcid.isStdAminoAcid(cleaveAt.charAt(i))) {
//                            logger.error("Invalid user-defined enzyme at " + enzymeFile.getAbsolutePath() + ": " + paramStr);
//                            logger.error("Unrecognizable aa residue: " + cleaveAt.charAt(i));
//                            throw new OmicsRuntimeException("Unrecognizable aa residue: " + cleaveAt.charAt(i));
//                        }
//                    }
//                }
//                boolean isNTerm;    // C-Term: false, N-term: true
//                if (token[2].equals("C"))
//                    isNTerm = false;
//                else if (token[2].equals("N"))
//                    isNTerm = true;
//                else {
//                    logger.error("Invalid user-defined enzyme at " + enzymeFile.getAbsolutePath() + ": " + paramStr);
//                    logger.error(token[2] + " must be 'C' or 'N'.");
//                    throw new OmicsRuntimeException("Wrong enzyme format");
//                }
//                String description = token[3];
//
//                Enzyme userEnzyme = new Enzyme(shortName, cleaveAt, isNTerm, description, null);
//                for (char aa : cleaveAt.toCharArray()) {
//
//                }
//                register(shortName, userEnzyme);
//            }
//        }
    }
}
