package edu.ucsd.msjava.msutil;

import java.util.Hashtable;


/**
 * Amino acid.
 *
 * @author Sangtae Kim
 */
public class AminoAcid extends Matter
{
    // this is recommended for Serializable objects
    static final private long serialVersionUID = 1L;

    private double mass;
    private int nominalMass;
    private char residue;    // 1 letter code for standard amino acid
    private String name;
    private float probability = 0.05f;
    private Composition composition;

    /**
     * Constructor.
     *
     * @param residue     single letter identifier.
     * @param name        full name of the amino acid.
     * @param composition CHNOS composition object.
     */
    protected AminoAcid(char residue, String name, Composition composition)
    {
        this.mass = composition.getAccurateMass();
        this.nominalMass = composition.getNominalMass();
        this.residue = residue;
        this.name = name;
        this.composition = composition;
    }

    /**
     * Constructor. Generates a custom amino acid.
     *
     * @param residue single letter identifier
     * @param mass    amino acid amss
     */
    protected AminoAcid(char residue, String name, double mass)
    {
        this.mass = mass;
        this.nominalMass = Math.round(Constants.INTEGER_MASS_SCALER * (float) mass);
        this.residue = residue;
        this.name = name;
    }

    /**
     * Set probability of this amino acid, which is the ratio of the count of this amino acid and all the amino acid
     * in the database.
     *
     * @return this object.
     */
    public AminoAcid setProbability(float probability)
    {
        this.probability = probability;
        return this;
    }


    public String toString()
    {
        return residue + ": " + String.format("%.2f", mass);
    }

    /**
     * @return false if this is not modified.
     */
    public boolean isModified()
    {
        return false;
    }

    /**
     * @return the number of variable modifications applied to this amino acid.
     */
    public int getNumVariableMods()
    {
        return 0;
    }

    /**
     * @return false if this is not associated with terminal-specific modification
     */
    public boolean hasTerminalVariableMod()
    {
        return false;
    }

    /**
     * @return false if this is not associated with residue-specific modification
     */
    public boolean hasResidueSpecificVariableMod()
    {
        return false;
    }

    /**
     * Gets the mass of this amino acid. This is the mono isotopic mass.
     */
    @Override
    public float getMass()
    {
        return (float) mass;
    }

    /**
     * @return the mass of this amino acid (double precision). This is the mono isotopic mass.
     */
    @Override
    public double getAccurateMass()
    {
        return mass;
    }

    /**
     * @return nominal mass of this object.
     */
    @Override
    public int getNominalMass()
    {
        return nominalMass;
    }

    /**
     * @return the probability of this amino acid. Currently set as 1/20, uniformly.
     */
    public float getProbability()
    {
        return probability;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (!(obj instanceof AminoAcid))
            return false;
        AminoAcid aa = (AminoAcid) obj;
        return this == aa;
    }

    /**
     * Gets the representation of the residue as string.
     *
     * @return the string representing this amino acid.
     */
    public String getResidueStr()
    {
        return String.valueOf(residue);
    }

    /**
     * @return the single letter amino acid character.
     */
    public char getResidue()
    {
        return residue;
    }

    /**
     * @return the single letter amino acid character representation of the unmodified version of this amino acid.
     */
    public char getUnmodResidue()
    {
        return residue;
    }

    /**
     * @return the full name/description of the amino acid.
     */
    public String getName()
    {
        return name;
    }

    /**
     * @return the composition object for this amino acid.
     */
    public Composition getComposition()
    {
        return composition;
    }

    /**
     * return the standard amino acid of given letter
     */
    public static AminoAcid getStandardAminoAcid(char residue)
    {
        return residueMap.get(residue);
    }

    /**
     * @return array of the 20 standard amino acids.
     */
    public static AminoAcid[] getStandardAminoAcids()
    {
        return standardAATable;
    }

    /**
     * Returns a modified version of this amino acid (fixed modification).
     *
     * @param mod a modification.
     * @return a modified amino acid object.
     */
    public AminoAcid getAAWithFixedModification(Modification mod)
    {
        String name = mod.getName() + " " + this.getName();
        AminoAcid modAA;
        if (mod.getComposition() == null)
            modAA = getCustomAminoAcid(residue, name, mass + mod.getAccurateMass());
        else
            modAA = getAminoAcid(residue, name, composition.getAddition(mod.getComposition()));
        return modAA;
    }

    /**
     * Create a custom amino acid.
     *
     * @param residue amino acid residue
     * @param name    amino acid name
     * @param mass    amino acid name
     */
    public static AminoAcid getCustomAminoAcid(char residue, String name, double mass)
    {
        AminoAcid standardAA = AminoAcid.getStandardAminoAcid(residue);
        if (standardAA != null && Math.abs(mass - standardAA.getMass()) < 0.001f)
            return standardAA;
        else
            return new AminoAcid(residue, name, mass);
    }

    /**
     * Create a custom amino acid with given information
     *
     * @param residue amino acid residue
     * @param mass    amino acid mass
     */
    public static AminoAcid getCustomAminoAcid(char residue, float mass)
    {
        return new AminoAcid(residue, "Custom amino acid", mass);
    }

    /**
     * return a {@link AminoAcid} of given information
     *
     * @param residue     single letter identifier
     * @param name        amino acid name
     * @param composition amino acid Composition
     */
    public static AminoAcid getAminoAcid(char residue, String name, Composition composition)
    {
        AminoAcid standardAA = AminoAcid.getStandardAminoAcid(residue);
        if (standardAA != null && composition.getAccurateMass() == standardAA.getAccurateMass())
            return standardAA;
        else
            return new AminoAcid(residue, name, composition);
    }

    @Override
    public int hashCode()
    {
        return (int) residue;
    }


    private static Hashtable<Character, AminoAcid> residueMap;
    /**
     * Predefined standard Amino Acids
     */
    private static final AminoAcid[] standardAATable =
            {
                    new AminoAcid('G', "Glycine", new Composition(2, 3, 1, 1, 0)),
                    new AminoAcid('A', "Alanine", new Composition(3, 5, 1, 1, 0)),
                    new AminoAcid('S', "Serine", new Composition(3, 5, 1, 2, 0)),
                    new AminoAcid('P', "Proline", new Composition(5, 7, 1, 1, 0)),
                    new AminoAcid('V', "Valine", new Composition(5, 9, 1, 1, 0)),
                    new AminoAcid('T', "Threonine", new Composition(4, 7, 1, 2, 0)),
                    new AminoAcid('C', "Cystine", new Composition(3, 5, 1, 1, 1)),
                    new AminoAcid('L', "Leucine", new Composition(6, 11, 1, 1, 0)),
                    new AminoAcid('I', "Isoleucine", new Composition(6, 11, 1, 1, 0)),
                    new AminoAcid('N', "Asparagine", new Composition(4, 6, 2, 2, 0)),
                    new AminoAcid('D', "Aspartate", new Composition(4, 5, 1, 3, 0)),
                    new AminoAcid('Q', "Glutamine", new Composition(5, 8, 2, 2, 0)),
                    new AminoAcid('K', "Lysine", new Composition(6, 12, 2, 1, 0)),
                    new AminoAcid('E', "Glutamate", new Composition(5, 7, 1, 3, 0)),
                    new AminoAcid('M', "Methionine", new Composition(5, 9, 1, 1, 1)),
                    new AminoAcid('H', "Histidine", new Composition(6, 7, 3, 1, 0)),
                    new AminoAcid('F', "Phenylalanine", new Composition(9, 9, 1, 1, 0)),
                    new AminoAcid('R', "Arginine", new Composition(6, 12, 4, 1, 0)),
                    new AminoAcid('Y', "Tyrosine", new Composition(9, 9, 1, 2, 0)),
                    new AminoAcid('W', "Tryptophan", new Composition(11, 10, 2, 1, 0)),
            };

    static {
        residueMap = new Hashtable<>();
        for (AminoAcid aa : standardAATable)
            residueMap.put(aa.getResidue(), aa);
    }

    /**
     * Checks whether the character is an standard amino acid
     *
     * @param c the character input
     * @return true if it is part of the standard amino acid set, false otherwise
     */
    public static boolean isStdAminoAcid(char c)
    {
        return residueMap.containsKey(c);
    }
}

