package edu.ucsd.msjava.msutil;

import omics.msdk.filter.MsLevelFilter;
import omics.msdk.io.*;
import omics.util.ms.peaklist.PeakProcessorChain;
import omics.util.ms.spectra.MsnSpectrum;
import omics.util.utils.DelegateFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Accessor of spectra.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 25 Jan 2019, 2:00 PM
 */
public class SpectraAccessor implements SpectrumAccessorBySpecIndex, Iterable<Spectrum>, Iterator<Spectrum>
{
    private static final Logger logger = LoggerFactory.getLogger(SpectraAccessor.class);

    protected final File specFile;
    protected final SpecFileFormat specFormat;

    private Map<Integer, Spectrum> spectrumIndexMap = new HashMap<>();
    private Map<String, Spectrum> spectrumTitleMap = new HashMap<>();

    private Spectrum currentSpec;
    private Iterator<Integer> indexIt;

    private PeakProcessorChain peakProcessorChain;
    private DelegateFilter<MsnSpectrum> filter;

    public SpectraAccessor(File specFile)
    {
        this(specFile, SpecFileFormat.getSpecFileFormat(specFile.getName()));
    }

    public SpectraAccessor(File specFile, SpecFileFormat specFormat)
    {
        this.specFile = specFile;
        this.specFormat = specFormat;
        this.peakProcessorChain = new PeakProcessorChain();
        this.filter = new DelegateFilter<>();
        this.filter.addFilter(new MsLevelFilter(2, Integer.MAX_VALUE));
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public SpectraAccessor(File specFile, SpecFileFormat specFormat, PeakProcessorChain peakProcessorChain, DelegateFilter<MsnSpectrum> filter)
    {
        this.specFile = specFile;
        this.specFormat = specFormat;
        this.peakProcessorChain = peakProcessorChain;
        this.filter = filter;
        try {
            init();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void init() throws IOException
    {
        MSReader reader;
        if (specFormat.equals(SpecFileFormat.MGF)) {
            reader = new MgfReader(specFile);
        } else if (specFormat.equals(SpecFileFormat.MZXML)) {
            reader = new MzXMLReader(specFile);
        } else if (specFormat.equals(SpecFileFormat.MZML)) {
            reader = new MzMLReader(specFile);
        } else if (specFormat.equals(SpecFileFormat.PKL)) {
            reader = new PklReader(specFile);
        } else {
            throw new IOException("Unsupported file format: " + specFormat);
        }

        int totalCount = 0;
        int retainCount = 0;
        int index = 0;
        while (reader.hasNext()) {
            omics.util.ms.spectra.Spectrum spectrum = reader.next();
            if (!spectrum.isMsnSpectrum())
                continue;

            totalCount++;
            index++;
            MsnSpectrum msnSpectrum = spectrum.asMsnSpectrum();

            if (!filter.test(msnSpectrum))
                continue;

            msnSpectrum.apply(peakProcessorChain);
            retainCount++;

            Spectrum peaks = ScanConverter.convert(msnSpectrum);
            peaks.setSpecIndex(index);
            spectrumIndexMap.put(index, peaks);
            spectrumTitleMap.put(peaks.getTitle(), peaks);
        }
        reader.close();
        indexIt = getSpecIndexList().iterator();
        logger.info("Spectra : " + totalCount);
        logger.info("Spectra after filter: " + retainCount);
    }

    public File getSpecFile()
    {
        return specFile;
    }

    public SpectrumAccessorBySpecIndex getSpecMap()
    {
        return this;
    }

    /**
     * @return number of spectra
     */
    public int size()
    {
        return spectrumIndexMap.size();
    }

    public Iterator<Spectrum> getSpecItr()
    {
        return this;
    }

    public Spectrum getSpectrumBySpecIndex(int specIndex)
    {
        return spectrumIndexMap.get(specIndex);
    }

    public Spectrum getSpectrumById(String specId)
    {
        return spectrumTitleMap.get(specId);
    }

    @Override
    public ArrayList<Integer> getSpecIndexList()
    {
        ArrayList<Integer> indexList = new ArrayList<>(spectrumIndexMap.keySet());
        indexList.sort(Comparator.naturalOrder());
        return indexList;
    }

    @Override
    public Iterator<Spectrum> iterator()
    {
        return this;
    }

    @Override
    public boolean hasNext()
    {
        currentSpec = getNext();
        return currentSpec != null;
    }

    @Override
    public Spectrum next()
    {
        return currentSpec;
    }

    private Spectrum getNext()
    {
        while (indexIt.hasNext()) {
            Integer index = indexIt.next();
            Spectrum spectrum = spectrumIndexMap.get(index);
            if (spectrum != null) {
                return spectrum;
            }
        }
        return null;
    }
}
