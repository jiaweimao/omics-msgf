package edu.ucsd.msjava.msutil;

import java.util.ArrayList;

public interface SpectrumAccessorBySpecIndex
{
    /**
     * Return the spectrum at given index
     *
     * @param specIndex index in the file
     * @return {@link Spectrum} at the index
     */
    Spectrum getSpectrumBySpecIndex(int specIndex);

    /**
     * Return spectrum of given identifier
     *
     * @param specId spectrum identifier
     */
    Spectrum getSpectrumById(String specId);

    /**
     * @return indexes of all spectra
     */
    ArrayList<Integer> getSpecIndexList();
}
