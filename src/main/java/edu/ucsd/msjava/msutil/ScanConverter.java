/*
 * Copyright 2018 JiaweiMao jiaweiM_philo@hotmail.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package edu.ucsd.msjava.msutil;

import omics.util.ms.spectra.Dissociation;
import omics.util.ms.spectra.MsnSpectrum;
import omics.util.ms.spectra.RetentionTimeList;
import omics.util.ms.spectra.SpectrumType;

/**
 * @author JiaweiMao
 * @version 1.0.1
 * @since 13 Oct 2018, 4:15 PM
 */
public class ScanConverter
{
    /**
     * convert {@link MsnSpectrum} to {@link Spectrum}
     */
    public static Spectrum convert(MsnSpectrum spectrum)
    {
        Spectrum spec = new Spectrum();

        spec.setID(spec.getTitle());
        spec.setTitle(spectrum.getSpectrumTitle());
        spec.setScanNum(spectrum.getScanNumber().getValue());
        spec.setSpecIndex(spectrum.getIndex());
        RetentionTimeList retentionTimes = spectrum.getRetentionTimes();
        if (!retentionTimes.isEmpty()) {
            spec.setRt((float) retentionTimes.getFirst().getTime());
            spec.setRtIsSeconds(true);
        }

        omics.util.ms.peaklist.Peak pp = spectrum.getPrecursor();
        spec.setPrecursor(new Peak((float) pp.getMz(), (float) pp.getIntensity(), pp.getCharge()));
        spec.setMsLevel(spectrum.getMsLevel());
        Double isolateTargetMz = spectrum.getIsolateWindowTargetMz();
        if (isolateTargetMz != null) {
            spec.setIsolationWindowTargetMz(isolateTargetMz.floatValue());
        }

        SpectrumType spectrumType = spectrum.getSpectrumType();
        spec.setIsCentroided(spectrumType == SpectrumType.CENTROIDED);

        Dissociation dissociation = spectrum.getDissociation();
        spec.setActivationMethod(ActivationMethod.get(dissociation.toString()));

        for (int i = 0; i < spectrum.size(); i++) {
            spec.add(new Peak((float) spectrum.getMz(i), (float) spectrum.getIntensity(i), 1));
        }

        spec.determineIsCentroided();
        return spec;
    }
}
