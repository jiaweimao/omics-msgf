package edu.ucsd.msjava.msutil;

import edu.ucsd.msjava.misc.ExceptionCapturer;
import edu.ucsd.msjava.misc.ProgressData;
import edu.ucsd.msjava.misc.ProgressReporter;
import edu.ucsd.msjava.misc.ThreadPoolExecutorWithExceptions;
import edu.ucsd.msjava.msdbsearch.ReverseDB;
import edu.ucsd.msjava.mzid.MzIDParser;
import omics.pdk.IdentResult;
import omics.pdk.io.xml.MzIdentMLAccess;
import omics.util.OmicsException;
import omics.util.io.csv.CsvReader;

import javax.xml.stream.XMLStreamException;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * This class is used to generate annotated spectra of given identification results and MS files.
 *
 * @version 1.0.0
 * @since 21 Jan 2019, 2:46 PM
 */
public class AnnotatedSpectra implements ProgressReporter, ExceptionCapturer
{
    /**
     * identification result files
     */
    private File[] resultFiles;
    /**
     * spectrum file directory
     */
    private File specDir;
    private AminoAcidSet aaSet;
    private float fdrThreshold = 0.01f;
    private ProgressData progress;
    private boolean dropErrorDatasets = false;
    private Throwable exception = null;

    private SpectraContainer annotatedSpectra;

    @Override
    public void setProgressData(ProgressData data)
    {
        progress = data;
    }

    @Override
    public ProgressData getProgressData()
    {
        return progress;
    }

    @Override
    public boolean hasException()
    {
        return exception != null;
    }

    @Override
    public Throwable getException()
    {
        return exception;
    }

    public AnnotatedSpectra(File[] resultFiles, File specDir, AminoAcidSet aaSet)
    {
        this.resultFiles = resultFiles;
        this.specDir = specDir;
        this.aaSet = aaSet;
        this.progress = null;
    }

    public AnnotatedSpectra(File[] resultFiles, File specDir, AminoAcidSet aaSet, float fdrThreshold, boolean dropErrors)
    {
        this.resultFiles = resultFiles;
        this.specDir = specDir;
        this.aaSet = aaSet;
        this.fdrThreshold = fdrThreshold;
        this.dropErrorDatasets = dropErrors;
        this.progress = null;
    }

    public static class ConcurrentAnnotatedSpectraParser extends AnnotatedSpectra implements Runnable
    {
        private List<Spectrum> results;
        private List<String> errors;

        public ConcurrentAnnotatedSpectraParser(File[] resultFiles, File specDir, AminoAcidSet aaSet, float fdrThreshold, boolean dropErrors, List<Spectrum> resultList, List<String> errorList)
        {
            super(resultFiles, specDir, aaSet, fdrThreshold, dropErrors);
            results = resultList;
            errors = errorList;
        }

        @Override
        public void run()
        {
            String result = parse();
            results.addAll(getAnnotatedSpecContainer());
            if (result != null) {
                errors.add(result);
            }
        }
    }

    public AnnotatedSpectra fdrThreshold(float fdrThreshold)
    {
        this.fdrThreshold = fdrThreshold;
        return this;
    }

    public SpectraContainer getAnnotatedSpecContainer()
    {
        return annotatedSpectra;
    }

    public String parse(int numThreads, boolean dropErrors)
    {
        if (numThreads <= 1)
            return parse();

        List<Spectrum> results = Collections.synchronizedList(new ArrayList<>());
        List<String> errors = Collections.synchronizedList(new ArrayList<>());

        // Thread pool
        ThreadPoolExecutorWithExceptions executor = ThreadPoolExecutorWithExceptions.newFixedThreadPool(numThreads);
        executor.setTaskName("Parse");

        try {
            List<List<File>> taskFiles = new ArrayList<>();
            for (int i = 0; i < numThreads; i++) {
                taskFiles.add(new ArrayList<>());
            }
            for (int i = 0; i < resultFiles.length; i++) {
                // Evenly distribute the files to the threads; doing it in a collated fashion because files
                // of similar size will often have similar names, and we don't want to give one thread all
                // of the largest files.
                taskFiles.get(i % numThreads).add(resultFiles[i]);
            }
            for (int i = 0; i < numThreads; i++) {
                List<File> thisTaskFiles = taskFiles.get(i);
                System.out.println("Task " + (i + 1) + ": " + thisTaskFiles.size() + " files.");
                ConcurrentAnnotatedSpectraParser parser = new ConcurrentAnnotatedSpectraParser(thisTaskFiles.toArray(new File[0]), specDir, aaSet, fdrThreshold, dropErrors, results, errors);
                executor.execute(parser);
            }
            taskFiles.clear();

            // Output initial progress report.
            executor.outputProgressReport();
            executor.shutdown();
            try {
                executor.awaitTerminationWithExceptions(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
            } catch (InterruptedException e) {
                if (executor.HasThrownData()) {
                    e.printStackTrace();
                }
            }

            // Output completed progress report.
            executor.outputProgressReport();
        } catch (OutOfMemoryError ex) {
            ex.printStackTrace();
            executor.shutdownNow();
            return "Task terminated; results incomplete. Please run again with a greater amount of memory, using \"-Xmx4G\", for example.";
        } catch (Throwable ex) {
            ex.printStackTrace();
            executor.shutdownNow();
            return "Task terminated; results incomplete. Please run again.";
        }
        annotatedSpectra = new SpectraContainer();
        annotatedSpectra.addAll(results);
        String errorList = null;
        for (String error : errors) {
            if (errorList == null) {
                errorList = error;
            } else {
                errorList += "\n" + error;
            }
        }
        return errorList;
    }

    public String parse()
    {
        if (progress == null)
            progress = new ProgressData();

        annotatedSpectra = new SpectraContainer();

        System.out.println("Using " + resultFiles.length + " result files:");
        for (File resultFile : resultFiles)
            System.out.println("\t" + resultFile.getName());

        int count = 0;
        int total = resultFiles.length;
        String aggErrs = null;
        for (File resultFile : resultFiles) {
            String errMsg = null;
            try {
                errMsg = parseFile(resultFile);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XMLStreamException e) {
                e.printStackTrace();
            } catch (OmicsException e) {
                e.printStackTrace();
            }
            count++;
            progress.report(count, total);
            if (errMsg != null) {
                String msg = "Error while parsing " + resultFile.getName() + ": " + errMsg;
                if (dropErrorDatasets) {
                    System.out.println(msg);
                    if (aggErrs == null) {
                        aggErrs = msg;
                    } else {
                        aggErrs += "\n" + msg;
                    }
                } else {
                    exception = new Exception(msg);
                    return msg;
                }
            }
        }
        return null;
    }

    public void writeToMgf(PrintStream out)
    {
        if (annotatedSpectra != null) {
            for (Spectrum spec : annotatedSpectra)
                spec.outputMgf(out);
        }
    }

    /**
     * parse given result file. if the result file is mzid format, convert it to tsv first.
     *
     * @param resultFile identification result file
     * @return error message
     */
    public String parseFile(File resultFile) throws IOException, OmicsException, XMLStreamException
    {
        System.out.println("Parsing " + resultFile.getName());
        File tsvResultFile = null;
        if (resultFile.getName().endsWith(".mzid")) {
            String resultFileName = resultFile.getAbsolutePath();
            String tsvResultFileName = resultFileName.substring(0, resultFileName.lastIndexOf('.')) + ".tsv";
            tsvResultFile = new File(tsvResultFileName);

            MzIdentMLAccess access = new MzIdentMLAccess(resultFile, ReverseDB.DECOY_PROTEIN_PREFIX);
            access.start();
            IdentResult result = access.getValue();

            if (!tsvResultFile.exists()) {
                if (!tsvResultFile.canWrite()) {
                    MzIDParser parser = new MzIDParser(result);
                    parser.writeToTSVFile(tsvResultFile);
                } else {
                    try {
                        System.out.println("Converting " + resultFile.getName());
                        tsvResultFile = File.createTempFile("__AnnotatedSpectra", ".tsv");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    tsvResultFile.deleteOnExit();

                    MzIDParser parser = new MzIDParser(result);
                    parser.writeToTSVFile(tsvResultFile);
                }
            } else {
                System.out.println(tsvResultFileName + " already exists.");
            }
        } else if (resultFile.getName().endsWith(".tsv")) {
            tsvResultFile = resultFile;
        }


        CsvReader reader = new CsvReader(tsvResultFile.getPath(), '\t');
        reader.readHeaders();
        if (!reader.getHeader(0).startsWith("#"))
            return "Not a valid tsv result file";

        int specIdCol = reader.getIndex("SpecID");
        int specFileCol = reader.getIndex("#SpecFile");
        int pepCol = reader.getIndex("Peptide");
        int fdrCol = reader.getIndex("QValue");
        int chargeCol = reader.getIndex("Charge");
        if (specIdCol < 0 || specFileCol < 0 || pepCol < 0)
            return "Not a valid mzid file";
        if (fdrCol < 0)
            return "QValue is missing";

        List<Spectrum> annotatedResults = new ArrayList<>();
        HashMap<String, SpectraAccessor> specAccessorMap = new HashMap<>(); // file name -> SpectraAccessor
        while (reader.readRecord()) {
            float fdr = Float.parseFloat(reader.get(fdrCol));
            if (fdr > fdrThreshold)
                continue;

            String pep = reader.get(pepCol);
            if (pep.matches(".\\..+\\.."))
                pep = pep.substring(pep.indexOf('.') + 1, pep.lastIndexOf('.'));

            String specFileName = reader.get(specFileCol);
            specFileName = new File(specFileName).getName();
            int charge = Integer.parseInt(reader.get(chargeCol));

            SpectraAccessor specAccessor = specAccessorMap.get(specFileName);
            if (specAccessor == null) {
                File specFile = new File(specDir.getPath() + File.separator + specFileName);
                specAccessor = new SpectraAccessor(specFile);
                specAccessorMap.put(specFileName, specAccessor);
            }

            String specId = reader.get(specIdCol);
            Spectrum spec = specAccessor.getSpectrumById(specId);

            if (spec == null)
                return specFileName + ":" + specId + " is not available!";
            else {
                Peptide peptide = new Peptide(pep, aaSet);
                spec.setCharge(charge);

                if (Math.abs(spec.getPeptideMass() - peptide.getMass()) < 5) {
                    spec.setAnnotation(peptide);
                    annotatedResults.add(spec);
                } else {
                    return "parent mass doesn't match " + specFileName + ":" + specId + " " + peptide.toString() + " " + spec.getPeptideMass() + " != " + peptide.getMass();
                }
            }

        }

        reader.close();
        annotatedSpectra.addAll(annotatedResults);
        return null;
    }
}
