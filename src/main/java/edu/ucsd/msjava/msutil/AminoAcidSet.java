package edu.ucsd.msjava.msutil;

import edu.ucsd.msjava.msutil.Modification.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * A factory class to instantiate a set of amino acids
 *
 * @author sangtaekim
 */
public class AminoAcidSet implements Iterable<AminoAcid>
{
    private static final Logger logger = LoggerFactory.getLogger(AminoAcidSet.class);

    private static final AminoAcid[] EMPTY_AA_ARRAY = new AminoAcid[0];

    /**
     * this map hold all {@link AminoAcid} of this set, including fixed and variable modified amino acids.
     */
    private HashMap<Location, ArrayList<AminoAcid>> aaListMap;

    /**
     * location map, map from parent location its children locations.
     */
    private static HashMap<Location, Location[]> locMap;

    static {
        locMap = new HashMap<>();
        locMap.put(Location.Anywhere, new Location[]{Location.Anywhere, Location.N_Term, Location.C_Term, Location.Protein_N_Term, Location.Protein_C_Term});
        locMap.put(Location.N_Term, new Location[]{Location.N_Term, Location.Protein_N_Term});
        locMap.put(Location.C_Term, new Location[]{Location.C_Term, Location.Protein_C_Term});
        locMap.put(Location.Protein_N_Term, new Location[]{Location.Protein_N_Term});
        locMap.put(Location.Protein_C_Term, new Location[]{Location.Protein_C_Term});
    }

    /**
     * for fast indexing
     * residue -> aa (residue must be unique)
     */
    private HashMap<Character, AminoAcid> residueMap;
    /**
     * aa -> index (mass in ascending order)
     */
    private HashMap<AminoAcid, Integer> aa2index;
    /**
     * std residue -> array of amino acids
     */
    private HashMap<Location, HashMap<Character, AminoAcid[]>> standardResidueAAArrayMap;
    /**
     * nominalMass -> array of amino acids
     */
    private HashMap<Location, HashMap<Integer, AminoAcid[]>> nominalMass2aa;
    /**
     * array of all amino acids, sort in mass ascending order
     */
    private AminoAcid[] allAminoAcidArr;
    private int maxNumberOfVariableModificationsPerPeptide = 2;

    /**
     * true if this contains any variable or terminal (fixed or variable) modification
     */
    private boolean containsModification;
    /**
     * true if this contains any (fixed or variable) modification specific to N-terminus
     */
    private boolean containsNTermModification;
    /**
     * true if this contains any (fixed or variable) modification specific to C-terminus
     */
    private boolean containsCTermModification;
    private boolean containsPhosphorylation;    //
    /**
     * true if this contains iTRAQ
     */
    private boolean containsITRAQ;
    /**
     * true if this contains iTRAQ
     */
    private boolean containsTMT;

    /**
     * set of symbols used for residues, helper for assign symbols for new modified amino acid.
     */
    private HashSet<Character> modResidueSet = new HashSet<>();
    private char nextResidue;

    // for enzyme
    private int neighboringAACleavageCredit = 0;
    private int neighboringAACleavagePenalty = 0;
    private int peptideCleavageCredit = 0;
    private int peptideCleavagePenalty = 0;
    private float probCleavageSites = 0;

    /**
     * the amino acids with the heaviest and lighest mass
     */
    private AminoAcid lightestAA, heaviestAA;

    private AminoAcidSet() // prevents instantiation
    {
        aaListMap = new HashMap<>();
        standardResidueAAArrayMap = new HashMap<>();
        for (Location location : Location.values()) {
            aaListMap.put(location, new ArrayList<>());
        }
        nextResidue = 128;
    }

    /**
     * Returns the list of amino acids specific to the position.
     *
     * @return list of intermediate amino acids.
     */
    public ArrayList<AminoAcid> getAAList(Location location)
    {
        return aaListMap.get(location);
    }

    /**
     * Returns the iterator of anywhere amino acids
     */
    public Iterator<AminoAcid> iterator()
    {
        return aaListMap.get(Location.Anywhere).iterator();
    }

    /**
     * Returns the size of amino acid depending on the location.
     *
     * @param location amino acid location
     */
    public int size(Location location)
    {
        return aaListMap.get(location).size();
    }

    /**
     * @return the size of anywhere amino acids
     */
    public int size()
    {
        return aaListMap.get(Location.Anywhere).size();
    }

    /**
     * Retrieve an array of amino acids given the specific standard residue, modified {@link AminoAcid} are included
     *
     * @param location          amino acid location
     * @param standardAAResidue the standard residue to look up
     * @return the array of amino acids or an empty array otherwise
     */
    public AminoAcid[] getAminoAcids(Location location, char standardAAResidue)
    {
        AminoAcid[] matches = standardResidueAAArrayMap.get(location).get(standardAAResidue);
        if (matches != null)
            return matches;
        else
            return EMPTY_AA_ARRAY;
    }

    /**
     * Retrieve an array of amino acids given the specific nominal mass.
     *
     * @param location    amino acid location
     * @param nominalMass nominal mass to look up
     * @return the array of amino acids or an empty list otherwise
     */
    public AminoAcid[] getAminoAcids(Location location, int nominalMass)
    {
        AminoAcid[] matches = nominalMass2aa.get(location).get(nominalMass);
        if (matches != null) return matches;
        return EMPTY_AA_ARRAY;
    }

    /**
     * Retrieve an array of amino acids given the specific nominal mass.
     *
     * @param nominalMass the mass to look up
     * @return the array of amino acids or an empty list otherwise
     */
    public AminoAcid[] getAminoAcids(int nominalMass)
    {
        return getAminoAcids(Location.Anywhere, nominalMass);
    }

    /**
     * Checks whether a residue belongs to this amino acid set,
     * such as 'S' for standard Ser and 's' for variable modified Ser.
     *
     * @param residue a residue, which the char of Amino acid, standard or modified.
     * @return true if residue belongs to the amino acid set
     */
    public boolean contains(char residue)
    {
        return residueMap.containsKey(residue);
    }

    /**
     * Returns a list of all residues without mods, no duplication.
     */
    public ArrayList<Character> getResidueListWithoutMods()
    {
        ArrayList<Character> residues = new ArrayList<>();
        for (Map.Entry<Character, AminoAcid> aa : residueMap.entrySet()) {
            char residue = aa.getValue().getUnmodResidue();
            if (!residues.contains(residue)) {
                residues.add(residue);
            }
        }
        return residues;
    }

    /**
     * Returns a list of all residues, including modified residues, fixed modifications do not change the set.
     */
    public ArrayList<Character> getResidueList()
    {
        return new ArrayList<>(residueMap.keySet());
    }

    /**
     * Get the unmodified amino acid of the residue.
     *
     * @param residue the amino acid mass. Use upper case for standard aa (convention).
     *                this method is case sensitive.
     * @return the amino acid object. null if no aa corresponding to the residue
     */
    public AminoAcid getAminoAcid(Location location, char residue)
    {
        AminoAcid[] aaArr = getAminoAcids(location, residue);
        for (AminoAcid aa : aaArr)
            if (!aa.isModified())
                return aa;
        return null;
    }

    /**
     * Get the amino acid mass of the residue.
     *
     * @param residue the amino acid mass. Use upper case for standard aa (convention).
     *                this method is case sensitive.
     * @return the amino acid object. null if no aa corresponding to the residue
     */
    public AminoAcid getAminoAcid(char residue)
    {
        return residueMap.get(residue);
    }

    /**
     * Set the number of allowable variable modifications per peptide
     *
     * @param maxNumberOfVariableModificationsPerPeptide the number of allowable variable modifications per peptide
     */
    public void setMaxNumberOfVariableModificationsPerPeptide(int maxNumberOfVariableModificationsPerPeptide)
    {
        this.maxNumberOfVariableModificationsPerPeptide = maxNumberOfVariableModificationsPerPeptide;
    }

    /**
     * @return the number of allowable variable modifications per peptide
     */
    public int getMaxNumberOfVariableModificationsPerPeptide()
    {
        return this.maxNumberOfVariableModificationsPerPeptide;
    }

    /**
     * Get all amino acids for all locations, including modified amino acids.
     *
     * @return an array of all amino acids
     */
    public AminoAcid[] getAllAminoAcidArr()
    {
        return this.allAminoAcidArr;
    }

    /**
     * Get the amino acid corresponding to the index, the index is the order of the
     * amino acid in amino acid array in ascending mass order
     *
     * @param index amino acid index
     * @return amino acid object
     */
    public AminoAcid getAminoAcid(int index)
    {
        return allAminoAcidArr[index];
    }

    /**
     * Get the index of the aa, the index is the order of Amino acid with mass in ascending order in all amino acids.
     *
     * @param aa amino acid
     * @return the index of aa. null if aa does not belong to this amino acid set
     */
    public int getIndex(AminoAcid aa)
    {
        Integer index = aa2index.get(aa);
        if (index == null)
            index = -1;
        return index;
    }

    /**
     * Get the peptide corresponding to the string sequence.
     *
     * @param sequence sequence of the peptide.
     * @return peptide object of the sequence
     */
    public Peptide getPeptide(String sequence)
    {
        boolean isModified = false;
        ArrayList<AminoAcid> aaArray = new ArrayList<>();
        for (int i = 0; i < sequence.length(); i++) {
            char residue = sequence.charAt(i);
            AminoAcid aa = this.getAminoAcid(residue);
            if (aa == null) {
                logger.info("{}: {} is null!", sequence, residue);
            }
            assert (aa != null) : sequence + ": " + residue + " is null!";
            if (aa.isModified())
                isModified = true;
            aaArray.add(aa);
        }
        Peptide pep = new Peptide(aaArray);
        pep.setModified(isModified);

        return pep;
    }

    /**
     * @return the nominal mass of the amino acid with largest mass
     */
    public int getMaxNominalMass()
    {
        return this.heaviestAA.getNominalMass();
    }

    /**
     * @return the nominal mass of the amino acid with smallest mass
     */
    public int getMinNominalMass()
    {
        return this.lightestAA.getNominalMass();
    }

    /**
     * @return the {@link AminoAcid} with smallest mass
     */
    public AminoAcid getLightestAA()
    {
        return this.lightestAA;
    }

    /**
     * @return {@link AminoAcid} with largest mass
     */
    public AminoAcid getHeaviestAA()
    {
        return this.heaviestAA;
    }

    /**
     * @return true if this contains any variable or terminal (fixed or variable) modification
     */
    public boolean containsModification()
    {
        return this.containsModification;
    }

    /**
     * @return true if this contains any (fixed or variable) modification specific to N-terminus
     */
    public boolean containsNTermModification()
    {
        return this.containsNTermModification;
    }

    /**
     * @return true if this contains any (fixed or variable) modification specific to C-terminus
     */
    public boolean containsCTermModification()
    {
        return this.containsCTermModification;
    }

    /**
     * @return true if this contains phosphorylation
     */
    public boolean containsPhosphorylation()
    {
        return this.containsPhosphorylation;
    }

    /**
     * @return true if contains the iTRAQ modification.
     */
    public boolean containsITRAQ()
    {
        return this.containsITRAQ;
    }

    public boolean containsTMT()
    {
        return this.containsTMT;
    }

    /**
     * @return the residue with maximum char value
     */
    public char getMaxResidue()
    {
        return nextResidue;
    }

    public void registerEnzyme(Enzyme enzyme)
    {
        if (enzyme == null || enzyme.isUnspecific() ||
                enzyme.getPeptideCleavageEfficiency() == 0 || enzyme.getNeighboringAACleavageEffiency() == 0)
            return;

        probCleavageSites = 0;
        HashSet<Character> residues = new HashSet<>(enzyme.getAminoAcidBeforeSet());
        residues.addAll(enzyme.getAminoAcidAfterSet());
        for (char residue : residues) {
            AminoAcid aa = this.getAminoAcid(residue);
            if (aa == null) {
                logger.error("Invalid Enzyme cleavage site: " + residue);
                throw new IllegalArgumentException("No amino acid of '" + residue + "' for enzyme " + enzyme.getName());
            }
            probCleavageSites += aa.getProbability();
        }

        if (probCleavageSites == 0 || probCleavageSites == 1) {
            logger.error("Probability of enzyme residues must be in (0,1)!");
            throw new IllegalArgumentException("Probability of enzyme residues must be in (0,1)!");
        }

        float peptideCleavageEfficiency = enzyme.getPeptideCleavageEfficiency();
        float neighboringAACleavageEfficiency = enzyme.getNeighboringAACleavageEffiency();

        peptideCleavageCredit = (int) Math.round(Math.log(peptideCleavageEfficiency / probCleavageSites));
        peptideCleavagePenalty = (int) Math.round(Math.log((1 - peptideCleavageEfficiency) / (1 - probCleavageSites)));
        neighboringAACleavageCredit = (int) Math.round(Math.log(neighboringAACleavageEfficiency / probCleavageSites));
        neighboringAACleavagePenalty = (int) Math.round(Math.log((1 - neighboringAACleavageEfficiency) / (1 - probCleavageSites)));
    }

    /**
     * Return the credit of the neighbor amino acids of the peptide to be enzyme cleavable.
     */
    public int getNeighboringAACleavageCredit()
    {
        return neighboringAACleavageCredit;
    }

    /**
     * @return the penalty of the neighbor amino acids not to be enzyme cleavable.
     */
    public int getNeighboringAACleavagePenalty()
    {
        return neighboringAACleavagePenalty;
    }

    /**
     *
     */
    public int getPeptideCleavageCredit()
    {
        return peptideCleavageCredit;
    }

    public int getPeptideCleavagePenalty()
    {
        return peptideCleavagePenalty;
    }

    /**
     * probability of the cleavage of current Enzyme, which is the sum of the ratios all cleavable
     * amino acids in the fasta database.
     *
     * @return ratios sum of all cleavable amino acids in the database.
     */
    public float getProbCleavageSites()
    {
        return probCleavageSites;
    }

    /**
     * print the Amino acid set
     */
    public void printAASet()
    {
        System.out.println("NumMods: " + this.getMaxNumberOfVariableModificationsPerPeptide());
        for (Location location : Location.values()) {
            ArrayList<AminoAcid> aaList = this.getAAList(location);
            System.out.println(location + "\t" + aaList.size());
            for (AminoAcid aa : aaList)
                System.out.println(aa.getResidueStr() + (aa.isModified() ? "*" : "") + "\t" + (int) aa.getResidue() + "\t" + aa.getNominalMass() + "\t" + aa.getMass() + "\t" + aa.getProbability());
        }
    }

    /**
     * add an {@link AminoAcid} with {@link Location#Anywhere}
     */
    private void addAminoAcid(AminoAcid aa)
    {
        addAminoAcid(aa, Location.Anywhere);
    }

    /**
     * Add an {@link AminoAcid} to given {@link Location}
     */
    private void addAminoAcid(AminoAcid aa, Location location)
    {
        for (Location loc : locMap.get(location)) {
            aaListMap.get(loc).add(aa);
        }
    }

    private List<Modification.Instance> modifications;

    /**
     * apply given modification list to AminoAcidSet.
     */
    private void applyModifications(ArrayList<Modification.Instance> mods)
    {
        this.modifications = mods;

        // partition modification instances into different types
        HashMap<Modification.Location, ArrayList<Modification.Instance>> fixedMods = new HashMap<>();
        HashMap<Modification.Location, ArrayList<Modification.Instance>> variableMods = new HashMap<>();
        for (Location location : Modification.Location.values()) {
            fixedMods.put(location, new ArrayList<>());
            variableMods.put(location, new ArrayList<>());
        }
        for (Modification.Instance mod : mods) {
            if (mod.isFixedModification())
                fixedMods.get(mod.getLocation()).add(mod);
            else
                variableMods.get(mod.getLocation()).add(mod);
        }

        Location[] locArr = new Location[]{
                Location.Anywhere,
                Location.N_Term,
                Location.C_Term,
                Location.Protein_N_Term,
                Location.Protein_C_Term,
        };

        // Fixed modifications
        for (Location loc : locArr)
            applyFixedMods(fixedMods, loc);

        // Variable modifications
        for (Location loc : locArr)
            addVariableMods(variableMods, loc);

        // setup containsNTermModification and containsCTermModification
        for (Modification.Instance mod : mods) {
            Location location = mod.getLocation();
            if (!containsNTermModification && (location == Location.N_Term || location == Location.Protein_N_Term))
                this.containsNTermModification = true;
            if (!containsCTermModification && (location == Location.C_Term || location == Location.Protein_C_Term))
                this.containsCTermModification = true;
            if (location != Location.Anywhere || !mod.isFixedModification())
                this.containsModification = true;
            if (mod.getModification().getName().toLowerCase().startsWith("phospho"))
                this.containsPhosphorylation = true;
            if (mod.getModification().getName().toLowerCase().startsWith("itraq"))
                this.containsITRAQ = true;
            if (mod.getModification().getName().toLowerCase().startsWith("tmt"))
                this.containsTMT = true;
        }
    }

    /**
     * apply fixed modifications at given Location to this {@link AminoAcidSet}
     *
     * @param fixedMods fixed modification
     * @param location  {@link Location}
     */
    private void applyFixedMods(HashMap<Modification.Location, ArrayList<Modification.Instance>> fixedMods, Location location)
    {
        for (Modification.Instance mod : fixedMods.get(location)) {
            // residue-specific
            char residue = mod.getResidue();
            if (residue == '*')
                continue;

            ArrayList<AminoAcid> oldAAList = this.getAAList(location);
            ArrayList<AminoAcid> newAAList = new ArrayList<>();

            for (AminoAcid aa : oldAAList) {
                if (aa.getUnmodResidue() != residue)
                    newAAList.add(aa);
                else {
                    if (location == Location.Anywhere)
                        newAAList.add(aa.getAAWithFixedModification(mod.getModification()));    // replace with a new amino acid
                    else {
                        ModifiedAminoAcid modAA = getModifiedAminoAcid(aa, mod);
                        newAAList.add(modAA);
                    }
                }
            }

            for (Location loc : locMap.get(location))
                aaListMap.put(loc, new ArrayList<>(newAAList));
        }

        // any residue
        for (Modification.Instance mod : fixedMods.get(location)) {
            char residue = mod.getResidue();
            if (residue != '*')
                continue;
            ArrayList<AminoAcid> oldAAList = this.getAAList(location);
            ArrayList<AminoAcid> newAAList = new ArrayList<>();

            for (AminoAcid aa : oldAAList) {
                if (location == Location.Anywhere)
                    newAAList.add(aa.getAAWithFixedModification(mod.getModification()));
                else {
                    ModifiedAminoAcid modAA = getModifiedAminoAcid(aa, mod);
                    newAAList.add(modAA);
                }
            }

            for (Location loc : locMap.get(location))
                aaListMap.put(loc, new ArrayList<>(newAAList));
        }
    }

    private void addVariableMods(HashMap<Modification.Location, ArrayList<Modification.Instance>> variableMods, Location location)
    {
        // residue-specific
        for (Location loc : locMap.get(location)) {
            ArrayList<AminoAcid> newAAList = new ArrayList<>();
            ArrayList<AminoAcid> oldAAList = this.getAAList(loc);
            for (AminoAcid targetAA : oldAAList) {
                for (Modification.Instance mod : variableMods.get(location)) {
                    char residue = mod.getResidue();
                    if (residue == '*')
                        continue;
                    if (targetAA.getUnmodResidue() == residue) {
                        if (targetAA.isModified()) {
                            if (targetAA.hasResidueSpecificVariableMod())
                                continue;
                        }
                        ModifiedAminoAcid modAA = getModifiedAminoAcid(targetAA, mod);
                        newAAList.add(modAA);
                    }
                }
            }
            for (AminoAcid newAA : newAAList)
                aaListMap.get(loc).add(newAA);
        }

        // any residue
        for (Location loc : locMap.get(location)) {
            ArrayList<AminoAcid> newAAList = new ArrayList<>();
            ArrayList<AminoAcid> oldAAList = this.getAAList(loc);
            for (AminoAcid targetAA : oldAAList) {
                for (Modification.Instance mod : variableMods.get(location)) {
                    char residue = mod.getResidue();
                    if (residue != '*')
                        continue;
                    if (targetAA.isModified()) {
                        if (targetAA.hasTerminalVariableMod())
                            continue;
                    }
                    ModifiedAminoAcid modAA = getModifiedAminoAcid(targetAA, mod);
                    newAAList.add(modAA);
                }
            }
            for (AminoAcid newAA : newAAList)
                aaListMap.get(loc).add(newAA);
        }
    }

    /**
     * process all amino acids, and create all the related information maps.
     */
    private AminoAcidSet finalizeSet()
    {
        // update the allAminoAcidArr
        HashSet<AminoAcid> allAASet = new HashSet<>();
        for (Location location : aaListMap.keySet()) {
            allAASet.addAll(aaListMap.get(location));
        }
        this.allAminoAcidArr = allAASet.toArray(EMPTY_AA_ARRAY);
        Arrays.sort(allAminoAcidArr);

        // assign index, heaviest and lightest aa
        double minMass = Double.MAX_VALUE;
        int lightIndex = -1;
        double maxMass = Double.MIN_VALUE;
        int heavyIndex = -1;
        aa2index = new HashMap<>();
        for (int i = 0; i < allAminoAcidArr.length; i++) {
            aa2index.put(allAminoAcidArr[i], i);

            double mass = allAminoAcidArr[i].getAccurateMass();
            if (mass < minMass) {
                lightIndex = i;
                minMass = mass;
            }
            if (mass > maxMass) {
                heavyIndex = i;
                maxMass = mass;
            }
        }
        this.heaviestAA = allAminoAcidArr[heavyIndex];
        this.lightestAA = allAminoAcidArr[lightIndex];

        // initialize residueMap
        residueMap = new HashMap<>();
        for (AminoAcid aa : allAminoAcidArr) {
            assert (residueMap.get(aa.getResidue()) == null) : aa.getResidue() + " already exists!";
            residueMap.put(aa.getResidue(), aa);
        }

        // initialize standardResidueAAArrayMap and nominalMass2aa map
        standardResidueAAArrayMap = new HashMap<>();
        nominalMass2aa = new HashMap<>();
        for (Location location : Location.values()) {
            standardResidueAAArrayMap.put(location, new HashMap<>());
            nominalMass2aa.put(location, new HashMap<>());
        }

        for (Location location : Location.values()) {
            HashMap<Integer, ArrayList<AminoAcid>> mass2aaList = new HashMap<>(); // nominal mass to amino acid list
            HashMap<Character, LinkedList<AminoAcid>> stdResidue2aaList = new HashMap<>();

            for (AminoAcid aa : this.getAAList(location)) {
                int thisMass = aa.getNominalMass();
                if (!mass2aaList.containsKey(thisMass)) {
                    mass2aaList.put(thisMass, new ArrayList<>());
                }
                mass2aaList.get(thisMass).add(aa);

                char stdResidue = aa.getUnmodResidue();
                LinkedList<AminoAcid> aaList = stdResidue2aaList.get(stdResidue);
                if (aaList == null)
                    aaList = new LinkedList<>();
                if (!aa.isModified())
                    aaList.addFirst(aa);    // unmodified residue is at first
                else
                    aaList.addLast(aa);
                stdResidue2aaList.put(stdResidue, aaList);
            }

            // convert the array back to real arrays
            HashMap<Integer, AminoAcid[]> mass2aaArray = new HashMap<>();
            for (int mass : mass2aaList.keySet()) {
                mass2aaArray.put(mass, mass2aaList.get(mass).toArray(new AminoAcid[0]));
            }

            HashMap<Character, AminoAcid[]> stdResidue2aaArray = new HashMap<>();
            for (char residue : stdResidue2aaList.keySet())
                stdResidue2aaArray.put(residue, stdResidue2aaList.get(residue).toArray(new AminoAcid[0]));

            this.nominalMass2aa.put(location, mass2aaArray);
            this.standardResidueAAArrayMap.put(location, stdResidue2aaArray);
        }

        return this;
    }

    /**
     * standard amino acid set.
     */
    private static AminoAcidSet standardAASet = null;
    private static AminoAcidSet standardAASetWithCarbamidomethylatedCys = null;
    private static AminoAcidSet standardAASetWithCarboxyomethylatedCys = null;
    private static AminoAcidSet standardAASetWithCarbamidomethylatedCysWithTerm = null;

    /**
     * parse modification, and apply it to AminoAcidSet.
     *
     * @param fileName modification text file
     */
    public static AminoAcidSet getAminoAcidSetFromModFile(String fileName)
    {
        // parse modifications
        ArrayList<Modification.Instance> mods = new ArrayList<>();
        ArrayList<AminoAcid> customAA = new ArrayList<>();
        int numMods = 2;
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(fileName));
            assert in != null;

            String customAAResidues = "";
            String s;
            int lineNum = 0;
            while ((s = in.readLine()) != null) {
                lineNum++;
                String[] tokenArr = s.split("#");
                if (tokenArr.length == 0)
                    continue;

                s = tokenArr[0].trim();
                if (s.length() == 0) // ignore comments
                    continue;

                if (s.startsWith("NumMods=")) {
                    try {
                        numMods = Integer.parseInt(s.split("=")[1].trim());
                    } catch (NumberFormatException e) {
                        logger.error("{}: Invalid NumMods option at line {}: {}", fileName, lineNum, s);
                        throw new NumberFormatException(fileName + ": Invalid NumMods option at line " + lineNum + ": " + s);
                    }
                } else {
                    String[] token = s.split(",");
                    if (token.length != 5)
                        continue;

                    // Mass or Composition
                    double modMass;
                    String compStr = token[0].trim();
                    Double mass = Composition.getMass(compStr);
                    if (mass != null)
                        modMass = mass;
                    else {
                        try {
                            modMass = Double.parseDouble(compStr);
                        } catch (NumberFormatException e) {
                            logger.error("{}: AminoAcidSet: Invalid Mass/Composition at line {}: {}", fileName, lineNum, s);
                            throw new NumberFormatException(fileName + ": AminoAcidSet: Invalid Mass/Composition at line " + lineNum + ": " + s);
                        }
                    }

                    // Residues
                    String residueStr = token[1].trim();
                    boolean isResidueStrLegitimate = true;
                    boolean matchesCustomAA = false;
                    if (!residueStr.equals("*")) {
                        if (residueStr.length() > 0) {
                            for (int i = 0; i < residueStr.length(); i++) {
                                boolean matchesCustom = customAAResidues.indexOf(residueStr.charAt(i)) > -1;
                                if (matchesCustom) {
                                    matchesCustomAA = true;
                                }
                                if (!matchesCustom && !AminoAcid.isStdAminoAcid(residueStr.charAt(i))) {
                                    isResidueStrLegitimate = false;
                                    break;
                                }
                            }
                        } else
                            isResidueStrLegitimate = false;
                    }

                    // isFixedModification
                    boolean isFixedModification = false;
                    boolean isCustomAminoAcid = false;
                    boolean modTypeParseFailed = false;
                    if (token[2].trim().equalsIgnoreCase("fix"))
                        isFixedModification = true;
                    else if (token[2].trim().equalsIgnoreCase("opt"))
                        isFixedModification = false;
                    else if (token[2].trim().equalsIgnoreCase("custom"))
                        isCustomAminoAcid = true;
                    else
                        modTypeParseFailed = true;

                    if ((!isResidueStrLegitimate && !isCustomAminoAcid) || (isCustomAminoAcid && matchesCustomAA)) {
                        logger.error("{}: AminoAcidSet: Invalid Residue(s) at line {}: ", fileName, lineNum, s);
                        throw new IllegalArgumentException(fileName + ": AminoAcidSet: Invalid Residue(s) at line " + lineNum + ": " + s);
                    }
                    if (isCustomAminoAcid && (residueStr.length() > 1 || !residueStr.toLowerCase().matches("[bjouxz]"))) {
                        logger.error("{}: AminoAcidSet: Invalid Residue(s) at line {}: {}", fileName, lineNum, s);
                        logger.error("Custom Amino acids are only allowed using B, J, O, U, X, or Z as the custom symbol.");
                        throw new IllegalArgumentException(fileName + ": AminoAcidSet: Invalid Residue(s) at line " + lineNum + ": " + s);
                    }
                    if (isCustomAminoAcid && !compStr.matches("([CHNOS][0-9]{0,3})+")) {
                        logger.error("{}: AminoAcidSet: Invalid composition/mass at line {}: {}", fileName, lineNum, s);
                        logger.error("Custom Amino acids must supply a composition string, and must not use elements other than C H N O S.");
                        throw new IllegalArgumentException(fileName + ": AminoAcidSet: Invalid composition/mass at line " + lineNum + ": " + s);
                    }
                    if (modTypeParseFailed) {
                        logger.error("{}: AminoAcidSet: Modification must be either fix, opt, or custom at line {}: {}", fileName, lineNum, s);
                        throw new IllegalArgumentException(fileName + ": AminoAcidSet: Modification must be either fix, opt, or custom at line " + lineNum + ": " + s);
                    }

                    // Location
                    Modification.Location location = null;
                    String customResidueBase = "";
                    String locStr = token[3].trim().split("\\s+")[0].trim();
                    if (locStr.equalsIgnoreCase("any"))
                        location = Modification.Location.Anywhere;
                    else if (locStr.equalsIgnoreCase("N-Term") || locStr.equalsIgnoreCase("NTerm"))
                        location = Modification.Location.N_Term;
                    else if (locStr.equalsIgnoreCase("C-Term") || locStr.equalsIgnoreCase("CTerm"))
                        location = Modification.Location.C_Term;
                    else if (locStr.equalsIgnoreCase("Prot-N-Term") || locStr.equalsIgnoreCase("ProtNTerm"))
                        location = Modification.Location.Protein_N_Term;
                    else if (locStr.equalsIgnoreCase("Prot-C-Term") || locStr.equalsIgnoreCase("ProtCTerm"))
                        location = Modification.Location.Protein_C_Term;
                    else if (isCustomAminoAcid)
                        customResidueBase = locStr;
                    else {
                        logger.error("{}: AminoAcidSet: Invalid Location at line {}: {}", fileName, lineNum, s);
                        throw new IllegalArgumentException(fileName + ": AminoAcidSet: Invalid Location at line " + lineNum + ": " + s);
                    }

                    String name = token[4].trim().split("\\s+")[0].trim();
                    if (!isCustomAminoAcid) {
                        Modification mod = Modification.register(name, modMass);

                        for (int i = 0; i < residueStr.length(); i++) {
                            char residue = residueStr.charAt(i);
                            Modification.Instance modIns = new Modification.Instance(mod, residue, location);
                            if (isFixedModification)
                                modIns.fixedModification();
                            mods.add(modIns);
                        }
                    } else {
                        AminoAcid aa = new AminoAcid(residueStr.charAt(0), name, new Composition(compStr));
                        customAAResidues += residueStr.charAt(0);
                        customAA.add(aa);
                    }
                }
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods, customAA);
        aaSet.setMaxNumberOfVariableModificationsPerPeptide(numMods);
        return aaSet;
    }

    public static AminoAcidSet getAminoAcidSetFromXMLFile(String fileName)
    {
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(fileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        int numMods = 2;

        // memorize keywords
        String numModsKey = "<parameter name=\"ptm.mods\">";
        String cysKey = "<parameter name=\"cysteine_protease.cysteine\">";
        String oxidationKey = "<parameter name=\"ptm.OXIDATION\">on</parameter>";
        String lysMetKey = "<parameter name=\"ptm.LYSINE_METHYLATION\">on</parameter>";
        String pyrogluKey = "<parameter name=\"ptm.PYROGLUTAMATE_FORMATION\">on</parameter>";
        String phosphoKey = "<parameter name=\"ptm.PHOSPHORYLATION\">on</parameter>";
        String ntermCarbamyKey = "<parameter name=\"ptm.NTERM_CARBAMYLATION\">on</parameter>";
        String ntermAcetylKey = "<parameter name=\"ptm.NTERM_ACETYLATION\">on</parameter>";
        String ptmKey = "<parameter name=\"ptm.custom_PTM\">";
        String closeKey = "</parameter>";

        // parse modifications
        ArrayList<Modification.Instance> mods = new ArrayList<Modification.Instance>();
        String s;
        int lineNum = 0;
        try {
            while ((s = in.readLine()) != null) {
                lineNum++;
                if (s.startsWith(numModsKey)) {
                    try {
                        String value = s.substring(numModsKey.length(), s.lastIndexOf(closeKey));
                        numMods = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        logger.error(fileName + ": Invalid ptm.mods option at line " + lineNum + ": " + s);
                        e.printStackTrace();
                        System.exit(-1);
                    }
                } else if (s.startsWith(cysKey)) {
                    String value = s.substring(cysKey.length(), s.lastIndexOf(closeKey));
                    switch (value) {
                        case "c57": {
                            char residue = 'C';
                            Modification mod = Modification.Carbamidomethyl;
                            Modification.Instance modIns = new Modification.Instance(mod, residue, Location.Anywhere).fixedModification();
                            mods.add(modIns);
                            break;
                        }
                        case "c58": {
                            char residue = 'C';
                            Modification mod = Modification.Carboxymethyl;
                            Modification.Instance modIns = new Modification.Instance(mod, residue, Location.Anywhere).fixedModification();
                            mods.add(modIns);
                            break;
                        }
                        case "c99": {
                            char residue = 'C';
                            Modification mod = Modification.NIPCAM;
                            Modification.Instance modIns = new Modification.Instance(mod, residue, Location.Anywhere).fixedModification();
                            mods.add(modIns);
                            break;
                        }
                        case "None":
                            // do nothing
                            break;
                        default:
                            logger.error(fileName + ": Invalid Cycteine protecting group at line " + lineNum + ": " + s);
                            System.exit(-1);
                    }
                } else if (s.startsWith(ptmKey))    // custom PTM
                {
                    String value = s.substring(ptmKey.length(), s.lastIndexOf(closeKey));
                    String[] token = value.split(",");

                    if (token.length != 3) {
                        System.err.println(fileName + ": Invalid custom ptm option at line " + lineNum + ": " + s);
                        System.exit(-1);
                    }

                    // Mass
                    double modMass = 0;
                    try {
                        modMass = Double.parseDouble(token[0]);
                    } catch (NumberFormatException e) {
                        System.err.println(fileName + ": AminoAcidSet: Invalid Mass at line " + lineNum + ": " + s);
                        e.printStackTrace();
                        System.exit(-1);
                    }

                    // Residues
                    String residueStr = token[1];
                    boolean isResidueStrLegitimate = true;
                    if (!residueStr.equals("*")) {
                        if (residueStr.length() > 0) {
                            for (int i = 0; i < residueStr.length(); i++) {
                                if (!AminoAcid.isStdAminoAcid(residueStr.charAt(i))) {
                                    isResidueStrLegitimate = false;
                                    break;
                                }
                            }
                        } else
                            isResidueStrLegitimate = false;
                    }
                    if (!isResidueStrLegitimate) {
                        System.err.println(fileName + ": AminoAcidSet: Invalid Residue(s) at line " + lineNum + ": " + s);
                        System.exit(-1);
                    }

                    boolean isFixedModification = false;
                    Location location = null;

                    // Type
                    String type = token[2];
                    if (type.equals("fix")) {
                        isFixedModification = true;
                        location = Location.Anywhere;
                    } else if (type.equals("opt")) {
                        isFixedModification = false;
                        location = Location.Anywhere;
                    } else if (type.equals("opt_nterm")) {
                        isFixedModification = false;
                        location = Location.N_Term;
                    } else if (type.equals("fix_nterm")) {
                        isFixedModification = true;
                        location = Location.N_Term;
                    } else if (type.equals("opt_cterm")) {
                        isFixedModification = false;
                        location = Location.C_Term;
                    } else if (type.equals("fix_cterm")) {
                        isFixedModification = true;
                        location = Location.C_Term;
                    } else {
                        System.err.println(fileName + ": AminoAcidSet: Invalid Type(s) at line " + lineNum + ": " + s);
                        System.exit(-1);
                    }

                    String name = residueStr + " " + modMass;

                    Modification mod = Modification.register(name, modMass);

                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, location);
                        if (isFixedModification)
                            modIns.fixedModification();
                        mods.add(modIns);
                    }
                } else if (s.startsWith(oxidationKey))    // predefined Oxidation
                {
                    String residueStr = "M";
                    Modification mod = Modification.Oxidation;
                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, Location.Anywhere);
                        mods.add(modIns);
                    }
                } else if (s.startsWith(lysMetKey))    // predefined
                {
                    String residueStr = "K";
                    Modification mod = Modification.Methyl;
                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, Location.Anywhere);
                        mods.add(modIns);
                    }
                } else if (s.startsWith(pyrogluKey))    // predefined
                {
                    String residueStr = "Q";
                    Modification mod = Modification.PyroGluQ;
                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, Location.N_Term);
                        mods.add(modIns);
                    }
                } else if (s.startsWith(phosphoKey))    // predefined
                {
                    String residueStr = "STY";
                    Modification mod = Modification.Phospho;
                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, Location.Anywhere);
                        mods.add(modIns);
                    }
                } else if (s.startsWith(ntermCarbamyKey))    // predefined
                {
                    String residueStr = "*";
                    Modification mod = Modification.Carbamyl;
                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, Location.N_Term);
                        mods.add(modIns);
                    }
                } else if (s.startsWith(ntermAcetylKey))    // predefined
                {
                    String residueStr = "*";
                    Modification mod = Modification.Acetyl;
                    for (int i = 0; i < residueStr.length(); i++) {
                        char residue = residueStr.charAt(i);
                        Modification.Instance modIns = new Modification.Instance(mod, residue, Location.N_Term);
                        mods.add(modIns);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSet(mods);
        aaSet.setMaxNumberOfVariableModificationsPerPeptide(numMods);

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return aaSet;
    }

    public List<Modification.Instance> getModifications()
    {
        return modifications;
    }

    /**
     * Gets standard amino acids from file
     *
     * @param fileName amino acid set file name.
     * @return amino acid set object.
     */
    public static AminoAcidSet getAminoAcidSet(String fileName)
    {
        AminoAcidSet aaSet = new AminoAcidSet();
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            String s;
            int lineNum = 0;
            int fileType = 0;    // 0: G,Glycine,57.021464   1: G=57.021463723
            while ((s = in.readLine()) != null) {
                lineNum++;
                if (s.startsWith("#") || s.length() == 0)
                    continue;
                if (fileType == 0 && Character.isDigit(s.charAt(0))) {
                    fileType = 1;
                    continue;
                }

                AminoAcid aa;
                if (fileType == 0)    // composition is available e.g. G, Glycine, C2H3N1O1
                {
                    String[] token = s.split(",");
                    if (token.length != 3)
                        continue;
                    String residueStr = token[0].trim();
                    if (residueStr.length() != 1) {
                        System.err.println("Invalid amino acid file format: " + fileName);
                        System.err.println("Residue must be a single character: " + s);
                        System.exit(-1);
                    }
                    char residue = residueStr.charAt(0);
                    if (!Character.isUpperCase(residue)) {
                        System.err.println("Invalid amino acid file format: " + fileName);
                        System.err.println("Residue must be an upper case letter: " + s);
                        System.exit(-1);
                    }
                    String name = token[1].trim();

                    // composition is available
                    if (token[2].matches("(C\\d+)*(H\\d+)*(N\\d+)*(O\\d+)*(S\\d+)*")) {
                        String compositionStr = token[2].trim();    // e.g. C5H9N1O1S1
                        Composition composition = new Composition(compositionStr);
                        aa = AminoAcid.getAminoAcid(residue, name, composition);
                    } else    // composition is not available, there should be a mass
                    {
                        double mass = -1;
                        try {
                            mass = Double.parseDouble(token[2]);
                        } catch (NumberFormatException e) {
                            System.err.println("Invalid AASet File format at line " + lineNum + ": " + s);
                            System.exit(-1);
                        }
                        aa = AminoAcid.getCustomAminoAcid(residue, name, mass);
                    }
                } else    // fileType == 1, only masses (and probabilities) are available (e.g. D=115 or D=115,0.0467)
                {
                    String[] token = s.split("=");
                    if (token.length != 2 || token[0].length() != 1 || !Character.isLetter(token[0].charAt(0))) {
                        System.err.println("Invalid AASet File format at line" + lineNum + ": " + s);
                        System.exit(-1);
                    }
                    char residue = token[0].charAt(0);
                    String name = token[0];
                    float mass = -1;
                    float prob = 0.05f;
                    try {
                        if (!token[1].contains(","))
                            mass = Float.parseFloat(token[1]);
                        else {
                            mass = Float.parseFloat(token[1].split(",")[0]);
                            prob = Float.parseFloat(token[1].split(",")[1]);
                        }
                    } catch (NumberFormatException e) {
                        System.err.println("Invalid AASet File format at line" + lineNum + ": " + s);
                        System.exit(-1);
                    }
                    if (mass <= 0) {
                        System.err.println("Invalid AASet File format at line" + lineNum + ": " + s);
                        System.exit(-1);
                    }
                    aa = AminoAcid.getCustomAminoAcid(residue, name, mass).setProbability(prob);
                }
                aaSet.addAminoAcid(aa);
            }
            aaSet.finalizeSet();
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return aaSet;
    }

    /**
     * @return amino acid set contains all standard amino acids.
     */
    public static AminoAcidSet getStandardAminoAcidSet()
    {
        if (standardAASet == null) {
            standardAASet = new AminoAcidSet();
            for (AminoAcid aa : AminoAcid.getStandardAminoAcids())
                standardAASet.addAminoAcid(aa);
            standardAASet.finalizeSet();
        }
        return standardAASet;
    }

    public static AminoAcidSet getStandardAminoAcidSetWithFixedCarbamidomethylatedCys()
    {
        if (standardAASetWithCarbamidomethylatedCys == null) {
            ArrayList<Modification.Instance> mods = new ArrayList<>();
            mods.add(new Modification.Instance(Modification.Carbamidomethyl, 'C').fixedModification());
            standardAASetWithCarbamidomethylatedCys = AminoAcidSet.getAminoAcidSet(mods);
        }
        return standardAASetWithCarbamidomethylatedCys;
    }

    /**
     * Return the standard amino acid set with a fixed carboxymethylated cys modification
     */
    public static AminoAcidSet getStandardAminoAcidSetWithFixedCarboxymethylatedCys()
    {
        if (standardAASetWithCarboxyomethylatedCys == null) {
            ArrayList<Modification.Instance> mods = new ArrayList<>();
            mods.add(new Modification.Instance(Modification.Carboxymethyl, 'C').fixedModification());
            standardAASetWithCarboxyomethylatedCys = AminoAcidSet.getAminoAcidSet(mods);
        }
        return standardAASetWithCarboxyomethylatedCys;
    }

    /**
     * Creates an alternative amino acid set with the terminal amino acid also encoded.
     *
     * @return the AminoAcidSet with C+57 and X with an arbitrary mass.
     */
    public static AminoAcidSet getStandardAminoAcidSetWithFixedCarbamidomethylatedCysWithTerm()
    {
        if (standardAASetWithCarbamidomethylatedCysWithTerm == null) {
            Modification.Instance[] mods = {
                    new Modification.Instance(Modification.Carbamidomethyl, 'C').fixedModification()
            };

            HashMap<Character, Modification.Instance> modTable = new HashMap<>();
            for (Modification.Instance mod : mods) {
                if (mod.isFixedModification()) // variable modifications will be ignored
                    modTable.put(mod.getResidue(), mod);
            }
            AminoAcidSet aaSet = new AminoAcidSet();
            for (AminoAcid aa : AminoAcid.getStandardAminoAcids()) {
                Modification.Instance mod = modTable.get(aa);
                if (mod == null)
                    aaSet.addAminoAcid(aa);
                else
                    aaSet.addAminoAcid(aa.getAAWithFixedModification(mod.getModification()));
            }
            aaSet.addAminoAcid(AminoAcid.getCustomAminoAcid('X', new Composition(2, 6, 1, 1, 0).getMass()));

            standardAASetWithCarbamidomethylatedCysWithTerm = aaSet.finalizeSet();
        }
        return standardAASetWithCarbamidomethylatedCysWithTerm;
    }

    /**
     * Return the {@link AminoAcidSet} of given modifications
     *
     * @param mods modifications
     * @return an {@link AminoAcidSet}
     */
    public static AminoAcidSet getAminoAcidSet(ArrayList<Modification.Instance> mods)
    {
        AminoAcidSet aaSet = new AminoAcidSet();
        for (AminoAcid aa : getStandardAminoAcidSet())
            aaSet.addAminoAcid(aa);

        aaSet.applyModifications(mods);
        aaSet.finalizeSet();

        return aaSet;
    }

    /**
     * add custom amino acids, and apply mods to amino acids.
     *
     * @param mods             modifications
     * @param customAminoAcids custom amino acids
     */
    public static AminoAcidSet getAminoAcidSet(ArrayList<Modification.Instance> mods, ArrayList<AminoAcid> customAminoAcids)
    {
        AminoAcidSet aaSet = new AminoAcidSet();
        for (AminoAcid aa : getStandardAminoAcidSet())
            aaSet.addAminoAcid(aa);

        for (AminoAcid aa : customAminoAcids)
            aaSet.addAminoAcid(aa);

        aaSet.applyModifications(mods);
        aaSet.finalizeSet();

        return aaSet;
    }

    public static AminoAcidSet getAminoAcidSetFromModAAList(AminoAcidSet baseAASet, ArrayList<AminoAcid> modAAList)
    {
        AminoAcidSet aaSet = new AminoAcidSet();
        for (AminoAcid aa : baseAASet)
            aaSet.addAminoAcid(aa);

        for (AminoAcid aa : modAAList)
            aaSet.addAminoAcid(aa);

        aaSet.finalizeSet();

        return aaSet;
    }


    /**
     * stored all modified amino acid
     */
    private List<ModifiedAminoAcid> modAAList = new ArrayList<>();

    /**
     * return a {@link ModifiedAminoAcid} of given target Amino acid
     *
     * @param targetAA {@link AminoAcid} to modified
     * @param mod      {@link Modification} instance.
     */
    private ModifiedAminoAcid getModifiedAminoAcid(AminoAcid targetAA, Modification.Instance mod)
    {
        // test if it is a new Modification
        for (ModifiedAminoAcid modAA : modAAList) {
            if (modAA.getTargetAA() == targetAA && modAA.getModification() == mod.getModification())
                return modAA;
        }

        char modResidue = getModifiedResidue(targetAA.getUnmodResidue());
        ModifiedAminoAcid modAA = new ModifiedAminoAcid(targetAA, mod, modResidue);
        modAAList.add(modAA);

        return modAA;
    }

    /**
     * Returns a new residue for modified amino acid, for different modification at the same residue,
     * different char is used to distinguish.
     * <p>
     * If the lower case is not occupied, use it fist.
     *
     * @param unmodResidue unmodified amino acid residue
     */
    char getModifiedResidue(char unmodResidue)
    {
        if (!Character.isUpperCase(unmodResidue)) {
            logger.error("Invalid unmodified residue: " + unmodResidue);
            throw new IllegalArgumentException("Only upper case is allowed");
        }
        // if lower case letter is available
        char lowerCaseR = Character.toLowerCase(unmodResidue);
        if (!modResidueSet.contains(lowerCaseR)) {
            modResidueSet.add(lowerCaseR);
            return lowerCaseR;
        }

        // if not, use char value >= 128
        char symbol = this.nextResidue;
        nextResidue++;
        if (nextResidue >= Character.MAX_VALUE) {
            logger.error("Too many modifications!");
            throw new IllegalArgumentException("Too many modifications.");
        }
        return symbol;
    }
}
