/***************************************************************************
 * Title:
 * Author:         Sangtae Kim
 * Last modified:
 *
 * Copyright (c) 2008-2009 The Regents of the University of California
 * All Rights Reserved
 * See file LICENSE for details.
 ***************************************************************************/
package edu.ucsd.msjava.fdr;

import omics.util.utils.Pair;

/**
 * The Class ScoredString.
 * <p>
 * a score value with a string
 */
public class ScoredString extends Pair<String, Float> implements Comparable<Pair<String, Float>>
{
    /**
     * Instantiates a new scored string.
     *
     * @param peptide the peptide
     * @param score   the score
     */
    public ScoredString(String peptide, Float score)
    {
        super(peptide, score);
    }

    public int compareTo(Pair<String, Float> o)
    {
        int scoreComp = getSecond().compareTo(o.getSecond());
        if (scoreComp != 0)
            return scoreComp;
        else
            return getFirst().compareTo(o.getFirst());
    }

    /**
     * Gets the str.
     *
     * @return the str
     */
    public String getStr()
    {
        return super.getFirst();
    }

    /**
     * Gets the score.
     *
     * @return the score
     */
    public float getScore()
    {
        return super.getSecond();
    }
}

