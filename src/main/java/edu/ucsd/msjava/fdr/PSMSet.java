package edu.ucsd.msjava.fdr;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

public abstract class PSMSet
{
    /**
     * scored string, for MSGFPlus, it is peptide sequence+eValue for every PSM
     */
    protected ArrayList<ScoredString> psmList;
    /**
     * peptide -> best score
     */
    protected HashMap<String, Float> peptideScoreTable;

    /**
     * @return
     */
    public ArrayList<ScoredString> getPSMList()
    {
        return psmList;
    }

    public HashMap<String, Float> getPeptideScoreTable()
    {
        return peptideScoreTable;
    }

    public abstract boolean isGreaterBetter();

    public void printPeptideScoreTable()
    {
        if (peptideScoreTable != null) {
            for (Entry<String, Float> entry : peptideScoreTable.entrySet()) {
                System.out.println(entry.getKey() + "\t" + entry.getValue());
            }
        }
    }

    /**
     * @return PSM scores
     */
    public ArrayList<Float> getPSMScores()
    {
        if (psmList == null)
            return null;
        ArrayList<Float> psmScores = new ArrayList<>();
        for (ScoredString ss : psmList)
            psmScores.add(ss.getScore());
        return psmScores;
    }

    public ArrayList<Float> getPepScores()
    {
        if (peptideScoreTable == null)
            return null;
        ArrayList<Float> pepScores = new ArrayList<>();
        for (Entry<String, Float> entry : peptideScoreTable.entrySet()) {
            pepScores.add(entry.getValue());
        }
        return pepScores;
    }

    /**
     * get required PSMSet from source.
     */
    public abstract void read();
}
