package edu.ucsd.msjava.parser;

import edu.ucsd.msjava.msutil.Spectrum;

import java.util.Iterator;

/**
 * A data structure that allows iteration of the mzXML file.
 * Only MS/MS spectra will be returned.
 *
 * @author jung
 */
public class MzXMLSpectraIterator implements Iterator<Spectrum>
{
    private MzXMLSpectraMap map;
    private Iterator<Integer> scanIterator;

    /**
     * Constructor taking the file name.
     */
    public MzXMLSpectraIterator(String fileName)
    {
        this(fileName, 2, Integer.MAX_VALUE);
    }

    /**
     * Constructor taking the file name and mslevel selectors
     *
     * @param fileName   the path to the mzXML file.
     * @param minMSLevel spectra with msLevel less than this will be ignored.
     * @param maxMSLevel spectra with msLevel equal or greater than this will be ignored.
     */
    public MzXMLSpectraIterator(String fileName, int minMSLevel, int maxMSLevel)
    {
        map = new MzXMLSpectraMap(fileName).msLevel(minMSLevel, maxMSLevel);
        this.scanIterator = map.getSpecIndexList().iterator();
    }

    /**
     * Get next spectrum.
     *
     * @return the next spectrum.
     */
    public Spectrum next()
    {
        return map.getSpectrumBySpecIndex(scanIterator.next());
    }

    /**
     * Check whether there is more to parse.
     *
     * @return true if not done or false
     */
    public boolean hasNext()
    {
        return scanIterator.hasNext();
    }

    public void remove()
    {
        assert (false);
    }
}
