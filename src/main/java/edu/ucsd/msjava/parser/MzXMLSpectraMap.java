package edu.ucsd.msjava.parser;

import edu.ucsd.msjava.msutil.ActivationMethod;
import edu.ucsd.msjava.msutil.Peak;
import edu.ucsd.msjava.msutil.Spectrum;
import edu.ucsd.msjava.msutil.SpectrumAccessorBySpecIndex;
import omics.msdk.io.MzXMLReader;
import omics.util.ms.spectra.Dissociation;
import omics.util.ms.spectra.MsnSpectrum;
import omics.util.ms.spectra.RetentionTimeList;
import omics.util.ms.spectra.SpectrumType;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A data structure that allows random access of the mzXML file.
 *
 * @author jung
 */
public class MzXMLSpectraMap implements SpectrumAccessorBySpecIndex
{
    private int minMSLevel = 2;        // inclusive
    private int maxMSLevel = Integer.MAX_VALUE;        // exclusive
    private Map<Integer, MsnSpectrum> spectrumMap = new HashMap<>();
    private int maxScan = 0;

    /**
     * Constructor taking the file path to the mzXML file.
     *
     * @param fileName the path to the file.
     */
    public MzXMLSpectraMap(String fileName)
    {
        try {
            MzXMLReader reader = new MzXMLReader(new File(fileName));
            while (reader.hasNext()) {
                MsnSpectrum spectrum = reader.next();
                int scan = spectrum.getScanNumber().getValue();
                maxScan = Math.max(maxScan, scan);
                spectrumMap.put(scan, spectrum);
            }
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Setter to set msLevel.
     *
     * @param minMSLevel minimum msLevel to be considered (inclusive).
     * @param maxMSLevel maximum msLevel to be considered (inclusive).
     * @return this object.
     */
    public MzXMLSpectraMap msLevel(int minMSLevel, int maxMSLevel)
    {
        this.minMSLevel = minMSLevel;
        this.maxMSLevel = maxMSLevel;
        return this;
    }

    /**
     * Get the spectrum by scan number. The scanNumber is the absolute position
     * of this scan in the file starting at 1. This is different than the num in
     * the mzXML file.
     *
     * @param scanNumber the scan number of the spectrum to look for.
     * @return the Spectrum object or null if not found.
     */
    public Spectrum getSpectrumByScanNum(int scanNumber)
    {
        MsnSpectrum spectrum = spectrumMap.get(scanNumber);
        if (spectrum == null)
            return null;

        int msLevel = spectrum.getMsLevel();
        if (msLevel < minMSLevel || msLevel >= maxMSLevel)
            return null;

        omics.util.ms.peaklist.Peak precursor = spectrum.getPrecursor();
        int precursorCharge = precursor.getCharge();
        precursorCharge = (precursorCharge < 0) ? 0 : precursorCharge;

        Spectrum spec = new Spectrum((float) precursor.getMz(), precursorCharge, (float) precursor.getIntensity());
        int scanNum = spectrum.getScanNumber().getValue();
        spec.setScanNum(scanNum);
        spec.setID("scan=" + scanNum);
        spec.setSpecIndex(scanNum);

        SpectrumType spectrumType = spectrum.getSpectrumType();
        if (spectrumType == SpectrumType.CENTROIDED)
            spec.setIsCentroided(true);
        else
            spec.setIsCentroided(false);

        // parse retention time. Note that retention time is a required field
        RetentionTimeList retentionTimes = spectrum.getRetentionTimes();
        if (!retentionTimes.isEmpty()) {
            spec.setRtIsSeconds(true);
            spec.setRt((float) retentionTimes.getFirst().getTime());
        }

        // set ms level
        spec.setMsLevel(msLevel);

        // add activation method
        Dissociation dissociation = spectrum.getDissociation();
        if (dissociation != Dissociation.UNKNOWN) {
            String activationName = dissociation.toString();
            ActivationMethod method = ActivationMethod.get(activationName);
            if (method == null) {
                method = ActivationMethod.register(activationName, activationName);
            }
            spec.setActivationMethod(method);
        }
        // add peaks
        for (int i = 0; i < spectrum.size(); i++) {
            spec.add(new Peak((float) spectrum.getMz(i), (float) spectrum.getIntensity(i), 1));
        }
        spec.determineIsCentroided();
        return spec;
    }

    public Spectrum getSpectrumBySpecIndex(int specIndex)
    {
        return getSpectrumByScanNum(specIndex);
    }

    /**
     * Get the number of scans in this file.
     *
     * @return the number of total scans.
     */
    public int getMaxScanNumber()
    {
        return maxScan;
    }

    private ArrayList<Integer> specIndexList = null;

    public ArrayList<Integer> getSpecIndexList()
    {
        if (specIndexList == null) {
            specIndexList = new ArrayList<>();
            for (int scanNumber = 1; scanNumber <= maxScan; scanNumber++) {
                MsnSpectrum spectrum = spectrumMap.get(scanNumber);
                if (spectrum != null && spectrum.getMsLevel() >= minMSLevel && spectrum.getMsLevel() <= maxMSLevel) {
                    specIndexList.add(scanNumber);
                }
            }
        }
        return specIndexList;
    }

    @Override
    public Spectrum getSpectrumById(String specId)
    {
        if (!specId.matches("scan=\\d+"))
            return null;
        int scanNum = Integer.parseInt(specId.substring(specId.lastIndexOf('=') + 1));
        return getSpectrumByScanNum(scanNum);
    }
}
