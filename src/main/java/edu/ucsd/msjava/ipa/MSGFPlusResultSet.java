package edu.ucsd.msjava.ipa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MSGFPlusResultSet
{
    private String header;
    private List<PSM> psmList;

    public MSGFPlusResultSet(File msgfPlusResultFile)
    {
        parse(msgfPlusResultFile);
    }

    public String getHeader()
    {
        return header;
    }

    public List<PSM> getPSMList()
    {
        return psmList;
    }

    private void parse(File msgfPlusResultFile)
    {
        psmList = new ArrayList<PSM>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(msgfPlusResultFile));
            String s;

            header = in.readLine();    // header
            while ((s = in.readLine()) != null) {
                PSM psm = new PSM(s);
                psmList.add(psm);
            }
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
