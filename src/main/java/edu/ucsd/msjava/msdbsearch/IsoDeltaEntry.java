package edu.ucsd.msjava.msdbsearch;

import edu.ucsd.msjava.msutil.Composition;
import omics.msgf.DeltaEntry;
import omics.util.chem.Weighable;
import omics.util.utils.StringUtils;

import java.util.Objects;
import java.util.TreeSet;

/**
 * This class represents the delta mass combination of isotope and DeltaEntry.
 *
 * @author JiaweiMao
 * @version 1.0.0
 * @since 24 Jan 2019, 7:12 PM
 */
public class IsoDeltaEntry implements Comparable<IsoDeltaEntry>, Weighable
{
    private int isotopeNum;
    private DeltaEntry deltaEntry;
    private double mass;
    private TreeSet<IsoDeltaEntry> degenerateSet;

    /**
     * Create a new {@link IsoDeltaEntry}
     *
     * @param isotopeNum number of isotope error
     * @param deltaEntry a {@link DeltaEntry}
     */
    public IsoDeltaEntry(int isotopeNum, DeltaEntry deltaEntry)
    {
        this.isotopeNum = isotopeNum;
        this.deltaEntry = deltaEntry;
        this.mass = deltaEntry.getSecond() + isotopeNum * Composition.ISOTOPE;
    }

    /**
     * @return number of isotope
     */
    public int getIsotopeNum()
    {
        return isotopeNum;
    }

    public DeltaEntry getDeltaEntry()
    {
        return deltaEntry;
    }

    /**
     * @return the {@link IsoDeltaEntry} set with same mass
     */
    public TreeSet<IsoDeltaEntry> getDegenerateSet()
    {
        return degenerateSet;
    }

    public void addDegenerateEntry(IsoDeltaEntry entry)
    {
        if (degenerateSet == null) {
            degenerateSet = new TreeSet<>();
        }
        degenerateSet.add(entry);
    }

    /**
     * @return delta mass, which is sum of isotope error and delta entry
     */
    @Override
    public double getMolecularMass()
    {
        return mass;
    }

    @Override
    public String toString()
    {
        StringBuilder buffer = new StringBuilder();
        buffer.append("Isotope=");
        buffer.append(isotopeNum);
        buffer.append(StringUtils.SEP0);
        buffer.append("DeltaEntry=(");
        buffer.append(deltaEntry.getFirst());
        buffer.append(StringUtils.SEP0);
        buffer.append(deltaEntry.getSecond());
        buffer.append(StringUtils.SEP0);
        buffer.append(")Mass=");
        buffer.append(mass);
        buffer.append(StringUtils.SEP1);
        if (degenerateSet != null && !degenerateSet.isEmpty()) {
            for (IsoDeltaEntry deltaEntry : degenerateSet) {
                buffer.append(deltaEntry.isotopeNum);
                buffer.append(StringUtils.SEP0);
                buffer.append(deltaEntry.deltaEntry.getFirst());
                buffer.append(StringUtils.SEP0);
                buffer.append(deltaEntry.deltaEntry.getSecond());
                buffer.append(StringUtils.SEP0);
                buffer.append(deltaEntry.getMolecularMass());
                buffer.append(StringUtils.SEP1);
            }
        }
        return buffer.toString();
    }

    @Override public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IsoDeltaEntry that = (IsoDeltaEntry) o;
        return isotopeNum == that.isotopeNum &&
                deltaEntry.equals(that.deltaEntry);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(isotopeNum, deltaEntry);
    }

    @Override
    public int compareTo(IsoDeltaEntry o)
    {
        return Double.compare(this.mass, o.mass);
    }
}
