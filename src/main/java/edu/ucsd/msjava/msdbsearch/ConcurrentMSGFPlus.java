package edu.ucsd.msjava.msdbsearch;

import edu.ucsd.msjava.misc.ProgressData;
import edu.ucsd.msjava.misc.ProgressReporter;

import java.util.List;

/**
 * class implements the logic of MS-GF+ search processes.
 *
 * @version 1.0.0
 * @since 21 Jan 2019, 5:58 PM
 */
public class ConcurrentMSGFPlus implements Runnable, ProgressReporter
{
    private final ScoredSpectraMap specScanner;
    private final DBScanner scanner;
    private SearchParams params;
    private List<MSGFPlusMatch> resultList;
    private final int taskNum;
    private ProgressData progress;

    @Override
    public void setProgressData(ProgressData data)
    {
        progress = data;
    }

    @Override
    public ProgressData getProgressData()
    {
        return progress;
    }

    /**
     * Constructor.
     *
     * @param specScanner {@link ScoredSpectraMap}
     * @param sa          {@link CompactSuffixArray}
     * @param params      {@link SearchParams}
     * @param resultList  list to store {@link MSGFPlusMatch}
     * @param taskNum     task id number
     */
    public ConcurrentMSGFPlus(ScoredSpectraMap specScanner, CompactSuffixArray sa, SearchParams params,
                              List<MSGFPlusMatch> resultList, int taskNum
    )
    {
        this.specScanner = specScanner;
        this.params = params;
        this.scanner = new DBScanner(
                specScanner,
                sa,
                params.getEnzyme(),
                params.getAASet(),
                params.getNumMatchesPerSpec(),
                params.getMinPeptideLength(),
                params.getMaxPeptideLength(),
                params.getMaxNumVariatsPerPeptide(),
                params.ignoreMetCleavage(),
                params.getMaxMissedCleavages()
        );
        this.resultList = resultList;
        this.taskNum = taskNum;
        progress = null;
    }

    @Override
    public void run()
    {
        if (progress == null)
            progress = new ProgressData();

        progress.stepRange(5.0);
        String threadName = Thread.currentThread().getName();

        specScanner.setProgressObj(new ProgressData(progress));

        if (Thread.currentThread().isInterrupted())
            return;

        if (specScanner.getPepMassSpecKeyMap().isEmpty()) {
            if (!params.isSearchDelta())
                specScanner.makePepMassSpecKeyMap();
            else
                specScanner.makePepMassSpecKeyMapWithDelta(params.getIsotopeDeltaSet());
        }

        if (Thread.currentThread().isInterrupted())
            return;

        specScanner.preProcessSpectra();
        if (Thread.currentThread().isInterrupted())
            return;

        specScanner.getProgressObj().setParentProgress(null);
        progress.report(5.0);
        progress.stepRange(80.0);
        scanner.setProgressObj(new ProgressData(progress));

        scanner.setThreadName(threadName);

        int ntt = params.getNumTolerableTermini();
        if (params.getEnzyme() == null)
            ntt = 0;
        int nnet = 2 - ntt;
        if (Thread.currentThread().isInterrupted())
            return;

        scanner.dbSearch(nnet);
        if (Thread.currentThread().isInterrupted())
            return;

        progress.stepRange(95.0);

        if (Thread.currentThread().isInterrupted())
            return;

        scanner.computeSpecEValue(false);
        if (Thread.currentThread().isInterrupted())
            return;

        scanner.getProgressObj().setParentProgress(null);
        progress.stepRange(100);

        if (Thread.currentThread().isInterrupted())
            return;

        scanner.generateSpecIndexDBMatchMap();

        progress.report(30.0);

        if (params.outputAdditionalFeatures())
            scanner.addAdditionalFeatures();

        progress.report(60.0);

        scanner.addResultsToList(resultList);

        progress.report(100.0);
    }
}
