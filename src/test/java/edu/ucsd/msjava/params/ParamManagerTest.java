package edu.ucsd.msjava.params;

import edu.ucsd.msjava.ui.MSGFPlus;
import org.testng.annotations.Test;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 31 Oct 2018, 4:33 PM
 */
public class ParamManagerTest
{

    @Test
    public void testPrint()
    {
        ParamManager manager = new ParamManager("MS-GF+", MSGFPlus.VERSION, MSGFPlus.RELEASE_DATE, "java -Xmx3500M -jar MSGFPlus.jar");
        manager.addMSGFPlusParams();
        manager.printUsageInfo();

    }

}