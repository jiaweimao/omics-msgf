package edu.ucsd.msjava.ui;

import edu.ucsd.msjava.msscorer.NewScorerFactory;
import edu.ucsd.msjava.msscorer.ScoringParameterGeneratorWithErrors;
import edu.ucsd.msjava.msutil.*;
import edu.ucsd.msjava.params.ParamManager;
import org.testng.annotations.Test;

import java.io.File;

import static org.testng.Assert.*;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 19 Jan 2019, 6:59 PM
 */
public class ScoringParamGenTest
{

    @Test
    public void testScoringParamGen()
    {
        File resultPath = new File("/Users/kims336/Research/Data/TrainingITRAQ/Phospho/test");
        File specPath = new File("/Users/kims336/Research/Data/TrainingITRAQ/Phospho/mzMLFiles");

        String[] argv = {"-i", resultPath.getPath(), "-d", specPath.getPath(), "-m", "2", "-inst", "1", "-e", "0", "-protocol", "3"};
        ParamManager paramManager = new ParamManager("ScoringParamGen", String.valueOf(ScoringParamGen.VERSION), ScoringParamGen.DATE,
                "java -Xmx2000M -cp MSGFPlus.jar edu.ucsd.msjava.ui.ScoringParamGen");
        paramManager.addScoringParamGenParams();
        paramManager.parseParams(argv);

        ScoringParamGen.runScoringParamGen(paramManager);
    }

    @Test(enabled = false)
    public void testScoringParamGenETD()
    {
        File resultPath = new File("/Users/kims336/Research/Data/TrainingITRAQ/Global/beforeTraining/CPTAC_OvC_JB5427_iTRAQ_01_9Apr12_Cougar_12-03-21.mzid");
        File specPath = new File("/Users/kims336/Research/Data/TrainingITRAQ/Global/mzMLFiles");

        String[] argv = {"-i", resultPath.getPath(), "-d", specPath.getPath(), "-m", "1", "-inst", "0", "-e", "1"};
        ParamManager paramManager = new ParamManager("ScoringParamGen", String.valueOf(ScoringParamGen.VERSION), ScoringParamGen.DATE,
                "java -Xmx2000M -cp MSGFPlus.jar edu.ucsd.msjava.ui.ScoringParamGen");
        paramManager.addScoringParamGenParams();
        paramManager.parseParams(argv);

        ScoringParamGen.runScoringParamGen(paramManager);
    }


    @Test(enabled = false)
    public void testScoringParamGen2()
    {
        File resultPath = new File("C:\\cygwin\\home\\kims336\\Data\\Scoring");
        File specPath = new File("C:\\cygwin\\home\\kims336\\Data\\Scoring");

        String[] argv = {"-i", resultPath.getPath(), "-d", specPath.getPath(), "-m", "2", "-inst", "3", "-e", "0"
                , "-protocol", "5"
        };

        ParamManager paramManager = new ParamManager("ScoringParamGen", "Test", "Test",
                "java -Xmx2000M -cp MSGFPlus.jar edu.ucsd.msjava.ui.ScoringParamGen");

        paramManager.addScoringParamGenParams();
        paramManager.parseParams(argv);
        ScoringParamGen.runScoringParamGen(paramManager);
        System.out.println("Done");
    }

    @Test(enabled = false)
    public void testScoringParamGenFromMgf()
    {
        ActivationMethod actMethod = ActivationMethod.HCD;
        InstrumentType instType = InstrumentType.QEXACTIVE;
        Enzyme enzyme = Enzyme.TRYPSIN;
        Protocol protocol = Protocol.STANDARD;
        File specFile = new File("D:\\Research\\Data\\TrainingMSGFPlus\\AnnotatedSpectra\\HCD_QExactive_Tryp.mgf");
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSetWithFixedCarbamidomethylatedCys();

        NewScorerFactory.SpecDataType dataType = new NewScorerFactory.SpecDataType(actMethod, instType, enzyme, protocol);
        System.out.println("Processing " + dataType.toString());
        ScoringParameterGeneratorWithErrors.generateParameters(
                specFile,
                dataType,
                aaSet,
                new File("C:\\cygwin\\home\\kims336\\Data\\Scoring"),
                false);

    }
}