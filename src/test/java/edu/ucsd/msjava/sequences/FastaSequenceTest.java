package edu.ucsd.msjava.sequences;

import edu.ucsd.msjava.msdbsearch.CompactFastaSequence;
import edu.ucsd.msjava.msdbsearch.DBScanner;
import edu.ucsd.msjava.msdbsearch.SuffixArrayForMSGFDB;
import edu.ucsd.msjava.msutil.AminoAcid;
import edu.ucsd.msjava.msutil.AminoAcidSet;
import edu.ucsd.msjava.msutil.Composition;
import edu.ucsd.msjava.suffixarray.SuffixArray;
import edu.ucsd.msjava.suffixarray.SuffixArraySequence;
import omics.util.ms.peaklist.impl.AbsoluteTolerance;
import omics.util.ms.peaklist.impl.PpmTolerance;
import org.testng.annotations.Test;

import java.io.File;
import java.net.URISyntaxException;

/**
 * @author JiaweiMao
 * @version 1.0.0
 * @since 24 Oct 2018, 12:29 PM
 */
public class FastaSequenceTest
{
    @Test
    public void getAAProbabilities() throws URISyntaxException
    {
        File dbFile = new File(FastaSequenceTest.class.getClassLoader().getResource("human-uniprot-contaminants.fasta").toURI());
        AminoAcidSet aaSet = AminoAcidSet.getStandardAminoAcidSetWithFixedCarbamidomethylatedCys();
        DBScanner.setAminoAcidProbabilities(dbFile.getPath(), aaSet);
        for (AminoAcid aa : aaSet) {
            System.out.println(aa.getResidue() + "\t" + aa.getProbability());
        }
    }

    @Test
    public void getNumCandidatePeptides() throws URISyntaxException
    {
        File dbFile = new File(FastaSequenceTest.class.getClassLoader().getResource("human-uniprot-contaminants.fasta").toURI());
        SuffixArraySequence sequence = new SuffixArraySequence(dbFile.getPath());
        SuffixArray sa = new SuffixArray(sequence);
        AminoAcidSet aaSet = AminoAcidSet.getAminoAcidSetFromModFile(new File(FastaSequenceTest.class.getClassLoader().getResource("Mods.txt").toURI()).getAbsolutePath());
        System.out.println("NumPeptides: " + sa.getNumCandidatePeptides(aaSet, 2364.981689453125f, new PpmTolerance(10)));
    }


    @Test(enabled = false)
    public void testRedundantProteins() throws URISyntaxException
    {
        File databaseFile = new File(FastaSequenceTest.class.getClassLoader().getResource("ecoli-reversed.fasta").toURI());

        CompactFastaSequence fastaSequence = new CompactFastaSequence(databaseFile.getPath());
        float ratioUniqueProteins = fastaSequence.getRatioUniqueProteins();
        if (ratioUniqueProteins < 0.5f) {
            fastaSequence.printTooManyDuplicateSequencesMessage(databaseFile.getName(), "MS-GF+", ratioUniqueProteins);
            System.exit(-1);
        }

        float fractionDecoyProteins = fastaSequence.getFractionDecoyProteins();
        if (fractionDecoyProteins < 0.4f || fractionDecoyProteins > 0.6f) {
            System.err.println("Error while reading: " + databaseFile.getName() + " (fraction of decoy proteins: " + fractionDecoyProteins + ")");
            System.err.println("Delete " + databaseFile.getName() + " and run MS-GF+ again.");
            System.exit(-1);
        }

    }

    @Test
    public void testTSA() throws Exception
    {
        File dbFile = new File(FastaSequenceTest.class.getClassLoader().getResource("human-uniprot-contaminants.fasta").toURI());
        SuffixArraySequence sequence = new SuffixArraySequence(dbFile.getPath());

        long time;
        System.out.println("SuffixArrayForMSGFDB");
        time = System.currentTimeMillis();
        SuffixArrayForMSGFDB sa2 = new SuffixArrayForMSGFDB(sequence);
        System.out.println("Time: " + (System.currentTimeMillis() - time));
        int numCandidates = sa2.getNumCandidatePeptides(AminoAcidSet.getStandardAminoAcidSetWithFixedCarbamidomethylatedCys(), (383.8754f - (float) Composition.ChargeCarrierMass()) * 3 - (float) Composition.H2O, new AbsoluteTolerance(2.5f));
        System.out.println("NumCandidatePeptides: " + numCandidates);
        int length10 = sa2.getNumDistinctPeptides(10);
        System.out.println("NumUnique10: " + length10);
    }
}