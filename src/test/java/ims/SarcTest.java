package ims;

import edu.ucsd.msjava.msdbsearch.PeptideEnumerator;
import org.testng.annotations.Test;

import java.io.File;

public class SarcTest
{
    @Test
    public void generatePeptideList() throws Exception
    {
        File fastaFile = new File(SarcTest.class.getClassLoader().getResource("human-uniprot-contaminants.fasta").toURI());
        File outputFile = File.createTempFile("HumanPeptides", "tsv");
        PeptideEnumerator.enumerate(fastaFile, outputFile);
        outputFile.deleteOnExit();
        System.out.println("Done");
    }
}
