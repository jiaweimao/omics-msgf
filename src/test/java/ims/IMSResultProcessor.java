package ims;

import org.testng.annotations.Test;

import java.io.*;

public class IMSResultProcessor
{

    @Test(enabled = false)
    public void filterIMSResults() throws Exception
    {
        File result = new File("C:\\cygwin\\home\\kims336\\Data\\IMS_Sarc\\SarcCtrl_P21_1mgml_IMS6_AgTOF07_210min_CID_01_05Oct12_Frodo_Precursors_Removed_Collision_Energy_Collapsed_0801.tsv");
        File output = new File("C:\\cygwin\\home\\kims336\\Data\\IMS_Sarc\\SarcCtrl_P21_1mgml_IMS6_AgTOF07_210min_CID_01_05Oct12_Frodo_Precursors_Removed_Collision_Energy_Collapsed_0801_filtered.tsv");

        PrintStream out = new PrintStream(new BufferedOutputStream(new FileOutputStream(output)));

        BufferedReader reader = new BufferedReader(new FileReader(result));
//        BufferedLineReader in = new BufferedLineReader(result.getPath());

        // header
        out.println(reader.readLine());

        String s;
        while ((s = reader.readLine()) != null) {
            String[] token = s.split("\t");
            if (token.length < 14)
                continue;
            double score = Double.parseDouble(token[12]);
            if (score > -20)
                out.println(s);
        }

        reader.close();
        out.close();

        System.out.println("Done");
    }
}
